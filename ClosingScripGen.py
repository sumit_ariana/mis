import os
import numpy as np
import pandas as pd
from helper import Database
from helper import TableName
import datetime as dt
from dateutil import relativedelta
import helper as hp

BASE_U_FOLDER = os.path.join(os.path.abspath(r'\\192.168.10.2\aia-drive\MIS\Ariana_MIS\MIS_Input'), "Closing_Scripts")


def get_previous_date(ex_date, db_connection):
    """

    :param db_connection:
    :return:
    """
    start_date = ex_date - relativedelta.relativedelta(days=10)
    query_str = "SELECT DISTINCT Date from {t_name} WHERE Date BETWEEN '{s_date}' and '{e_date}'".format(
        s_date=str(start_date), e_date=str(ex_date), t_name=TableName.Date_List_Table.value)
    df_data = pd.read_sql(query_str, db_connection, parse_dates=['Date'])
    df_data.sort_values('Date', inplace=True)
    if ex_date not in df_data.Date.tolist():
        raise Exception("Date {} not in Date List Table".format(str(ex_date)))
    if len(df_data) > 1:
        previous_date = df_data.Date.tolist()[-2]
    else:
        previous_date = df_data.Date.tolist()[-1]
    return previous_date


def get_scrip(ex_date, db):
    """

    :param ex_date:
    :return:
    """
    db_conn = db.db_connection
    previous_date = get_previous_date(ex_date, db_conn)
    df_trade = db.get_trade_report(ex_date)
    df_trade.set_index(['UniqueTicker', 'Broker', 'SecurityType'], inplace=True)
    column_list = ['MIS_DATE', 'UniqueTicker', 'Broker', 'Exchange', 'StrategyTag', 'SecurityType', 'LastTradeableDate',
                   'NET_QTY', 'NET_VALUE', 'Expired']
    df_mis = db.get_mis(previous_date, column_list)
    df_mis['Sec'] = df_mis['SecurityType']
    df_mis.set_index(['UniqueTicker', 'Broker', 'SecurityType'], inplace=True)
    ix1 = np.logical_or(df_mis['Expired'] == 0, df_mis['Sec'] == 'SPOT')
    uni_list = df_mis[ix1].index.tolist()
    uni_list = list(set(uni_list).union(df_trade.index.tolist()))
    df_list = pd.DataFrame(data=uni_list, columns=['UniqueTicker', 'Broker', 'SecurityType'])
    ix1 = df_list.SecurityType == 'SPOT'
    df_list.loc[ix1, "Broker"] = 'ALL'
    df_list = df_list.loc[~df_list.index.duplicated(keep='first')]
    df_list.set_index(['UniqueTicker', 'Broker', 'SecurityType'],inplace=True)
    df_list = df_list.loc[~df_list.index.duplicated(keep='first')]
    df_list.reset_index(drop=False,inplace=True)
    # # df_list.drop_duplicates(['UniqueTicker', 'Broker', 'SecurityType'], keep='first', inplace=True)
    return df_list


def tally_conversion(df_current_close, ex_date, db):
    """

    :param df_current_close:
    :param ex_date:
    :param db:
    :return:
    """
    previous_date = get_previous_date(ex_date,db.db_connection)
    df_test = df_current_close.copy()
    df_previous_close = db.get_close_price(df_current_close.UniqueTicker.tolist(),previous_date)
    new_list = list(set(df_test.UniqueTicker.tolist()).difference(df_previous_close.UniqueTicker.tolist()))
    ix1 = df_previous_close.Conversion == 1
    df_previous_close = df_previous_close.loc[ix1]
    df_previous_close.set_index(['UniqueTicker', 'Broker'],inplace=True)
    df_test.set_index(['UniqueTicker', 'Broker'],inplace=True)
    df_previous_close = df_previous_close.loc[~df_previous_close.index.duplicated(keep='first')]
    df_test  = df_test.loc[~df_test.index.duplicated(keep='first')]

    df_previous_close['new_conversion'] = df_test['Conversion']
    df_previous_close['test'] = df_previous_close['new_conversion'].astype(float) == df_previous_close['Conversion'].astype(float)
    ix1 = df_previous_close['test']==False
    df_t = df_previous_close.loc[ix1]
    df_t.reset_index(inplace=True,drop=False)
    df_test.reset_index(inplace=True,drop=False)
    mis_match_scrip = df_t.UniqueTicker.tolist()
    df_previous_close.reset_index(drop=False,inplace=True)

    mis_match_scrip =list(set(mis_match_scrip).union(new_list))

    return mis_match_scrip


def generate_close(ex_date,db):
    """
    Callable function from the menu
    :param ex_date:
    :param db:
    :return:
    """
    close_path = os.path.abspath(
        r'\\192.168.10.2\aia-drive\Back-Office\Ariana Advisory Pvt Ltd\Sunny Daily Activity\Athena Inputs\MIS Close')
    close_folder = os.path.join(os.path.abspath(r'\\192.168.10.2\aia-drive\MIS\Ariana_MIS\UploadFiles'),
                                ex_date.strftime("%Y-%m-%d"))
    close_file_name = os.path.join(close_path, ex_date.strftime("%Y%m%d") + '.csv')
    df_scrips = get_scrip(ex_date, db)
    filename = os.path.join(BASE_U_FOLDER, str(ex_date.date()) + ".csv")
    df_scrips.to_csv(filename)
    df_mapping = db.get_mapping_table(df_scrips.UniqueTicker.tolist())
    df_scrips.set_index(['UniqueTicker', 'SecurityType'], inplace=True)
    df_mapping.set_index(['UniqueTicker', 'SecurityType'], inplace=True)
    df_mapping = df_mapping.loc[~df_mapping.index.duplicated(keep='first')]
    df_scrips['MIS_TICKER'] = df_mapping['MIS_Ticker']
    df_scrips['Is_Quanto'] = df_mapping['Is_Quanto']
    df_scrips.reset_index(inplace=True, drop=False)
    df_scrips.set_index(['MIS_TICKER'], inplace=True)
    df_close_files = pd.read_csv(close_file_name, index_col=None,
                                 names=['Date', 'MIS_TICKER', 'Close', 'Broker Close', 'Conversion'])
    df_close_files.sort_values(['Broker Close'], ascending=False, inplace=True)
    ix_q = df_scrips.Is_Quanto == 1

    df_close_files.set_index(['MIS_TICKER'], inplace=True)
    df_close_files = df_close_files.loc[~df_close_files.index.duplicated(keep='first')]
    df_scrips['Close'] = df_close_files['Close']
    df_scrips['Broker Close'] = df_close_files['Broker Close']
    df_scrips['Conversion'] = df_close_files['Conversion']
    ix_repo = df_scrips.SecurityType == 'REPO'
    df_scrips.loc[ix_repo, ['Close', 'Broker Close', 'Conversion']] = [0, 0, 1]
    df_scrips.loc[ix_q, 'Type'] = "Quanto"
    df_scrips.loc[ix_q, 'RoundingFactor'] = 4
    df_scrips.loc[ix_q, 'Conversion Factor'] = np.nan

    mis_scrips = tally_conversion(df_scrips, ex_date, db)
    df_scrips.reset_index(drop=True, inplace=True)
    df_scrips['Date'] = ex_date.date()
    dict_rename = {'UniqueTicker': "Ticker", 'SecurityType': 'Security Type'}
    df_scrips.rename(dict_rename, inplace=True, axis=1)
    df_scrips.drop(['Is_Quanto'], axis=1, inplace=True)
    ix1 = df_scrips.Ticker.isin(mis_scrips)
    df_scrips.loc[ix1, 'wrong_conversion'] = True
    df_scrips.loc[np.logical_not(ix1), 'wrong_conversion'] = False
    df_scrips.reset_index(drop=True, inplace=True)
    df_scrips.set_index('Date', inplace=True)
    save_close_file = os.path.join(close_folder, ex_date.strftime("%Y%m%d") + "_Close.csv")
    df_scrips.to_csv(save_close_file)






# if __name__ == '__main__':
#     base_path = os.path.join(os.path.abspath(r'\\192.168.10.2\aia-drive\MIS\Ariana_MIS'), 'Database')
#     close_path = os.path.abspath(r'\\192.168.10.20\Ariana\Sunny Daily Activity\Athena Inputs\MIS Close')
#
#     db_name = 'DELTA_MIS_DB'
#     db = hp.Database(base_path, db_name)
#     db.conncet_db()
#     ex_date = dt.datetime.strptime(input("Enter Date:"), "%d-%m-%Y")
#     close_folder = os.path.join(os.path.abspath(r'\\192.168.10.2\aia-drive\MIS\Ariana_MIS\UploadFiles'),
#                                 ex_date.strftime("%Y-%m-%d"))
#     close_file_name = os.path.join(close_path, ex_date.strftime("%Y%m%d") + '.csv')
#     df_scrips = get_scrip(ex_date, db)
#     filename = os.path.join(BASE_U_FOLDER, str(ex_date.date()) + ".csv")
#     df_scrips.to_csv(filename)
#     df_mapping = db.get_mapping_table(df_scrips.UniqueTicker.tolist())
#     df_scrips.set_index(['UniqueTicker', 'SecurityType'], inplace=True)
#     df_mapping.set_index(['UniqueTicker', 'SecurityType'], inplace=True)
#     df_mapping = df_mapping.loc[~df_mapping.index.duplicated(keep='first')]
#     df_scrips['MIS_TICKER'] = df_mapping['MIS_Ticker']
#     df_scrips['Is_Quanto'] = df_mapping['Is_Quanto']
#     df_scrips.reset_index(inplace=True, drop=False)
#     df_scrips.set_index(['MIS_TICKER'], inplace=True)
#     df_close_files = pd.read_csv(close_file_name, index_col=None,
#                                  names=['Date', 'MIS_TICKER', 'Close', 'Broker Close', 'Conversion'])
#     df_close_files.sort_values(['Broker Close'],ascending=False,inplace=True)
#     ix_q = df_scrips.Is_Quanto == 1
#
#     df_close_files.set_index(['MIS_TICKER'], inplace=True)
#     df_close_files = df_close_files.loc[~df_close_files.index.duplicated(keep='first')]
#     df_scrips['Close'] = df_close_files['Close']
#     df_scrips['Broker Close'] = df_close_files['Broker Close']
#     df_scrips['Conversion'] = df_close_files['Conversion']
#     ix_repo = df_scrips.SecurityType == 'REPO'
#     df_scrips.loc[ix_repo, ['Close', 'Broker Close', 'Conversion']] = [0, 0, 1]
#     df_scrips.loc[ix_q, 'Type'] = "Quanto"
#     df_scrips.loc[ix_q, 'RoundingFactor'] = 4
#     df_scrips.loc[ix_q, 'Conversion Factor'] = np.nan
#
#     mis_scrips = tally_conversion(df_scrips, ex_date, db)
#     df_scrips.reset_index(drop=True, inplace=True)
#     df_scrips['Date'] = ex_date.date()
#     dict_rename = {'UniqueTicker': "Ticker", 'SecurityType': 'Security Type'}
#     df_scrips.rename(dict_rename, inplace=True, axis=1)
#     df_scrips.drop(['Is_Quanto'], axis=1, inplace=True)
#     ix1 = df_scrips.Ticker.isin(mis_scrips)
#     df_scrips.loc[ix1,'wrong_conversion'] = True
#     df_scrips.loc[np.logical_not(ix1), 'wrong_conversion'] = False
#     df_scrips.reset_index(drop=True, inplace=True)
#     df_scrips.set_index('Date', inplace=True)
#     save_close_file = os.path.join(close_folder,ex_date.strftime("%Y%m%d")+"_Close.csv")
#     df_scrips.to_csv(save_close_file)
#
