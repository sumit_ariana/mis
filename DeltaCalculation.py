import os
import pandas as pd
import helper as hp
import datetime as dt
import numpy as np
import ReportGenerator as rg

def get_delta(ex_date, database):
    """

    :param ex_date:
    :param database:
    :return:
    """
    df_delta = database.get_delta_report(ex_date)
    df_mapping = database.get_mapping_table(df_delta.UniqueTicker.tolist())
    df_delta.set_index(['UniqueTicker'], inplace=True)
    df_mapping.set_index(['UniqueTicker'], inplace=True)
    df_mapping = df_mapping.loc[~df_mapping.index.duplicated(keep='first')]
    df_delta[['Strike', 'CallPut']] = df_mapping[['Strike', 'CallPut']]
    df_delta[["Leg1_Currency", "Leg2_Currency", "Leg3_Currency"]] = df_mapping[
        ["Leg1_Cur", "Leg2_Cur", "Leg3_Cur"]]

    df_close = database.get_close_price([], ex_date)
    ix_close = np.logical_and(df_close['SecurityType'] == 'SPOT', df_close['Broker'] == 'ALL')
    df_close = df_close[ix_close]
    df_close.set_index('UniqueTicker', inplace=True)
    df_delta.reset_index(drop=False, inplace=True)
    ix_option_call = np.logical_and(df_delta.SecurityType == 'OPT', df_delta.CallPut == 'CE')
    ix_option_put = np.logical_and(df_delta.SecurityType == 'OPT', df_delta.CallPut == 'PE')
    df_delta.loc[ix_option_call, ['L1_Delta', 'L2_Delta']] = df_delta.loc[
                                                                 ix_option_call, ['L1_Delta', 'L2_Delta']] * 1.0
    df_delta.loc[ix_option_put, ['L1_Delta', 'L2_Delta']] = df_delta.loc[
                                                                ix_option_put, ['L1_Delta', 'L2_Delta']] * -1.0
    ix_option_call = np.logical_and(ix_option_call, df_delta["Leg1_Currency"].str.contains('PE', regex=False))
    df_delta.loc[ix_option_call, ['L1_Delta', 'L2_Delta']] = df_delta.loc[
                                                                 ix_option_call, ['L1_Delta', 'L2_Delta']] * -1.0
    ix_option_put = np.logical_and(ix_option_put, df_delta["Leg1_Currency"].str.contains('CE', regex=False))
    df_delta.loc[ix_option_put, ['L1_Delta', 'L2_Delta']] = df_delta.loc[
                                                                ix_option_put, ['L1_Delta', 'L2_Delta']] * -1.0
    func_check = lambda x: True if '/USD' in x else False
    df_delta['Test'] = df_delta['L1_Delta'] + df_delta['L2_Delta'] + df_delta['L3_Delta']
    df_delta = df_delta[df_delta['Test'] != 0]
    df_delta.drop('Test', axis=1, inplace=True)
    all_currency = []
    for i in [1, 2, 3]:
        currency_string = 'Leg{}_Currency'.format(str(i))
        new_column = 'Close_Currency_{}'.format(str(i))
        close_column = 'Close_{}'.format(str(i))
        dollar_delta_column = 'L{}_Dollar_Delta'.format(str(i))
        local_delta_column = 'L{}_Delta'.format(str(i))
        df_delta[new_column] = df_delta[currency_string].apply(hp.get_currency)
        all_currency.extend(list(set(all_currency).union(df_delta[currency_string].values.tolist())))
        df_delta.set_index(new_column, inplace=True)
        df_close = df_close.drop_duplicates(keep='first')
        df_delta[close_column] = df_close['Close']
        df_delta[close_column].fillna(value=1, inplace=True)
        df_delta.reset_index(drop=False, inplace=True)
        ix1 = df_delta[new_column].apply(func_check)
        ix2 = np.logical_not(ix1)
        df_delta.loc[ix1, dollar_delta_column] = df_delta.loc[ix1, local_delta_column] * df_delta.loc[ix1, close_column]
        df_delta.loc[ix2, dollar_delta_column] = df_delta.loc[ix2, local_delta_column] / df_delta.loc[ix2, close_column]
        df_delta.drop([close_column], axis=1, inplace=True)

    all_currency = list(set(all_currency))
    df_delta['Strat'] = df_delta['StrategyTag'].apply(lambda x: '|'.join(x.split('|')[:4]))
    category_index = []
    new = df_delta.Strat.str.split("|", n=3, expand=True)
    for j in range(len(new.columns.tolist())):
        column_string = 'Category' + str(j + 1)
        category_index.append(column_string)
        df_delta[column_string] = new[j]

    return df_delta, all_currency


def generate_strategy_delta(df_delta, all_currencies):
    """

    :param df_delta:
    :return:
    """

    df_t = df_delta.copy()
    df_final_local = pd.DataFrame()
    df_final_dollar = pd.DataFrame()
    df_final_local_rv = pd.DataFrame()
    df_final_dollar_rv = pd.DataFrame()
    for j in [1, 2]:
        # J == 1 is for Local Delta and J == 2 is for Dollar delta
        df_final = pd.DataFrame()
        for i in [1, 2, 3]:
            # i represents the delta of different legs
            currency_string = 'Leg{}_Currency'.format(str(i))
            if j == 1:
                value_string = 'L{}_Delta'.format(str(i))
            elif j == 2:
                value_string = 'L{}_Dollar_Delta'.format(str(i))

            df_temp = df_delta.pivot_table(index=['StrategyTag', currency_string], values=value_string,
                                           aggfunc=sum).unstack()
            df_temp = df_temp.xs(value_string, axis=1, drop_level=True)
            missing_columns = list(set(all_currencies).difference(df_temp.columns.tolist()))
            df_missing = pd.DataFrame(data=0, columns=missing_columns, index=df_temp.index)
            df_temp = pd.concat([df_temp, df_missing], sort=True)
            df_temp.fillna(value=0, inplace=True)
            if df_final.empty:
                df_final = df_temp.copy()
            else:
                df_final += df_temp
        df_final.reset_index(drop=False, inplace=True)
        df_final['Strat'] = df_final['StrategyTag'].apply(lambda x: '|'.join(x.split('|')[:4]))
        columns_to_sum = list(set((df_final.columns.tolist())).difference(['Strat', 'StrategyTag']))
        new = df_final.Strat.str.split("|", n=3, expand=True)
        category_index = []
        for k in range(len(new.columns.tolist())):
            column_string = 'Category' + str(k + 1)
            category_index.append(column_string)
            df_final[column_string] = new[k]
        ix_band = df_final['Category1'] == 'RV'
        df_final_rv =  df_final[ix_band]
        df_final = df_final[np.logical_not(ix_band)]
        if j == 1:
            header_name = "Local Delta"
        elif j == 2:
            header_name = "Dollar Delta"

        df_final = pd.pivot_table(df_final, index=category_index, values=columns_to_sum, aggfunc=[np.sum],
                                    margins=True)
        df_final = df_final.rename(index=dict(All='Grand Total'))
        df_final = df_final.rename(dict(sum=header_name), axis=1)
        df_final_rv = pd.pivot_table(df_final_rv, index=category_index, values=columns_to_sum, aggfunc=[np.sum],
                                  margins=True)
        df_final_rv = df_final_rv.rename(index=dict(All='Grand Total'))
        df_final_rv = df_final_rv.rename(dict(sum=header_name), axis=1)

        if j == 1:
            df_final_local = df_final.copy()
            df_final_local_rv = df_final_rv.copy()
        elif j == 2:
            df_final_dollar = df_final.copy()
            df_final_dollar_rv = df_final_dollar_rv.copy()

    return df_final_local, df_final_dollar,df_final_local_rv,df_final_dollar_rv


def generate_maturity_delta(df_delta, all_currencies):
    """

    :param df_delta:
    :return:
    """

    df_t = df_delta.copy()
    df_final_local = pd.DataFrame()
    df_final_dollar = pd.DataFrame()
    df_final_local_rv = pd.DataFrame()
    df_final_dollar_rv = pd.DataFrame()
    for k in [1,2]:
        # K = 1 is for pure and 2 is for RV
        for j in [1, 2]:
            # J == 1 is for Local Delta and J == 2 is for Dollar delta
            df_final = pd.DataFrame()
            for i in [1, 2, 3]:
                # i represents the delta of different legs
                currency_string = 'Leg{}_Currency'.format(str(i))
                if j == 1:
                    value_string = 'L{}_Delta'.format(str(i))
                elif j == 2:
                    value_string = 'L{}_Dollar_Delta'.format(str(i))
                ix_band = df_delta['Category1'] == 'RV'
                if k == 1:
                    ix_band = np.logical_not(ix_band)
                df_t = df_delta[ix_band]
                df_temp = df_t.pivot_table(index=['LastTradeableDate', currency_string], values=value_string,
                                               aggfunc=sum).unstack()
                df_temp = df_temp.xs(value_string, axis=1, drop_level=True)

                missing_columns = list(set(all_currencies).difference(df_temp.columns.tolist()))
                df_missing = pd.DataFrame(data=0, columns=missing_columns, index=df_temp.index)
                df_temp = pd.concat([df_temp, df_missing], sort=True)
                df_temp.fillna(value=0, inplace=True)
                if df_final.empty:
                    df_final = df_temp.copy()
                else:
                    df_final += df_temp
        df_final['Total'] = df_final.sum(axis=1)
        ix_non_zero = df_final['Total']!=0
        df_final = df_final.loc[ix_non_zero]
        df_final.drop('Total',axis=1,inplace=True)
        if j == 1:
            if k == 1:
                df_final_local = df_final.copy()
            elif k == 2:
                df_final_local_rv = df_final.copy()
        elif j == 2:
            if k == 1:
                df_final_dollar = df_final.copy()
            elif k == 2:
                df_final_dollar_rv = df_final.copy()

    return df_final_local, df_final_dollar,df_final_local_rv,df_final_dollar_rv

if __name__ == '__main__':
    base_path = os.path.join(os.path.abspath(r'\\192.168.10.2\aia-drive\MIS\Ariana_MIS'), 'Database')
    db_name = 'DELTA_MIS_DB'
    database = hp.Database(base_path, db_name)
    database.conncet_db()
    # ex_date = dt.datetime.strptime(input("Enter Date:"), "%d-%m-%Y")
    query_string = "select Date from DateList where Date between '2016-12-31' and '2020-05-26'"
    df_dates = database.get_query_df(query_string)
    df_final = pd.DataFrame()
    df_final_rv = pd.DataFrame()
    for ex_date in df_dates['Date'].tolist():
        print(str(ex_date))
        df_t, all_cu = rg.get_delta(ex_date, database)
        df_p = rg.generate_strategy_delta(df_t, all_cu)
        for i in [1,3]:
            df_temp = df_p[i]
            if not df_temp.empty:
                df_temp = df_temp.xs('Grand Total')
                df_temp = df_temp.xs('Dollar Delta',axis=1)
                df_temp.reset_index(inplace=True)
                df_temp = pd.DataFrame(index=[ex_date],data = df_temp[all_cu].as_matrix(),columns=all_cu)
            else:
                df_temp = pd.DataFrame(index=[ex_date], data=0, columns=all_cu)
            if i == 1:
                df_final = pd.concat([df_final,df_temp])
            else:
                df_final_rv = pd.concat([df_final_rv, df_temp])
    # df_temp[ex_date] = df_final[all_cu]
    file_t = os.path.abspath("Total_Delta.csv")
    file_rv = os.path.abspath("Total_Delta_RV.csv")
    df_final.to_csv(file_t)
    df_final_rv.to_csv(file_rv)
    # df_p = generate_maturity_delta(df_t, all_cu)[1]
    # df_p.to_clipboard()