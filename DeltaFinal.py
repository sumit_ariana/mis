import os
import helper as hp
import datetime as dt
import pandas as pd
import numpy as np
import FreshMIS
from helper import TableName
from enum import Enum
# import Delta_Testing

INO_LIST = ['INO-CE','INO-PE']

class NotionalType(Enum):
    inter_strategy_notional = 1
    treasury_notional = 2
    delta_strategy_notional = 3


def create_notional_trade(currency, ticker, broker, lasttradedate, valuedate, trade_side_1, trade_value, trade_price,
                          strategy_tag, trade_date, exchange, treasury_tag):
    """
    Creates notional trades
    :param ticker:
    :param trade_side:
    :param trade_qty:
    :param trade_price:
    :param strategy_tag:
    :return: DF with to notional entries
    """
    treasury_stratgey_tag = treasury_tag + '|FX-FWD'

    trade_columns = ['TradeDate', 'Broker', 'Exchange', 'Symbol', 'LastTradeableDate', 'ValueDate', 'SecurityType',
                     'Strike', 'CallPut', 'TransactionType', 'FilledQty', 'TradePrice', 'Classification', 'Strategy',
                     'Value', 'Multiplier', 'OTCType', 'ID', 'UniqueTicker', 'StrategyTag', 'IsNotional']
    df_trades = pd.DataFrame(columns=trade_columns)

    # entry for notional entry 1
    trade_qty = int(trade_value / trade_price)
    df_trades.loc[len(df_trades)] = [trade_date, broker, exchange, ticker, lasttradedate, valuedate, 'FWD', np.nan,
                                     np.nan, trade_side_1, trade_qty, trade_price, np.nan, np.nan,
                                     trade_qty * trade_price, 1, 'DF', np.nan, ticker, strategy_tag + '|FX-FWD', True]
    if trade_side_1 == 'Buy':
        treasury_side = 'Sell'
    else:
        treasury_side = 'Buy'
    df_trades.loc[len(df_trades)] = [trade_date, broker, exchange, ticker, lasttradedate, valuedate, 'FWD', np.nan,
                                     np.nan,
                                     treasury_side, trade_qty, trade_price, np.nan, np.nan, trade_qty * trade_price, 1,
                                     'DF', np.nan, ticker, treasury_stratgey_tag, True]
    return df_trades


def create_strategy_notional(ticker, broker, lasttradedate, valuedate, trade_side_1, trade_value, trade_price,
                             strategy_tag1, strategy_tag2, trade_date, exchange, instrument, multiplier,
                             ntm_type, notional_type):
    """
    Creates notional trades
    :param ticker:
    :param trade_side:
    :param trade_qty:
    :param trade_price:
    :param strategy_tag:
    :return: DF with to notional entries
    """

    trade_columns = ['TradeDate', 'Broker', 'Exchange', 'Symbol', 'LastTradeableDate', 'ValueDate', 'SecurityType',
                     'Strike', 'CallPut', 'TransactionType', 'FilledQty', 'TradePrice', 'Classification', 'Strategy',
                     'Value', 'Multiplier', 'OTCType', 'ID', 'UniqueTicker', 'StrategyTag', 'IsNotional', 'Remarks']

    df_trades = pd.DataFrame(columns=trade_columns)
    remarks = notional_type.name
    # entry for notional entry 1
    if ntm_type == 1:
        trade_qty = int(trade_value / trade_price/multiplier)
        t_value = trade_qty * trade_price * multiplier
        t_price = trade_price
    elif ntm_type == -1:
        trade_qty = int(trade_value / multiplier / trade_price)
        t_value = trade_qty * trade_price * multiplier
        t_price = trade_price * multiplier

    tag1 = strategy_tag1 + '|FX-{}'.format(instrument)
    tag2 = strategy_tag2 + '|FX-{}'.format(instrument)

    if instrument == 'FUT':
        otc_type = np.nan
    elif instrument == 'FWD':
        otc_type = 'NDF'

    df_trades.loc[len(df_trades)] = [trade_date, broker, exchange, ticker, lasttradedate, valuedate, instrument, np.nan,
                                     np.nan,
                                     trade_side_1, trade_qty, t_price, np.nan, np.nan, t_value, multiplier,
                                     otc_type, np.nan, ticker, tag1, True, remarks]
    if trade_side_1 == 'Buy':
        treasury_side = 'Sell'
    else:
        treasury_side = 'Buy'

    df_trades.loc[len(df_trades)] = [trade_date, broker, exchange, ticker, lasttradedate, valuedate, instrument, np.nan,
                                     np.nan,
                                     treasury_side, trade_qty, t_price, np.nan, np.nan, t_value, multiplier,
                                     otc_type, np.nan, ticker, tag2, True, remarks]
    return df_trades


def get_dollar_delta(df_delta, index_currency_col, pivot_currency_col, df_close):
    """

    :param df_delta:
    :param currency_column:
    :return:
    """
    func_check = lambda x: True if '/USD' in x else False
    df_temp = df_delta.unstack().reset_index()
    df_temp.rename({0: 'Delta'}, axis=1, inplace=True)
    df_temp['Delta'].fillna(value=0.0, inplace=True)
    df_temp['Close_Currency'] = df_temp[pivot_currency_col].apply(hp.get_currency)
    df_close.set_index('UniqueTicker', inplace=True)
    df_temp.set_index(index_currency_col, inplace=True)
    df_temp['Close'] = df_close['Close']
    df_temp['Close'].fillna(value=1, inplace=True)
    df_temp.reset_index(drop=False, inplace=True)
    df_temp['Multiplier'] = df_temp[index_currency_col].apply(func_check)
    ix1 = df_temp['Multiplier']
    ix2 = np.logical_not(ix1)
    df_temp.loc[ix1, 'DollarDelta'] = df_temp.loc[ix1, 'Delta'] * df_temp.loc[ix1, 'Close']
    df_temp.loc[ix2, 'DollarDelta'] = df_temp.loc[ix2, 'Delta'] / df_temp.loc[ix2, 'Close']
    df_temp.drop(['Close', 'Multiplier', 'Delta'], axis=1, inplace=True)
    df_temp = df_temp.pivot_table(index=['StrategyTag', pivot_currency_col], values='DollarDelta',
                                  aggfunc=sum).unstack()
    df_temp = df_temp.xs('DollarDelta', axis=1, drop_level=True)
    return df_temp


def generate_delta_report(ex_date, database):
    """

    :param ex_date:
    :return:
    """

    df_local = df_l1_delta.fillna(value=0.0) + df_l2_delta.fillna(value=0.0) + df_l3_delta.fillna(value=0.0)
    df_local.reset_index(drop=False, inplace=True)
    df_dollar = df_l1_delta_dollar.fillna(value=0.0) + df_l2_delta_dollar.fillna(value=0.0) + df_l3_delta_dollar.fillna(
        value=0.0)
    df_dollar.reset_index(drop=False, inplace=True)
    # df.rename(columns={df.columns[0]: "StrategyTag"},inplace=True)
    df_local['Strat'] = df_local['StrategyTag'].apply(lambda x: '|'.join(x.split('|')[:4]))
    df_dollar['Strat'] = df_dollar['StrategyTag'].apply(lambda x: '|'.join(x.split('|')[:4]))
    columns_to_sum = list(set((df_local.columns.tolist())).difference(['Strat', 'StrategyTag']))
    df_local = df_local.groupby(['Strat'])[columns_to_sum].sum()
    df_local.sort_values(by=df_local.index.tolist(), axis=1, inplace=True)
    df_local = df_local.reindex(sorted(df_local.columns), axis=1)
    df_dollar = df_dollar.groupby(['Strat'])[columns_to_sum].sum()
    df_dollar.sort_values(by=df_local.index.tolist(), axis=1, inplace=True)
    df_dollar = df_dollar.reindex(sorted(df_dollar.columns), axis=1)
    return df_local, df_dollar


def get_transfer_ticker(df_mis, currency, database, ex_date, check_for_future, strategy1, strategy2, treasury_transfer):
    """

    :param df_mis:
    :param currency:
    :return:
    """
    df_temp_mis = df_mis.copy()
    df_temp_mis.reset_index(drop=False, inplace=True)
    ticker_list = df_temp_mis.UniqueTicker.tolist()
    df_mapping = database.get_mapping_table(ticker_list)
    df_temp_mis.set_index(['UniqueTicker'], inplace=True)
    df_mapping.set_index(['UniqueTicker'], inplace=True)
    df_mapping = df_mapping.loc[~df_mapping.index.duplicated(keep='first')]
    df_temp_mis['Notional_Trade_Mapping'] = df_mapping['Notional_Trade_Mapping']
    df_temp_mis['NTM_Currency'] = df_mapping['NTM_Currency']
    df_temp_mis['Multiplier'] = df_mapping['Multiplier']
    df_temp_mis.reset_index(drop=False, inplace=True)
    cur_test = 'USD' + '/' + currency
    ix_cur = df_temp_mis.NTM_Currency == cur_test
    ix_cur = np.logical_and(ix_cur, df_temp_mis.LastTradeableDate > ex_date)
    ix_fwd = df_temp_mis.SecurityType == 'FWD'
    ix_fwd = np.logical_and(ix_cur, ix_fwd)
    test = 0
    ticker_details = pd.DataFrame()

    if ix_fwd.any() and test == 0:
        ix_temp = np.logical_and(ix_fwd, df_temp_mis.StrategyTag.str.contains(strategy1, regex=False))
        fwd_ticker_strategy1 = df_temp_mis[ix_temp].UniqueTicker.tolist()
        ix_temp = np.logical_and(ix_fwd, df_temp_mis.StrategyTag.str.contains(strategy2, regex=False))
        fwd_ticker_strategy2 = df_temp_mis[ix_temp].UniqueTicker.tolist()
        intersection_ticker = list(set(fwd_ticker_strategy1).intersection(fwd_ticker_strategy2))
        if intersection_ticker:
            ix_fwd = df_temp_mis.UniqueTicker.isin(intersection_ticker)
            ix_fwd = np.logical_and(ix_fwd, df_temp_mis.LastTradeableDate > ex_date)
            if ix_fwd.any():
                test += 1
        else:
            if treasury_transfer:
                ix_fwd = np.logical_and(ix_fwd, df_temp_mis.LastTradeableDate > ex_date)
        ticker_details = df_temp_mis[ix_fwd]
        ticker_details['abs_qty'] = abs(df_temp_mis['NET_QTY'])
        ticker_details.sort_values(['LastTradeableDate', 'abs_qty'], ascending=[False, False], inplace=True)

    if test == 0 and check_for_future:
        ix_fut = df_temp_mis.SecurityType == 'FUT'
        ix_fut = np.logical_and(ix_cur, ix_fut)
        if ix_fut.any():
            ix_temp = np.logical_and(ix_fut, df_temp_mis.StrategyTag.str.contains(strategy1, regex=False))
            fut_ticker_strategy1 = df_temp_mis[ix_temp].UniqueTicker.tolist()
            ix_temp = np.logical_and(ix_fut, df_temp_mis.StrategyTag.str.contains(strategy2, regex=False))
            fut_ticker_strategy2 = df_temp_mis[ix_temp].UniqueTicker.tolist()
            intersection_ticker = list(set(fut_ticker_strategy1).intersection(fut_ticker_strategy2))
            if intersection_ticker:
                ix_fut = df_temp_mis.UniqueTicker.isin(intersection_ticker)
                ix_fut = np.logical_and(ix_fut, df_temp_mis.LastTradeableDate > ex_date)
            else:
                if treasury_transfer:
                    ix_fut = np.logical_and(ix_fut, df_temp_mis.LastTradeableDate > ex_date)
            ticker_details = df_temp_mis[ix_fut]
            ticker_details['abs_qty'] = abs(df_temp_mis['NET_QTY'])
            ticker_details.sort_values(['LastTradeableDate', 'Exchange', 'abs_qty'], ascending=[False, True, False],
                                       inplace=True)


    if not ticker_details.empty:
        ticker_details = ticker_details.iloc[0]

    return ticker_details



def generate_delta_notional(ex_date, database, dollar_delta_threshold, currency_list):
    """

    :param ex_date:
    :param database:
    :return:
    """
    df_delta, df_delta_dollar = hp.VaR.generate_delta_report(ex_date, database)
    df_mis = database.get_mis(ex_date)
    ix_tick_fwd = df_mis.SecurityType == 'FWD'
    ix_tick_fut = df_mis.SecurityType == 'FUT'
    ix_tick_fwd = np.logical_or(ix_tick_fwd, ix_tick_fut)
    fwd_ticker = df_mis[ix_tick_fwd].UniqueTicker.tolist()
    df_close = database.get_close_price(fwd_ticker, ex_date)
    # df_close.set_index(['UniqueTicker'])
    df_notional_trades = pd.DataFrame()
    df_remark = pd.DataFrame(columns=['Currency', 'Date', 'Delta_Value', 'Remarks'])
    cur_list = df_delta_dollar.columns.tolist()
    ino_list = list(set(INO_LIST).intersection(cur_list))
    for currency in cur_list:
        if currency == 'INR' and df_delta_dollar.columns.isin(ino_list).any():
            df_ino_test = df_delta_dollar[ino_list]
        else:
            df_ino_test = pd.DataFrame()

        df_local = df_delta[currency]
        df_dollar = df_delta_dollar[currency]
        ix_arb = df_dollar.index.str.contains('ARBITRAGE', regex=False)
        df_arb = df_dollar[ix_arb]
        ix_check = np.logical_or(df_arb > dollar_delta_threshold, df_arb < dollar_delta_threshold * -1)
        df_arb = df_arb[ix_check]
        for strat_tag , delta_value in df_arb.iteritems():
            df_temp_notional_trades = pd.DataFrame()
            new_delta_strategy = strat_tag.replace('ARBITRAGE|','DELTA|')
            inr_check = True
            if currency == 'INR':
                ino_sum = ((abs(df_ino_test) > 100000).any()).any()
                if ino_sum and  ('OPTIONS' in strat_tag) and delta_value!=0:
                    inr_check = False

            if abs(delta_value)>dollar_delta_threshold and inr_check:
                ix_strategy = np.logical_or(df_mis.StrategyTag.str.contains(strat_tag, regex=False),
                                            df_mis.StrategyTag.str.contains(new_delta_strategy, regex=False))
                df_mis_temp = df_mis[ix_strategy]
                ticker_details = get_transfer_ticker(df_mis_temp.copy(), currency, database, ex_date, True,
                                                     strat_tag, new_delta_strategy,True)
                if not ticker_details.empty:
                    ticker, broker, lasttradeabledate, exchange, ntm, instrument, multiplier = ticker_details[
                        ['UniqueTicker', 'Broker', 'LastTradeableDate', 'Exchange', 'Notional_Trade_Mapping',
                         'SecurityType', 'Multiplier']]

                    if ntm == 1:
                        delta_dollar_1 = df_local[strat_tag]
                    elif ntm == -1:
                        delta_dollar_1 = df_dollar[strat_tag]

                    delta_value = delta_dollar_1

                    # delta_value = delta_value * np.sign(delta_dollar_1)
                    delta_strategy = strat_tag
                    other_strategy = new_delta_strategy
                    ix_close = np.logical_and(df_close['UniqueTicker'] == ticker, df_close['Broker'] == broker)
                    close_price = df_close[ix_close]['Close'].values[0]
                    if ntm == 1:
                        if delta_value > 0:
                            trade_side_1 = 'Buy'
                        else:
                            trade_side_1 = 'Sell'
                    elif ntm == -1:
                        if delta_value > 0:
                            trade_side_1 = 'Sell'
                        else:
                            trade_side_1 = 'Buy'
                    df_temp_notional_trades = create_strategy_notional(ticker, broker, lasttradeabledate,
                                                                       lasttradeabledate, trade_side_1,
                                                                       abs(delta_value),
                                                                       close_price, delta_strategy,
                                                                       other_strategy, ex_date, exchange, instrument,
                                                                       multiplier, ntm,
                                                                       NotionalType.delta_strategy_notional)

            df_notional_trades = pd.concat([df_notional_trades,df_temp_notional_trades])

    return df_notional_trades


def generate_inter_trade_notional(ex_date, database, dollar_delta_threshold, currency_list):
    """

    :param ex_date:
    :param database:
    :return:
    """
    df_delta_local, df_delta_dollar = hp.VaR.generate_delta_report(ex_date, database)
    df_mis = database.get_mis(ex_date)
    ix_tick = np.logical_or(df_mis.SecurityType == 'FWD', df_mis.SecurityType == 'FUT')
    ticker_for_close = df_mis[ix_tick].UniqueTicker.tolist()
    df_close = database.get_close_price(ticker_for_close, ex_date)
    df_notional_trades = pd.DataFrame()
    df_remark = pd.DataFrame(columns=['Currency', 'Date', 'Delta_Value', 'Remarks'])
    cur_list = df_delta_dollar.columns.tolist()
    ino_list = list(set(INO_LIST).intersection(cur_list))
    for currency in currency_list:
        df_temp_notional_trades = pd.DataFrame()
        if currency == 'INR' and df_delta_dollar.columns.isin(ino_list).any():
            df_ino_test = df_delta_dollar[ino_list ]
        else:
            df_ino_test = pd.DataFrame()

        df_local = df_delta_local[currency]
        df_dollar = df_delta_dollar[currency]
        ix2 = df_dollar.index.str.contains('ARBITRAGE', regex=False)
        ix2 = np.logical_and(ix2, df_dollar.index.str.contains('INR', regex=False))
        df_dollar = df_dollar.loc[ix2]
        # region Check Delta Value Condition
        transfer_check_positive = (df_dollar > dollar_delta_threshold)
        transfer_check_negative = (df_dollar < dollar_delta_threshold * -1.0)

        strat_check = len(transfer_check_positive[transfer_check_positive == True]) == 1 and len(
            transfer_check_negative[transfer_check_negative == True]) == 1
        inr_check = False
        if currency == 'INR':
            check1 = ((abs(df_ino_test) > 500000).any()).any()
            # check2 = (abs(
            #     df_dollar[df_dollar.index.str.contains('OPTIONS', regex=False)]) >= dollar_delta_threshold).any()
            check2 = True
            inr_check = np.logical_and(check1, check2)

        # endregion
        if strat_check or inr_check:
            # check forward
            if inr_check:
                ix1 = df_dollar.index.str.contains('OPTIONS', regex=False)
                if ix1.any():
                    strategy_positive = df_dollar[ix1].index[0]
                else:
                    strategy_positive = None
                ix1 = np.logical_and(df_dollar.index.str.contains('LINEAR', regex=False), df_dollar != 0)
                if ix1.any():
                    strategy_negative = df_dollar[ix1].index[0]
                else:
                    strategy_negative = None

            else:
                strategy_positive = df_dollar[transfer_check_positive].index[0]
                strategy_negative = df_dollar[transfer_check_negative].index[0]

            if strategy_negative!=None and strategy_positive!=None:
                ix_strategy = np.logical_or(df_mis.StrategyTag.str.contains(strategy_positive, regex=False),
                                            df_mis.StrategyTag.str.contains(strategy_negative, regex=False))
                df_mis_temp = df_mis[ix_strategy]
                ticker_details = get_transfer_ticker(df_mis_temp.copy(), currency, database, ex_date, True,
                                                     strategy_positive, strategy_negative,False)

                if not ticker_details.empty:
                    ticker, broker, lasttradeabledate, exchange, ntm, instrument, multiplier = ticker_details[
                        ['UniqueTicker', 'Broker', 'LastTradeableDate', 'Exchange', 'Notional_Trade_Mapping',
                         'SecurityType', 'Multiplier']]
                    if ntm == 1:
                        delta_dollar_1 = df_local[strategy_positive]
                        delta_dollar_2 = df_local[strategy_negative]
                    elif ntm == -1:
                        delta_dollar_1 = df_dollar[strategy_positive]
                        delta_dollar_2 = df_dollar[strategy_negative]

                    delta_value = min(abs(delta_dollar_1), abs(delta_dollar_2))
                    if inr_check:
                        if ntm == 1:
                            delta_value = df_local[strategy_negative]
                        elif ntm == -1:
                            delta_value = df_dollar[strategy_negative]
                        delta_strategy = strategy_negative
                        other_strategy = strategy_positive
                    else:
                        if delta_value == delta_dollar_2 * -1:
                            delta_value = delta_value * -1
                            delta_strategy = strategy_negative
                            other_strategy = strategy_positive
                        else:
                            delta_strategy = strategy_positive
                            other_strategy = strategy_negative

                    ix_close = np.logical_and(df_close['UniqueTicker'] == ticker, df_close['Broker'] == broker)
                    close_price = df_close[ix_close]['Close'].values[0]
                    if ntm == 1:
                        if delta_value > 0:
                            trade_side_1 = 'Buy'
                        else:
                            trade_side_1 = 'Sell'
                    elif ntm == -1:
                        if delta_value > 0:
                            trade_side_1 = 'Sell'
                        else:
                            trade_side_1 = 'Buy'

                    df_temp_notional_trades = create_strategy_notional(ticker, broker, lasttradeabledate,
                                                                       lasttradeabledate, trade_side_1,
                                                                       abs(delta_value),
                                                                       close_price, delta_strategy,
                                                                       other_strategy, ex_date, exchange, instrument,
                                                                       multiplier, ntm,
                                                                       NotionalType.inter_strategy_notional)

        df_notional_trades = pd.concat([df_notional_trades, df_temp_notional_trades])

        # if forward not available else check for Fut

    return df_notional_trades


def generate_all_notional(ex_date, database, dollar_delta_threshold, currency_list):
    """

    :param ex_date:
    :param database:
    :return:
    """
    df_delta, df_delta_dollar = hp.VaR.generate_delta_report(ex_date, database)
    df_mis = database.get_mis(ex_date)
    ix_tick_fwd = df_mis.SecurityType == 'FWD'
    ix_tick_fut = df_mis.SecurityType == 'FUT'
    ix_tick_fwd = np.logical_or(ix_tick_fwd,ix_tick_fut)
    fwd_ticker = df_mis[ix_tick_fwd].UniqueTicker.tolist()
    df_close = database.get_close_price(fwd_ticker, ex_date)
    # df_close.set_index(['UniqueTicker'])
    df_notional_trades = pd.DataFrame()
    df_remark = pd.DataFrame(columns=['Currency', 'Date', 'Delta_Value', 'Remarks'])
    cur_list = df_delta_dollar.columns.tolist()
    ino_list = list(set(INO_LIST).intersection(cur_list))
    for currency in currency_list:
        if currency == 'INR' and df_delta_dollar.columns.isin(ino_list).any():
            df_ino_test = df_delta_dollar[ino_list]
        else:
            df_ino_test = pd.DataFrame()

        df_local = df_delta[currency]
        df_dollar = df_delta_dollar[currency]

        treasury_stratgey_tag = 'TREASURY|FX|CASH|{}'.format(currency)
        ix_treasury = df_dollar.index.str.contains(treasury_stratgey_tag, regex=False)
        df_dollar_treasury = df_dollar[ix_treasury]
        df_local_treasury = df_local[ix_treasury]
        total_teasury_delta_dollar = df_dollar_treasury.sum()
        total_teasury_delta_local = df_local_treasury.sum()
        ix_arb = df_dollar.index.str.contains('ARBITRAGE', regex=False)
        ix_arb = np.logical_and(ix_arb,df_dollar.index.str.contains('INR', regex=False))
        df_arb = df_dollar[ix_arb]
        ix_check = np.logical_or(df_arb > dollar_delta_threshold, df_arb < dollar_delta_threshold * -1)
        df_arb = df_arb[ix_check]
        df_arb.sort_index(ascending=False,inplace=True)
        for strat_tag , delta_value in df_arb.iteritems():
            df_temp_notional_trades = pd.DataFrame()
            inr_check = True
            if currency == 'INR':
                ino_sum =  ((abs(df_ino_test) > 1000000).any()).any()
                if ino_sum and ('OPTIONS' in strat_tag) and delta_value!=0:
                    inr_check = False

            if np.sign(delta_value) != np.sign(total_teasury_delta_dollar) and abs(
                    total_teasury_delta_dollar) > dollar_delta_threshold and inr_check:
                ix_strategy = np.logical_or(df_mis.StrategyTag.str.contains(strat_tag, regex=False),
                                            df_mis.StrategyTag.str.contains(treasury_stratgey_tag, regex=False))
                df_mis_temp = df_mis[ix_strategy]
                ticker_details = get_transfer_ticker(df_mis_temp.copy(), currency, database, ex_date, True,
                                                     strat_tag, treasury_stratgey_tag,True)
                if not ticker_details.empty:
                    ticker, broker, lasttradeabledate, exchange, ntm, instrument, multiplier = ticker_details[
                        ['UniqueTicker', 'Broker', 'LastTradeableDate', 'Exchange', 'Notional_Trade_Mapping',
                         'SecurityType', 'Multiplier']]

                    if ntm == 1:
                        delta_dollar_1 = df_local[strat_tag]
                        delta_dollar_2 = total_teasury_delta_local
                    elif ntm == -1:
                        delta_dollar_1 = df_dollar[strat_tag]
                        delta_dollar_2 = total_teasury_delta_dollar

                    delta_value = min(abs(delta_dollar_1), abs(delta_dollar_2))

                    if delta_value == abs(delta_dollar_2):
                        reduction_delta_local = total_teasury_delta_local * np.sign(delta_dollar_1)
                        reduction_delta_dollar = total_teasury_delta_dollar * np.sign(delta_dollar_1)
                    elif delta_value == abs(delta_dollar_1):
                        reduction_delta_local = df_local[strat_tag] * np.sign(delta_dollar_1)
                        reduction_delta_dollar = df_dollar[strat_tag]* np.sign(delta_dollar_1)


                    delta_value = delta_value * np.sign(delta_dollar_1)
                    delta_strategy = strat_tag
                    other_strategy = treasury_stratgey_tag
                    ix_close = np.logical_and(df_close['UniqueTicker'] == ticker, df_close['Broker'] == broker)
                    close_price = df_close[ix_close]['Close'].values[0]
                    if ntm == 1:
                        if delta_value > 0:
                            trade_side_1 = 'Buy'
                        else:
                            trade_side_1 = 'Sell'
                    elif ntm == -1:
                        if delta_value > 0:
                            trade_side_1 = 'Sell'
                        else:
                            trade_side_1 = 'Buy'
                    df_temp_notional_trades = create_strategy_notional(ticker, broker, lasttradeabledate,
                                                                       lasttradeabledate, trade_side_1,
                                                                       abs(delta_value),
                                                                       close_price, delta_strategy,
                                                                       other_strategy, ex_date, exchange, instrument,
                                                                       multiplier, ntm,
                                                                       NotionalType.treasury_notional)
                    total_teasury_delta_local += reduction_delta_local
                    total_teasury_delta_dollar += reduction_delta_dollar
            df_notional_trades = pd.concat([df_notional_trades,df_temp_notional_trades])

    return df_notional_trades


if __name__ == '__main__':
    ex_date = dt.datetime(2019, 9, 10)

    base_path = os.path.join(os.path.abspath(r'\\192.168.10.2\aia-drive\MIS\Ariana_MIS'), 'Database')
    db_name = 'DELTA_MIS_DB'
    db = hp.Database(base_path, db_name)
    db.conncet_db()
    df_date_list = db.get_query_df(
        "select Date from DateList where Date>='2020-03-13 00:00:00' and Date<'2020-07-29 00:00:00' order by Date")
    df_date_list['Date'] = pd.to_datetime(df_date_list['Date'])
    df_date_list.set_index(['Date'], inplace=True)
    currency_list = ['CNH', 'CNY', 'HKD', 'IDR', 'INR', 'JPY', 'KRW', 'MXN', 'MYR', 'RUB',
                     'SGD', 'THB', 'TRY', 'TWD']
    currency_list = ['INR']
    for ex_date in df_date_list.index.tolist():

        runList = [0,1]
        for run1 in runList:

            db.delete_mis_entries_for_date(ex_date)
            FreshMIS.generate_mis(ex_date, db, db.db_connection)
            # generate notional_entries
            if run1 == 0:
                print("Processing for {}".format("InterStrategy"))
                df_p = generate_inter_trade_notional(ex_date, db, 50000, currency_list)

            elif run1 == 1:
                print("Processing for {}".format("IntraStrategy"))
                df_p = generate_all_notional(ex_date, db, 50000, currency_list)

            elif run1 == 2:
                print("Processing for {}".format("Delta"))
                df_p =generate_delta_notional(ex_date, db, 50000*2, currency_list)

            if not df_p.empty:
                try:
                    df_p.to_sql(TableName.Trade_Table.value, db.db_connection, index=False, if_exists='append')
                except:
                    pass
            # delete mis entries
            db.delete_mis_entries_for_date(ex_date)
            # run mis again
            FreshMIS.generate_mis(ex_date, db, db.db_connection)
            # generate delta_report


    # df_p.to_clipboard()
