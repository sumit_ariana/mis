# region Import
import datetime as dt
from builtins import str
import pandas as pd
import os
from helper import TableName
import numpy as np
import time
from helper import Database
from helper import Menu
from dateutil import relativedelta
import helper as hp

# endregion

# region Global Variables
DB_NAME = "Test_DB_MIS"
QUANTO = "Quanto"
NON_QUANTO_ROUNDING = int(12)
# DB_MIS = Database(DB_NAME)
FINAL_MIS_TABLE = "FINAL_MIS"
TRADE_TABLE = "Auto"

UNI_INDEX = ['UniqueTicker', 'Broker', 'Exchange', 'StrategyTag', 'SecurityType']

MAPPING_INDEX = ['UniqueTicker', 'Broker', 'Exchange']
MIS_COLUMNS = ['MIS_DATE', 'UniqueTicker', 'Broker', 'Exchange', 'StrategyTag', 'SecurityType',
               'LastTradeableDate', 'NET_QTY', 'NET_VALUE', 'TodayPnL_Local', 'TodayPnL_Dollar', 'PreviousPnL_Local',
               'PreviousPnL_Dollar', 'Expired', 'FTD_BROKER', 'FTD_LOCAL', 'FTD_DOLLAR', 'EXPIRED_ON', 'DeltaPnL',
               'Cumm_PnL_Local', 'Cumm_PnL_Broker']

REPORT_COLUMNS = ['Date', 'UniqueTicker', 'Broker', 'StrategyTag', 'SecurityType', 'LastTradeableDate',
                  'L1_Delta', 'L2_Delta', 'L3_Delta']


# endregion

def delete_mis_for_date(ex_date, db):
    """

    :param ex_date:
    :return:
    """
    # query_str = "DELETE from FINAL_MIS WHERE MIS_DATE = {d_date}".format(d_date=(ex_date))
    query_str = "DELETE  from FINAL_MIS WHERE MIS_DATE = '{d_date}'".format(d_date=(ex_date))
    db.run_query(query_str)


# region Get Table Info
def get_previous_date(ex_date, db_connection):
    """

    :param db_connection:
    :return:
    """
    start_date = ex_date - relativedelta.relativedelta(days=10)
    query_str = "SELECT DISTINCT Date from {t_name} WHERE Date BETWEEN '{s_date}' and '{e_date}'".format(
        s_date=str(start_date), e_date=str(ex_date), t_name=TableName.Date_List_Table.value)
    df_data = pd.read_sql(query_str, db_connection, parse_dates=['Date'])
    df_data.sort_values('Date', inplace=True)
    if ex_date not in df_data.Date.tolist():
        raise Exception("Date {} not in Date List Table".format(str(ex_date)))
    if len(df_data) > 1:
        previous_date = df_data.Date.tolist()[-2]
    else:
        previous_date = df_data.Date.tolist()[-1]
    return previous_date


def get_close_price(ticker_list, ex_date, previous_date, db_connection):
    """

    :param ex_date:
    :return:
    """
    if len(ticker_list) == 1:
        t_list = "('{t}')".format(t=ticker_list[0])
    else:
        t_list = tuple(ticker_list)
    query_str = f"""SELECT  * FROM '{TableName.Close_Table.value}' WHERE Date BETWEEN '{previous_date}' AND '{ex_date}' AND UniqueTicker IN {t_list}"""
    df_close = pd.read_sql(query_str, con=db_connection)
    df_close['Date'] = pd.to_datetime(df_close['Date'])
    df_close[df_close.ConversionFactor == 0] = 1.0
    df_close[df_close.RoundingFactor == 0] = NON_QUANTO_ROUNDING
    df_close.ConversionFactor.fillna(value=1.0, inplace=True)
    df_close.RoundingFactor.fillna(value=NON_QUANTO_ROUNDING, inplace=True)
    return df_close


def get_spot_price(previous_date, ex_date, db_connection):
    """

    :param ex_date:
    :return:
    """

    query_str = f"""SELECT  * FROM '{TableName.Close_Table.value}' WHERE Date BETWEEN '{previous_date}' AND '{ex_date}' and SecurityType='SPOT'"""
    df_close = pd.read_sql(query_str, con=db_connection)
    df_close['Date'] = pd.to_datetime(df_close['Date'])
    df_close[df_close.ConversionFactor == 0] = 1.0
    df_close[df_close.RoundingFactor == 0] = NON_QUANTO_ROUNDING
    df_close.ConversionFactor.fillna(value=1.0, inplace=True)
    df_close.RoundingFactor.fillna(value=NON_QUANTO_ROUNDING, inplace=True)
    return df_close


def get_mapping_table(ticker_list, db_connection):
    """
    :param db_connection
    :return: Mapping Table
    """
    if len(ticker_list) == 1:
        t_list = "('{t}')".format(t=ticker_list[0])
    else:
        t_list = tuple(ticker_list)
    query_str = f"""select *  from '{TableName.Mapping_Table.value}' where UniqueTicker in {t_list}"""
    df_data = pd.read_sql(query_str, db_connection)
    return df_data


def get_previous_day_position(prev_date, db_connection):
    """

    :param db_connection:
    :return:
    """
    query_string = "select * from {t_name} where MIS_DATE = '{d_date}'".format(t_name=TableName.Final_MIS_Table.value,
                                                                               d_date=(prev_date))
    df_data = pd.read_sql(query_string, db_connection)
    # region Converting String to Datetime
    df_data['MIS_DATE'] = pd.to_datetime(df_data['MIS_DATE'])
    df_data['LastTradeableDate'] = pd.to_datetime(df_data['LastTradeableDate'])
    df_data['EXPIRED_ON'] = pd.to_datetime(df_data['EXPIRED_ON'])
    df_data['Expired'] = df_data['Expired'].astype(bool)
    # endregion
    return df_data


def get_trade_report(ex_date, db_connection):
    """

    :param db_connection:
    :return:
    """
    query_string = "select * from {t_name} where TradeDate = '{d_date}'".format(t_name=TableName.Trade_Table.value,
                                                                                d_date=ex_date)
    df_data = pd.read_sql(query_string, db_connection)
    # region Converting String to Datetime
    df_data['TradeDate'] = pd.to_datetime(df_data['TradeDate'])
    df_data['LastTradeableDate'] = pd.to_datetime(df_data['LastTradeableDate'])
    df_data['ValueDate'] = pd.to_datetime(df_data['ValueDate'])
    ix1 = df_data.TransactionType == 'Sell'
    df_data.loc[ix1, 'FilledQty'] *= -1.0
    df_data.loc[np.logical_not(ix1), 'Value'] *= -1.0
    if df_data.empty:
        l_columns = UNI_INDEX.copy()
        l_columns.append('FilledQty')
        l_columns.append('Value')
        l_columns.append('LastTradeableDate')
        df_data = df_data[l_columns]
    else:
        df_data = df_data.groupby(by=UNI_INDEX).agg(
            {'FilledQty': np.sum, 'Value': np.sum, 'LastTradeableDate': 'first'})
        df_data.reset_index(drop=False, inplace=True)
    # endregion

    return df_data


def get_cash_report(ex_date, db_connection):
    """

    :param ex_date:
    :param db_connection:
    :return:
    """
    t_date = pd.to_datetime(ex_date)
    query_string = "select * from {t_name} where Date = '{d_date}'".format(t_name=TableName.Daily_Cash_Table.value,
                                                                           d_date=str(t_date))
    df_data = pd.read_sql(query_string, db_connection)
    df_data['Date'] = pd.to_datetime(df_data['Date'])

    return df_data


def get_expense(ex_date, db_connection):
    """

    :param ex_date:
    :param db_connection:
    :return:
    """
    t_date = pd.to_datetime(ex_date)
    query_string = "select * from {t_name} where Date = '{d_date}'".format(t_name=TableName.Expense.value,
                                                                           d_date=t_date)
    df_data = pd.read_sql(query_string, db_connection)
    df_data['Date'] = pd.to_datetime(df_data['Date'])

    return df_data


def get_subscribe_redeem(ex_date, prev_date, db_connection):
    """

    :param ex_date:
    :param db_connection:
    :return:
    """
    t_date = pd.to_datetime(ex_date)
    query_string = "select * from {t_name} where NAV_Date = '{d_date}' or Broker_Date= '{d_date}'".format(
        t_name=TableName.Subscription_Redemption_Table.value, d_date=t_date, p_date=prev_date)
    df_data = pd.read_sql(query_string, db_connection)
    df_data['Broker_Date'] = pd.to_datetime(df_data['Broker_Date'])
    df_data['NAV_Date'] = pd.to_datetime(df_data['NAV_Date'])

    return df_data


def get_cash_mis(ex_date, db_connection):
    """

    :param ex_date:
    :param db_connection:
    :return:
    """
    t_date = pd.to_datetime(ex_date)
    query_string = "select * from {t_name} where Date = '{d_date}'".format(t_name=TableName.Cash_MIS_Table.value,
                                                                           d_date=t_date)
    df_data = pd.read_sql(query_string, db_connection)
    df_data['Date'] = pd.to_datetime(df_data['Date'])

    return df_data


# endregion

# region Mapping
def map_calculation_type(df_data, df_mapping):
    """

    :param df_data:
    :param df_mapping:
    :return:
    """
    df_temp = df_data.copy()
    df_temp.reset_index(inplace=True, drop=False)
    df_temp.set_index(MAPPING_INDEX, inplace=True)
    df_mapping.reset_index(inplace=True, drop=True)
    df_mapping.set_index(MAPPING_INDEX, inplace=True)
    df_mapping = df_mapping.loc[~df_mapping.index.duplicated(keep='first')]
    # df_data = pd.concat([df_data, df_mapping], axis=1, join='outer')

    df_data[['PNL_Type1', 'PNL_Type2', 'Multiplier', 'Is_Quanto', 'To_Include_PnL', 'To_Include_Value']] = df_mapping[
        ['PnLCurrency1', 'PnLCurrency2', 'Multiplier', 'Is_Quanto', 'To_Include_PnL', 'To_Include_Value']]
    df_data.reset_index(inplace=True, drop=False)
    ix1 = np.logical_and(df_data['PNL_Type1'].isnull(), df_data['PNL_Type2'].isnull())
    if not ix1.empty:
        if ix1.any():
            stock_name = df_data.loc[ix1, 'UniqueTicker'].values[0]
            raise Exception("PNL Type Cannot be null for {}".format(stock_name))
    return df_data


def map_close(df_data, df_close, date, check_null):
    """

    :param df_data:
    :param df_close:
    :return:
    """
    # df_data.reset_index(drop=False,inplace=True)
    df_data.set_index(['UniqueTicker', 'Broker'], inplace=True)
    df_close.set_index(['UniqueTicker', 'Broker'], inplace=True)
    df_close_temp = df_close[df_close.Date == date]
    columms_to_get = ['Close', 'BrokerClose', 'Conversion', 'Type', 'RoundingFactor', 'ConversionFactor']
    func_t = lambda x: str(date) + "_" + x
    columns_to_set = list(map(func_t, columms_to_get))
    df_close_temp = df_close_temp.loc[~df_close_temp.index.duplicated(keep='first')]
    df_data[columns_to_set] = df_close_temp[columms_to_get]
    close_column = str(date) + "_" + 'Close'
    broker_close_column = str(date) + "_" + 'BrokerClose'
    ix1 = (df_data['SecurityType'] == 'EQ') & (df_data['NET_VALUE'] == df_data['PREV_VALUE'])
    ix1 = np.logical_or(ix1, df_data['LastTradeableDate'] < date)
    ix2 = np.logical_or(df_data[close_column].isnull(), df_data[broker_close_column].isnull())
    ix2 = np.logical_and(ix2, np.logical_not(ix1))
    if check_null and ix2.any():
        p = ix2[ix2 == True]
        p.to_clipboard()
        raise Exception("CLose or  Broker Close is not available for atleast on Scrip")
    return df_data.reset_index(drop=False)


# endregion

# region Cash
def generate_cash_entry(df_cash_ftd, ex_date, prev_date, mis_columns, db_connection):
    """

    :param df_cash_ftd:
    :param df_close:
    :param mis_columns:
    :return:
    """
    index_columns = ["Date", "Broker", "Currency"]

    df_mis = pd.DataFrame(columns=mis_columns)

    df_temp_cash = df_cash_ftd.copy()
    ix1 = np.logical_and(df_temp_cash.FTD == 0, df_temp_cash.Prev_Cash == 0)
    ix1 = np.logical_and(df_temp_cash.Cash == 0, ix1)
    ix1 = np.logical_and(df_temp_cash.Open_Value == 0, ix1)
    df_temp_cash.loc[ix1] = np.nan
    df_temp_cash.dropna(inplace=True)
    df_temp_close = get_spot_price(prev_date, ex_date, db_connection)
    for ix, rowval in df_temp_cash.iterrows():
        currency = rowval['Currency']
        broker = rowval['Broker']
        prev_value = rowval['Prev_Cash'] + (rowval['Open_Value'] * -1.0) - rowval['prev_total_pnl_local']
        # prev_value = rowval['Prev_Cash']
        current_cash = rowval['Cash']
        if currency in ['AUD', 'GBP', 'EUR', 'XAU', 'XAG']:
            p = currency + '/USD'
        elif currency == 'USD':
            p = currency
        else:
            p = 'USD/' + currency
        ix1 = (df_temp_close.UniqueTicker == p) & (df_temp_close.Date == ex_date)
        try:
            conversion = df_temp_close[ix1]['Conversion'].values[0]
            todays_close = df_temp_close[ix1]['Close'].values[0]
        except:
            pass
        if round(prev_value, 6) != 0:
            try:
                ix2 = (df_temp_close.UniqueTicker == p) & (df_temp_close.Date == prev_date)
                prev_close = df_temp_close[ix2]['Close'].values[0]
                if '/USD' in p:
                    delta = prev_value * (todays_close - prev_close)
                else:
                    delta = prev_value * (1 / todays_close - 1 / prev_close)
            except:
                raise Exception("Issue with currency cash generation for currency :- {}".format(p))
        else:
            delta = 0.0

        ftd_local = rowval['FTD']
        if p in ["GBP/USD", "EUR/USD", "AUD/USD"]:
            ftd_usd = ftd_local * conversion
        else:
            ftd_usd = ftd_local / conversion

        ftd_usd += delta

        if ftd_usd != 0 or current_cash != 0:
            df_mis.loc[len(df_mis)] = [ex_date, p, broker, "OTC",
                                       "TREASURY|FX|CASH|{}|FX-SPOT".format(currency.upper()),
                                       'SPOT', ex_date, 0, current_cash, ftd_local, ftd_usd, 0, 0, True, 0, ftd_local,
                                       ftd_usd,
                                       ex_date, delta, 0, 0]
    return df_mis


def generate_cash(ex_date, prev_date, df_mis_ftd, db_connection):
    """

    :param ex_date:
    :param df_mis_ftd:
    :return:
    """
    index_columns = ["Date", "Broker", "Currency"]
    broker_sub_red_columns = ['Broker_Date','Broker','Currency']
    nav_sub_red_columns = ['NAV_Date', 'Broker', 'Currency']

    df_prev_cash_mis = get_cash_mis(prev_date, db_connection)
    df_prev_cash_mis['Date'] = ex_date
    df_cash_report = get_cash_report(ex_date, db_connection)
    df_expense_report = get_expense(ex_date, db_connection)
    df_subscription = get_subscribe_redeem(ex_date, prev_date, db_connection)

    df_prev_mis = get_previous_day_position(prev_date,db_connection)
    df_mapping = get_mapping_table(df_prev_mis.UniqueTicker.tolist(),db_connection)
    df_mapping.drop_duplicates(subset='UniqueTicker', inplace=True)
    df_prev_mis.set_index('UniqueTicker', inplace=True)
    df_mapping.set_index('UniqueTicker', inplace=True)
    df_prev_mis['To_Include_PnL'] = df_mapping['To_Include_PnL']
    df_prev_mis['PNL_Type1'] = df_mapping['PnLCurrency1']
    df_prev_mis['PNL_Type2'] = df_mapping['PnLCurrency2']

    # region Generating Basic Cash
    df_cash_report = df_cash_report.groupby(by=index_columns).agg({'Cash': np.sum})

    df_expense_report = df_expense_report.groupby(by=index_columns).agg({'Cash': np.sum})
    df_expense_report.rename({'Cash': 'Expense'}, inplace=True, axis=1)

    df_broker_subscription = df_subscription.groupby(by=broker_sub_red_columns).agg(
        {'Subscriptions': np.sum, 'Redemptions': np.sum})

    df_broker_subscription['Broker_Sub_Red'] = df_broker_subscription['Redemptions'] + df_broker_subscription['Subscriptions']
    df_broker_subscription.drop(['Subscriptions', 'Redemptions'], inplace=True, axis=1)
    df_broker_subscription.reset_index(drop=False , inplace=True)
    df_broker_subscription.rename({'Broker_Date': 'Date'}, inplace=True, axis=1)
    if not df_broker_subscription.empty:
        df_broker_subscription = df_broker_subscription[df_broker_subscription['Date']==ex_date]
        df_broker_subscription.set_index(index_columns,inplace=True)

    df_nav_subscription = df_subscription.groupby(by=nav_sub_red_columns).agg(
        {'Subscriptions': np.sum, 'Redemptions': np.sum})

    df_nav_subscription['NAV_Sub_Red'] = df_nav_subscription['Redemptions'] + df_nav_subscription['Subscriptions']
    df_nav_subscription.drop(['Subscriptions', 'Redemptions'], inplace=True, axis=1)
    df_nav_subscription.reset_index(drop=False,inplace=True)
    df_nav_subscription.rename({'NAV_Date': 'Date'}, inplace=True, axis=1)
    if not df_nav_subscription.empty:
        df_nav_subscription = df_nav_subscription[df_nav_subscription['Date'] == ex_date]
        df_nav_subscription.set_index(index_columns, inplace=True)

    df_total = pd.DataFrame(columns=df_prev_cash_mis.columns)
    df_total.set_index(index_columns, inplace=True)

    df_prev_cash_mis = df_prev_cash_mis.groupby(by=index_columns).agg({'Total': np.sum,'Total_Broker':np.sum})
    df_prev_cash_mis.rename({'Total': 'Prev_Cash'}, inplace=True, axis=1)
    df_prev_cash_mis.rename({'Total_Broker': 'Prev_Cash_Broker'}, inplace=True, axis=1)

    if not df_broker_subscription.empty:
        df_total = pd.merge(df_total, df_broker_subscription['Broker_Sub_Red'].to_frame(), left_index=True, right_index=True,
                            how='outer')
    else:
        df_total['Broker_Sub_Red'] = 0.0

    if not df_nav_subscription.empty:
        df_total = pd.merge(df_total, df_nav_subscription['NAV_Sub_Red'].to_frame(), left_index=True, right_index=True,
                            how='outer')
    else:
        df_total['NAV_Sub_Red'] = 0.0

    if not df_cash_report.empty:
        df_total = pd.merge(df_total, df_cash_report['Cash'].to_frame(), left_index=True, right_index=True, how='outer')
    else:
        df_total['Cash'] = 0.0

    if not df_expense_report.empty:
        df_total = pd.merge(df_total, df_expense_report, left_index=True, right_index=True, how='outer')
    else:
        df_total['Expense'] = 0.0

    if not df_prev_cash_mis.empty:
        df_total = pd.merge(df_total, df_prev_cash_mis, left_index=True, right_index=True, how='outer')
    else:
        df_total['Prev_Cash'] = 0.0
        df_total['Prev_Cash_Broker'] = 0.0

    # endregion

    # region Expired Contract
    ix1 = np.logical_or(df_mis_ftd['To_Include_Value'] == 1, df_mis_ftd['To_Include_PnL'] == 1)
    ix1 = np.logical_and(df_mis_ftd['Expired'] == True, np.logical_not(ix1))
    df_temp = df_mis_ftd[ix1]
    df_temp.rename({'MIS_DATE': 'Date'}, inplace=True, axis=1)

    df_pnl_type_1 = df_temp.groupby(by=['Date', 'Broker', 'PNL_Type1']).agg({'NET_QTY': np.sum})
    df_pnl_type_1.reset_index(inplace=True)
    df_pnl_type_1.rename({'PNL_Type1': 'Currency', 'NET_QTY': 'Expired_Contract'}, inplace=True, axis=1)
    df_pnl_type_1.set_index(['Date', 'Broker', 'Currency'], inplace=True)
    if not df_pnl_type_1.empty:
        # if df_total.empty:
        #     z = False
        #     how_val = 'right'
        # else:
        #     z = True
        #     how_val = 'outer'
        df_total = pd.merge(df_total, df_pnl_type_1, left_index=True, right_index=True, how='outer')
    else:
        df_total['Expired_Contract'] = 0

    df_pnl_type_2 = df_temp.groupby(['Date', 'Broker', 'PNL_Type2']).agg({'NET_VALUE': np.sum})
    df_pnl_type_2.reset_index(inplace=True)
    df_pnl_type_2.rename({'PNL_Type2': 'Currency', 'NET_VALUE': 'Expired_Contract'}, inplace=True, axis=1)
    df_pnl_type_2.set_index(['Date', 'Broker', 'Currency'], inplace=True)
    if not df_pnl_type_2.empty:
        df_total = pd.merge(df_total, df_pnl_type_2, left_index=True, right_index=True, how='outer')
    else:
        df_total['Expired_Contract'] = 0

    # endregion

    # region PnL Part
    ix1 = df_mis_ftd['To_Include_PnL'] == 1
    df_temp = df_mis_ftd[ix1]
    ix2 = np.logical_and(np.logical_not(df_temp['PNL_Type1'].isnull()), np.logical_not(df_temp['PNL_Type2'].isnull()))
    if ix2.any():
        raise Exception("Cannot have both PNL Types for Cash Inclusion")
    if not df_temp.empty:
        df_temp.rename({'MIS_DATE': 'Date'}, inplace=True, axis=1)
        df_pnl_type_1 = df_temp.groupby(by=['Date', 'Broker', 'PNL_Type1']).agg({'FTD_LOCAL': np.sum})
        df_pnl_type_2 = df_temp.groupby(['Date', 'Broker', 'PNL_Type2']).agg({'FTD_LOCAL': np.sum})
        df_pnl_type_1.reset_index(inplace=True)
        df_pnl_type_2.reset_index(inplace=True)
        df_pnl_type_1.rename({'PNL_Type1': 'Currency', 'FTD_LOCAL': 'Contract_PnL'}, inplace=True, axis=1)
        df_pnl_type_2.rename({'PNL_Type2': 'Currency', 'FTD_LOCAL': 'Contract_PnL'}, inplace=True, axis=1)
        df_pnl_type_1.set_index(['Date', 'Broker', 'Currency'], inplace=True)
        df_pnl_type_2.set_index(['Date', 'Broker', 'Currency'], inplace=True)
        if not df_pnl_type_1.empty:
            df_total = pd.merge(df_total, df_pnl_type_1, left_index=True, right_index=True, how='outer')
        else:
            df_total['Contract_PnL'] = 0
        if not df_pnl_type_2.empty:
            df_total = pd.merge(df_total, df_pnl_type_2, left_index=True, right_index=True, how='outer')
        else:
            df_total['Contract_PnL'] = 0
        column_list = df_total.columns.tolist()
        if 'Contract_PnL_x' in column_list and 'Contract_PnL_y' in column_list:
            if 'Contract_PnL' in column_list:
                df_total['Contract_PnL'] = df_total['Contract_PnL'].fillna(value=0.0) + df_total[
                    'Contract_PnL_x'].fillna(value=0.0) + df_total['Contract_PnL_y'].fillna(value=0.0)
            else:
                df_total['Contract_PnL'] = df_total['Contract_PnL_x'].fillna(value=0.0) + df_total[
                    'Contract_PnL_y'].fillna(value=0.0)
            df_total.drop(['Contract_PnL_x', 'Contract_PnL_y'], inplace=True, axis=1)
    # endregion

    # region PnL Part
    ix1 = df_mis_ftd['To_Include_PnL'] == 1
    df_temp = df_mis_ftd[ix1]
    ix2 = np.logical_and(np.logical_not(df_temp['PNL_Type1'].isnull()),
                         np.logical_not(df_temp['PNL_Type2'].isnull()))
    if ix2.any():
        raise Exception("Cannot have both PNL Types for Cash Inclusion")
    if not df_temp.empty:
        df_temp.rename({'MIS_DATE': 'Date'}, inplace=True, axis=1)
        df_pnl_type_1 = df_temp.groupby(by=['Date', 'Broker', 'PNL_Type1']).agg({'FTD_BROKER': np.sum})
        df_pnl_type_2 = df_temp.groupby(['Date', 'Broker', 'PNL_Type2']).agg({'FTD_BROKER': np.sum})
        df_pnl_type_1.reset_index(inplace=True)
        df_pnl_type_2.reset_index(inplace=True)
        df_pnl_type_1.rename({'PNL_Type1': 'Currency', 'FTD_BROKER': 'Contract_PnL_Broker'}, inplace=True, axis=1)
        df_pnl_type_2.rename({'PNL_Type2': 'Currency', 'FTD_BROKER': 'Contract_PnL_Broker'}, inplace=True, axis=1)
        df_pnl_type_1.set_index(['Date', 'Broker', 'Currency'], inplace=True)
        df_pnl_type_2.set_index(['Date', 'Broker', 'Currency'], inplace=True)
        if not df_pnl_type_1.empty:
            df_total = pd.merge(df_total, df_pnl_type_1, left_index=True, right_index=True, how='outer')
        else:
            df_total['Contract_PnL_Broker'] = 0
        if not df_pnl_type_2.empty:
            df_total = pd.merge(df_total, df_pnl_type_2, left_index=True, right_index=True, how='outer')
        else:
            df_total['Contract_PnL_Broker'] = 0
        column_list = df_total.columns.tolist()
        if 'Contract_PnL_Broker_x' in column_list and 'Contract_PnL_Broker_y' in column_list:
            if 'Contract_PnL_Broker' in column_list:
                df_total['Contract_PnL_Broker'] = df_total['Contract_PnL_Broker'].fillna(value=0.0) + df_total[
                    'Contract_PnL_Broker_x'].fillna(value=0.0) + df_total['Contract_PnL_Broker_y'].fillna(value=0.0)
            else:
                df_total['Contract_PnL_Broker'] = df_total['Contract_PnL_Broker_x'].fillna(value=0.0) + df_total[
                    'Contract_PnL_Broker_y'].fillna(value=0.0)
            df_total.drop(['Contract_PnL_Broker_x', 'Contract_PnL_Broker_y'], inplace=True, axis=1)
    # endregion

    # region Cumm PnL For Delta
    ix1 = np.logical_and(df_prev_mis['To_Include_PnL'] == 1, df_prev_mis['Expired'] != 1)
    df_temp = df_prev_mis[ix1]
    ix2 = np.logical_and(np.logical_not(df_temp['PNL_Type1'].isnull()),
                         np.logical_not(df_temp['PNL_Type2'].isnull()))
    if ix2.any():
        raise Exception("Cannot have both PNL Types for Cash Inclusion")
    if not df_temp.empty:
        df_temp.rename({'MIS_DATE': 'Date'}, inplace=True, axis=1)
        df_temp['Date'] = ex_date
        df_pnl_type_1 = df_temp.groupby(by=['Date', 'Broker', 'PNL_Type1']).agg({'Cumm_PnL_Local': np.sum})
        df_pnl_type_2 = df_temp.groupby(['Date', 'Broker', 'PNL_Type2']).agg({'Cumm_PnL_Local': np.sum})
        df_pnl_type_1.reset_index(inplace=True)
        df_pnl_type_2.reset_index(inplace=True)
        df_pnl_type_1.rename({'PNL_Type1': 'Currency', 'Cumm_PnL_Local': 'prev_total_pnl_local'}, inplace=True,
                             axis=1)
        df_pnl_type_2.rename({'PNL_Type2': 'Currency', 'Cumm_PnL_Local': 'prev_total_pnl_local'}, inplace=True,
                             axis=1)
        df_pnl_type_1.set_index(['Date', 'Broker', 'Currency'], inplace=True)
        df_pnl_type_2.set_index(['Date', 'Broker', 'Currency'], inplace=True)
        if not df_pnl_type_1.empty:
            df_total = pd.merge(df_total, df_pnl_type_1, left_index=True, right_index=True, how='outer')
        else:
            df_total['prev_total_pnl_local'] = 0
        if not df_pnl_type_2.empty:
            df_total = pd.merge(df_total, df_pnl_type_2, left_index=True, right_index=True, how='outer')
        else:
            df_total['prev_total_pnl_local'] = 0
        column_list = df_total.columns.tolist()
        if 'prev_total_pnl_local_x' in column_list and 'prev_total_pnl_local_y' in column_list:
            if 'prev_total_pnl_local' in column_list:
                df_total['prev_total_pnl_local'] = df_total['prev_total_pnl_local'].fillna(value=0.0) + df_total[
                    'prev_total_pnl_local_x'].fillna(value=0.0) + df_total['prev_total_pnl_local_y'].fillna(
                    value=0.0)
            else:
                df_total['prev_total_pnl_local'] = df_total['prev_total_pnl_local_x'].fillna(value=0.0) + df_total[
                    'prev_total_pnl_local_y'].fillna(value=0.0)
            df_total.drop(['prev_total_pnl_local_x', 'prev_total_pnl_local_y'], inplace=True, axis=1)
    else:
        df_total['prev_total_pnl_local'] = 0.0
    # endregion

    # region Value Part
    ix1 = df_mis_ftd['To_Include_Value'] == 1
    df_temp = df_mis_ftd[ix1]
    ix2 = np.logical_and(np.logical_not(df_temp['PNL_Type1'].isnull()),
                         np.logical_not(df_temp['PNL_Type2'].isnull()))
    if ix2.any():
        raise Exception("Cannot have both PNL Types for Cash Inclusion")
    if not df_temp.empty:
        df_temp.rename({'MIS_DATE': 'Date'}, inplace=True, axis=1)
        df_pnl_type_1 = df_temp.groupby(by=['Date', 'Broker', 'PNL_Type1']).agg({'T_NET_VALUE': np.sum})
        df_pnl_type_2 = df_temp.groupby(['Date', 'Broker', 'PNL_Type2']).agg({'T_NET_VALUE': np.sum})
        df_pnl_type_1.reset_index(inplace=True)
        df_pnl_type_2.reset_index(inplace=True)
        df_pnl_type_1.rename({'PNL_Type1': 'Currency', 'T_NET_VALUE': 'Contract_Value'}, inplace=True, axis=1)
        df_pnl_type_2.rename({'PNL_Type2': 'Currency', 'T_NET_VALUE': 'Contract_Value'}, inplace=True, axis=1)
        df_pnl_type_1.set_index(['Date', 'Broker', 'Currency'], inplace=True)
        df_pnl_type_2.set_index(['Date', 'Broker', 'Currency'], inplace=True)
        if not df_pnl_type_1.empty:
            df_total = pd.merge(df_total, df_pnl_type_1, left_index=True, right_index=True, how='outer')
        else:
            df_total['Contract_Value'] = 0
        if not df_pnl_type_2.empty:
            df_total = pd.merge(df_total, df_pnl_type_2, left_index=True, right_index=True, how='outer')
        else:
            df_total['Contract_Value'] = 0
        column_list = df_total.columns.tolist()
        if 'Contract_Value_x' in column_list and 'Contract_Value_y' in column_list:
            if 'Contract_Value' in column_list:
                df_total['Contract_Value'] = df_total['Contract_Value'].fillna(value=0.0) + df_total[
                    'Contract_Value_x'].fillna(value=0.0) + \
                                             df_total['Contract_Value_y'].fillna(value=0.0)
            else:
                df_total['Contract_Value'] = df_total['Contract_Value_x'].fillna(value=0.0) + df_total[
                    'Contract_Value_y'].fillna(value=0.0)
            df_total.drop(['Contract_Value_x', 'Contract_Value_y'], inplace=True, axis=1)
    # endregion

    # region Value Part2
    ix1 = np.logical_and(df_mis_ftd['To_Include_Value'] == 1, df_mis_ftd['PREV_QTY']!=0)
    df_temp = df_mis_ftd[ix1]
    ix2 = np.logical_and(np.logical_not(df_temp['PNL_Type1'].isnull()),
                         np.logical_not(df_temp['PNL_Type2'].isnull()))
    if ix2.any():
        raise Exception("Cannot have both PNL Types for Cash Inclusion")
    if not df_temp.empty:
        df_temp.rename({'MIS_DATE': 'Date'}, inplace=True, axis=1)
        df_pnl_type_1 = df_temp.groupby(by=['Date', 'Broker', 'PNL_Type1']).agg({'PREV_VALUE': np.sum})
        df_pnl_type_2 = df_temp.groupby(['Date', 'Broker', 'PNL_Type2']).agg({'PREV_VALUE': np.sum})
        df_pnl_type_1.reset_index(inplace=True)
        df_pnl_type_2.reset_index(inplace=True)
        df_pnl_type_1.rename({'PNL_Type1': 'Currency', 'PREV_VALUE': 'Open_Value'}, inplace=True, axis=1)
        df_pnl_type_2.rename({'PNL_Type2': 'Currency', 'PREV_VALUE': 'Open_Value'}, inplace=True, axis=1)
        df_pnl_type_1.set_index(['Date', 'Broker', 'Currency'], inplace=True)
        df_pnl_type_2.set_index(['Date', 'Broker', 'Currency'], inplace=True)
        if not df_pnl_type_1.empty:
            df_total = pd.merge(df_total, df_pnl_type_1, left_index=True, right_index=True, how='outer')
        else:
            df_total['Open_Value'] = 0
        if not df_pnl_type_2.empty:
            df_total = pd.merge(df_total, df_pnl_type_2, left_index=True, right_index=True, how='outer')
        else:
            df_total['Open_Value'] = 0
        column_list = df_total.columns.tolist()
        if 'Open_Value_x' in column_list and 'Open_Value_y' in column_list:
            if 'Open_Value' in column_list:
                df_total['Open_Value'] = df_total['Open_Value'].fillna(value=0.0) + df_total[
                    'Open_Value_x'].fillna(value=0.0) + \
                                             df_total['Open_Value_y'].fillna(value=0.0)
            else:
                df_total['Open_Value'] = df_total['Open_Value_x'].fillna(value=0.0) + df_total[
                    'Open_Value_y'].fillna(value=0.0)
            df_total.drop(['Open_Value_x', 'Open_Value_y'], inplace=True, axis=1)
    # endregion

    df_total.fillna(value=0, inplace=True)
    column_list = df_total.columns.tolist()
    if 'Expired_Contract_x' in column_list and 'Expired_Contract_y' in column_list:
        if 'Expired_Contract' in column_list:
            df_total['Expired_Contract'] = df_total['Expired_Contract'].fillna(value=0.0) + df_total[
                'Expired_Contract_x'].fillna(value=0.0) + df_total['Expired_Contract_y'].fillna(value=0.0)
        else:
            df_total['Expired_Contract'] = df_total['Expired_Contract_x'].fillna(value=0.0) + df_total[
                'Expired_Contract_y'].fillna(value=0.0)
        df_total.drop(['Expired_Contract_x', 'Expired_Contract_y'], inplace=True, axis=1)

    if 'Cash_x' in column_list and 'Cash_y' in column_list:
        if 'Cash' in column_list:
            df_total['Cash'] = df_total['Cash'].fillna(value=0.0) + df_total['Cash_x'].fillna(value=0.0) + df_total[
                'Cash_y'].fillna(value=0.0)
        else:
            df_total['Cash'] = df_total['Cash_x'].fillna(value=0.0) + df_total['Cash_y'].fillna(value=0.0)
        df_total.drop(['Cash_x', 'Cash_y'], inplace=True, axis=1)

    if 'Expense_x' in column_list and 'Expense_y' in column_list:
        if 'Expense' in column_list:
            df_total['Expense'] = df_total['Expense'].fillna(value=0.0) + df_total['Expense_x'].fillna(value=0.0) + \
                                  df_total['Expense_y'].fillna(value=0.0)
        else:
            df_total['Expense'] = df_total['Expense_x'].fillna(value=0.0) + df_total['Expense_y'].fillna(value=0.0)
        df_total.drop(['Expense_x', 'Expense_y'], inplace=True, axis=1)

    if 'Broker_Sub_Red_x' in column_list and 'Broker_Sub_Red_y' in column_list:
        if 'Broker_Sub_Red' in column_list:
            df_total['Broker_Sub_Red'] = df_total['Broker_Sub_Red'].fillna(value=0.0) + df_total['Broker_Sub_Red_x'].fillna(value=0.0) + \
                                  df_total['Broker_Sub_Red_y'].fillna(value=0.0)
        else:
            df_total['Broker_Sub_Red'] = df_total['Broker_Sub_Red_x'].fillna(value=0.0) + df_total['Broker_Sub_Red_y'].fillna(value=0.0)
        df_total.drop(['Broker_Sub_Red_x', 'Broker_Sub_Red_y'], inplace=True, axis=1)
    
    if 'NAV_Sub_Red_x' in column_list and 'NAV_Sub_Red_y' in column_list:
        if 'NAV_Sub_Red' in column_list:
            df_total['NAV_Sub_Red'] = df_total['NAV_Sub_Red'].fillna(value=0.0) + df_total['NAV_Sub_Red_x'].fillna(value=0.0) + \
                                  df_total['NAV_Sub_Red_y'].fillna(value=0.0)
        else:
            df_total['NAV_Sub_Red'] = df_total['NAV_Sub_Red_x'].fillna(value=0.0) + df_total['NAV_Sub_Red_y'].fillna(value=0.0)
        df_total.drop(['NAV_Sub_Red_x', 'NAV_Sub_Red_y'], inplace=True, axis=1)
    
    df_total['Total'] = df_total['Cash'] + df_total['Expense'] + df_total['Prev_Cash'] + df_total['Expired_Contract'] + \
                        df_total['NAV_Sub_Red'] + df_total['Contract_PnL'] + df_total['Contract_Value']
    df_total['Total_Broker'] = df_total['Cash'] + df_total['Expense'] + df_total['Prev_Cash_Broker'] + df_total[
        'Expired_Contract'] + \
                               df_total['Broker_Sub_Red'] + df_total['Contract_PnL_Broker'] + df_total['Contract_Value']

    df_ftd = (df_total['Expense']).to_frame('FTD')
    df_ftd['Prev_Cash'] = df_total['Prev_Cash']
    df_ftd['Cash'] = df_total['Total']
    df_ftd['Open_Value'] = df_total['Open_Value']
    df_ftd['prev_total_pnl_local'] = df_total['prev_total_pnl_local']

    df_total.reset_index(inplace=True)
    df_total.drop(['Prev_Cash','Prev_Cash_Broker','prev_total_pnl_local'], inplace=True, axis=1)

    ix1 = (df_total.Cash == 0) & (df_total.Expense == 0) & (df_total.Broker_Sub_Red == 0) & (df_total.NAV_Sub_Red == 0) & (
            df_total.Expired_Contract == 0) & (df_total.Total == 0) & (df_total.Open_Value == 0)

    df_total.drop(df_total[ix1].index, axis=0, inplace=True)

    return df_total, df_ftd.reset_index(drop=False)


# endregion

def generate_delta(ex_date, db):
    """

    :param ex_date:
    :return:
    """

    column_list = ['MIS_DATE', 'UniqueTicker', 'Broker', 'Exchange', 'StrategyTag', 'SecurityType', 'LastTradeableDate',
                   'NET_QTY', 'NET_VALUE', 'Cumm_PnL_Local']
    uni_index = ['UniqueTicker', 'Broker', 'Exchange', 'SecurityType']
    close_uni_index = ['UniqueTicker', 'Broker']
    list_req_columns = ['Delta_Cur', 'Leg1_Cur', 'Leg1_Use_Close', 'Leg1_Multiplier', 'Leg2_Cur', 'Leg2_Use_Close',
                        'Leg2_Multiplier', 'Leg3_Cur', 'Leg3_Use_Close',
                        'Leg3_Multiplier','To_Include_Value','Multiplier']

    df_mis = db.get_mis(ex_date, column_list)
    ix1 = np.logical_or(df_mis['LastTradeableDate'] > ex_date, df_mis['LastTradeableDate'].isnull())
    ix1 = np.logical_or(ix1, df_mis['SecurityType'] == 'SPOT')
    df_mis = df_mis[ix1]
    ix1 = df_mis['SecurityType'] == 'SPOT'
    df_mis['Broker_Old'] = df_mis['Broker']
    df_mis.loc[ix1, 'Broker'] = 'ALL'
    unique_ticker = df_mis['UniqueTicker'].tolist()
    df_mapping = db.get_mapping_table(unique_ticker)
    df_mis = hp.map_calculation_type(df_mis, df_mapping, uni_index, list_req_columns)
    df_mis['Delta_Cur'].fillna(df_mis['UniqueTicker'], inplace=True)
    unique_ticker = list(set(unique_ticker).union(df_mis['Delta_Cur'].tolist()))
    df_close = db.get_close_price(unique_ticker, ex_date)
    df_mis = hp.map_delta_close(df_mis, df_close, ['Close'])
    df_mis['new_value'] = df_mis['NET_QTY'] * df_mis['Close'] * df_mis['Multiplier'] * -1.0
    df_mis['L1_Price_Multi'] = df_mis['Close']
    df_mis['L2_Price_Multi'] = df_mis['Close']
    df_mis['L3_Price_Multi'] = df_mis['Close']
    ix1_inv = df_mis['Leg1_Use_Close'] == -1
    df_mis.loc[ix1_inv, 'L1_Price_Multi'] = 1.0 / df_mis.loc[ix1_inv, 'L1_Price_Multi']
    ix1_inv = df_mis['Leg2_Use_Close'] == -1
    df_mis.loc[ix1_inv, 'L2_Price_Multi'] = 1.0 / df_mis.loc[ix1_inv, 'L2_Price_Multi']
    ix1_inv = df_mis['Leg3_Use_Close'] == -1
    df_mis.loc[ix1_inv, 'L3_Price_Multi'] = 1.0 / df_mis.loc[ix1_inv, 'L3_Price_Multi']
    ix1_no_use = df_mis['Leg1_Use_Close'] == 0
    df_mis.loc[ix1_no_use, 'L1_Price_Multi'] = 1.0
    ix1_no_use = df_mis['Leg2_Use_Close'] == 0
    df_mis.loc[ix1_no_use, 'L2_Price_Multi'] = 1.0
    ix1_no_use = df_mis['Leg3_Use_Close'] == 0
    df_mis.loc[ix1_no_use, 'L3_Price_Multi'] = 1.0
    df_mis['L1_Delta'] = df_mis['NET_QTY'] * df_mis['Leg1_Multiplier'] * df_mis['L1_Price_Multi']
    df_mis['L2_Delta'] = df_mis['NET_VALUE'] * df_mis['Leg2_Multiplier'] * df_mis['L2_Price_Multi']
    ix_sec = df_mis['SecurityType'].isin(['EQ', 'FIS'])
    df_mis.loc[ix_sec, 'L2_Delta'] = df_mis.loc[ix_sec, 'new_value'] * df_mis.loc[ix_sec, 'Leg2_Multiplier'] * \
                                     df_mis.loc[ix_sec, 'L2_Price_Multi']
    ix_sign = np.logical_or(df_mis['To_Include_Value'] == 1, df_mis['SecurityType']=='EQ')
    df_mis.loc[ix_sign, 'L2_Delta'] *= -1
    df_mis['L3_Delta'] = df_mis['Cumm_PnL_Local'] * df_mis['Leg3_Multiplier'] * df_mis['L3_Price_Multi']
    ix_cur = np.logical_and(df_mis['Delta_Cur'].str.contains('/USD'), df_mis['SecurityType'] == 'SPOT')
    df_mis.loc[ix_cur, 'L1_Delta'] = df_mis.loc[ix_cur, 'L2_Delta']
    df_mis.loc[ix_cur, 'L2_Delta'] = 0.0
    df_mis.loc[ix_cur, 'L3_Delta'] = 0.0
    df_mis.rename(
        {'MIS_DATE': 'Date', 'Leg1_Cur': 'Leg1_Currency', 'Leg2_Cur': 'Leg2_Currency', 'Leg3_Cur': 'Leg3_Currency'},
        inplace=True, axis=1)
    df_mis['Broker'] = df_mis['Broker_Old']
    df_mis.drop(['Broker_Old'], axis=1, inplace=True)
    df_final_delta = df_mis[REPORT_COLUMNS]
    return df_final_delta


# region Total MIS
def calculate_mtm_breakup(df_data, day_today, previous_day):
    """
    Purpose :-
    :param df_data: data frame
    :param day_today:
    :param previous_day:
    :return:
    """

    type_column = str(day_today) + "_" + "Type"
    today_close = str(day_today) + "_" + "Close"
    today_broker_close = str(day_today) + "_" + "BrokerClose"
    todays_cf = str(day_today) + "_" + "ConversionFactor"
    todays_co = str(day_today) + "_" + "Conversion"
    todays_rf = str(day_today) + "_" + "RoundingFactor"

    previous_close = str(previous_day) + "_" + "Close"
    previous_broker_close = str(previous_day) + "_" + "BrokerClose"
    previous_cf = str(previous_day) + "_" + "ConversionFactor"
    previous_rf = str(previous_day) + "_" + "RoundingFactor"
    previous_co = str(previous_day) + "_" + "Conversion"

    df_data[previous_co].fillna(df_data[todays_co], inplace=True)
    df_data[previous_close].fillna(df_data[today_close], inplace=True)
    df_data[previous_broker_close].fillna(df_data[today_broker_close], inplace=True)

    # region Calculating Todays PNL
    ix_q = df_data[type_column] == QUANTO
    df_data['TodayPnL_Local'] = (df_data['T_NET_VALUE'] + (
            df_data['T_NET_QTY'] * df_data['Multiplier'] * df_data[today_close]))
    df_data['TodayPnL_Broker'] = (df_data['T_NET_VALUE'] + (
            df_data['T_NET_QTY'] * df_data['Multiplier'] * df_data[today_broker_close]))

    res = [i for i in df_data.UniqueTicker if "DBW" in i]
    ix1 = df_data.UniqueTicker.isin(res)
    df_data['TodayPnL_Dollar'] = df_data['TodayPnL_Local'] * (df_data[todays_cf] / df_data[todays_co])
    df_data.loc[ix1, 'TodayPnL_Local'] = df_data.loc[ix1, 'TodayPnL_Dollar']

    df_data.loc[ix_q, 'TodayPnL_Local'] *= df_data.loc[ix_q, todays_co]
    df_data.loc[ix_q, 'TodayPnL_Broker'] *= df_data.loc[ix_q, todays_co]

    ix_q_temp = np.logical_and(df_data[type_column] == QUANTO,df_data['UniqueTicker'].str.contains('CNT'))
    df_data.loc[ix_q_temp, 'TodayPnL_Local'] = df_data.loc[ix_q_temp, 'TodayPnL_Dollar'] * df_data.loc[ix_q_temp, todays_co]
    df_data.loc[ix_q_temp, 'TodayPnL_Broker'] = df_data.loc[ix_q_temp, 'TodayPnL_Dollar'] * df_data.loc[ix_q_temp, todays_co]

    df_data.TodayPnL_Dollar.fillna(value=0, inplace=True)
    df_data.TodayPnL_Local.fillna(value=0, inplace=True)
    df_data.TodayPnL_Broker.fillna(value=0, inplace=True)
    # endregion

    df_data['PreviousPnL_Local'] = (np.round(df_data[today_close] * df_data[todays_cf], NON_QUANTO_ROUNDING) - np.round(
        df_data[previous_close] * df_data[todays_cf], NON_QUANTO_ROUNDING)) * df_data['PREV_QTY'] * df_data[
                                       'Multiplier']

    df_data['PreviousPnL_Broker'] = (np.round(df_data[today_broker_close] * df_data[todays_cf],
                                              NON_QUANTO_ROUNDING) - np.round(
        df_data[previous_broker_close] * df_data[todays_cf], NON_QUANTO_ROUNDING)) * df_data['PREV_QTY'] * df_data[
                                        'Multiplier']
    df_data['PreviousPnL_Local'].fillna(value=0, inplace=True)

    df_p_dollar = (df_data['PREV_VALUE'] + (df_data[previous_close] * df_data['Multiplier'] * df_data['PREV_QTY'])) / \
                  df_data[previous_co]
    df_t_dollar = (df_data['PREV_VALUE'] + (df_data[today_close] * df_data['Multiplier'] * df_data['PREV_QTY'])) / \
                  df_data[todays_co]
    df_final_dollar = df_t_dollar - df_p_dollar
    df_data['PreviousPnL_Dollar'] = df_final_dollar
    # df_data['previous_qty_valued_today'] = df_data['PREV_QTY'] * df_data['Multiplier'] * df_data[previous_close]
    df_data['previous_qty_valued_today'] = df_data['PREV_VALUE']
    # ix_qu = np.logical_not(df_data['Is_Quanto'] == 1)
    # df_data.loc[ix_qu,'PreviousPnL_Local'] = df_data.loc[ix_qu,'PreviousPnL_Dollar'] * df_data.loc[ix_qu,todays_co]

    ix1 = df_data[type_column] == QUANTO
    df_data.loc[ix1, 'PreviousPnL_Dollar'] = df_data['PreviousPnL_Local'] / df_data[todays_co]

    df_data['FTD_BROKER'] = df_data['TodayPnL_Broker'] + df_data['PreviousPnL_Broker']
    df_data['FTD_LOCAL'] = df_data['TodayPnL_Local'] + df_data['PreviousPnL_Local']
    df_data['FTD_DOLLAR'] = df_data['TodayPnL_Dollar'] + df_data['PreviousPnL_Dollar']

    df_data['Cumm_PnL_Local'] = (df_data['NET_VALUE'] + (
            df_data['NET_QTY'] * df_data['Multiplier'] * df_data[today_close]))
    df_data['Cumm_PnL_Broker'] = (df_data['NET_VALUE'] + (
            df_data['NET_QTY'] * df_data['Multiplier'] * df_data[today_broker_close]))

    is_derivative = np.logical_not(df_data['LastTradeableDate'].isnull())

    ix2 = np.logical_and(np.logical_not(is_derivative), df_data["NET_VALUE"] == df_data["PREV_VALUE"])
    ix2 = np.logical_and(ix2, df_data["NET_QTY"] == 0.0)
    df_data.loc[ix2, 'Expired'] = True
    df_data.loc[ix2, 'EXPIRED_ON'] = day_today

    ix2 = np.logical_and(df_data['SecurityType'] == 'REPO', df_data["NET_VALUE"] == 0.0)
    df_data.loc[ix2, 'Expired'] = True
    df_data.loc[ix2, 'EXPIRED_ON'] = day_today

    ix1 = np.logical_and(df_data['LastTradeableDate'] <= day_today, is_derivative)
    df_data.loc[ix1, 'Expired'] = True
    df_data.loc[ix1, 'EXPIRED_ON'] = day_today

    df_data.Expired.fillna(value=False, inplace=True)

    # endregion

    return df_data


def calculate_mis(ex_date, previous_date, df_previous_mis, df_current_trades, df_mapping, df_close):
    """

    :param ex_date:
    :param previous_date:
    :param df_previous_mis:
    :param df_todays_trades:
    :param df_mapping:
    :param df_close:
    :return:
    """
    df_final_mis = df_previous_mis.drop(
        ["TodayPnL_Local", "TodayPnL_Dollar", 'PreviousPnL_Local', "PreviousPnL_Dollar"], axis=1)
    df_final_mis.rename({'NET_QTY': 'PREV_QTY', 'NET_VALUE': 'PREV_VALUE'}, inplace=True, axis=1)
    df_final_mis.set_index(UNI_INDEX, inplace=True)
    df_current_trades.set_index(UNI_INDEX, inplace=True)
    # region Setting Quantity and Value for Today
    # df_final_mis = pd.concat([df_final_mis,df_current_trades],ignore_index=True,sort=False)
    df_final_mis = pd.merge(df_final_mis, df_current_trades, left_index=True, right_index=True, how='outer')
    df_final_mis['LastTradeableDate_x'].fillna(df_final_mis['LastTradeableDate_y'], inplace=True)
    df_final_mis.drop('LastTradeableDate_y', axis=1, inplace=True)
    df_final_mis.rename({'LastTradeableDate_x': 'LastTradeableDate'}, inplace=True, axis=1)
    # df_final_mis = df_final_mis.append(df_current_trades,ignore_index=False,sort=False)
    df_final_mis.rename({'FilledQty': 'T_NET_QTY', 'Value': 'T_NET_VALUE'}, inplace=True, axis=1)

    # df_final_mis[['T_NET_QTY','T_NET_VALUE','LastTradeableDate']] = df_current_trades[['FilledQty', 'Value','LastTradeableDate']]
    df_final_mis['PREV_QTY'].fillna(value=0, inplace=True)
    df_final_mis['T_NET_QTY'].fillna(value=0, inplace=True)
    df_final_mis['T_NET_VALUE'].fillna(value=0, inplace=True)
    df_final_mis['PREV_VALUE'].fillna(value=0, inplace=True)
    df_final_mis['NET_QTY'] = df_final_mis['PREV_QTY'] + df_final_mis['T_NET_QTY']
    df_final_mis['NET_VALUE'] = df_final_mis['PREV_VALUE'] + df_final_mis['T_NET_VALUE']
    # endregion
    df_final_mis = map_calculation_type(df_final_mis, df_mapping.copy())
    df_final_mis = map_close(df_final_mis, df_close.copy(), ex_date, True)
    df_final_mis = map_close(df_final_mis, df_close.copy(), previous_date, False)
    df_final_mis = calculate_mtm_breakup(df_final_mis, ex_date, previous_date)
    ix1 = df_final_mis.SecurityType == 'REPO'
    list_column = ['PREV_QTY', 'PREV_VALUE', 'FTD_BROKER', 'FTD_LOCAL', 'FTD_DOLLAR', 'DeltaPnL', 'T_NET_QTY',
                   'T_NET_VALUE',
                   'TodayPnL_Local', 'TodayPnL_Dollar', 'PreviousPnL_Local', 'PreviousPnL_Dollar', 'Cumm_PnL_Local',
                   'Cumm_PnL_Broker']
    df_final_mis.loc[ix1, list_column] = 0.0
    return df_final_mis


def generate_mis(ex_date, database, db_connection):
    """

    :param ex_date:
    :return:
    """

    previous_date = get_previous_date(ex_date, db_connection)

    # Get Previous Days MIS

    df_previous_mis = get_previous_day_position(previous_date, db_connection)
    df_get_todays_mis = get_previous_day_position(ex_date, db_connection)
    if len(df_get_todays_mis) > 0:
        raise ValueError("MIS for {} has already been prepared".format(str(ex_date)))
    # # Get Todays Days

    df_today_trades = get_trade_report(ex_date, db_connection)
    list_of_securities = []
    if not df_today_trades.empty:
        list_of_securities = df_today_trades.UniqueTicker.tolist()
    list_of_securities.extend(df_previous_mis.UniqueTicker.tolist())

    # Get Close Prices
    df_close_price = get_close_price(list_of_securities, ex_date, previous_date, db_connection)

    # Get Mappings
    df_mapping = get_mapping_table(list_of_securities, db_connection)

    # Combine to generate MIS
    t0 = time.time()
    df_expired = df_previous_mis[df_previous_mis.Expired == True]
    df_expired.loc[:]['MIS_DATE'] = ex_date
    df_not_expired = df_previous_mis[df_previous_mis.Expired == False]
    df_f = calculate_mis(ex_date, previous_date, df_not_expired.copy(), df_today_trades.copy(), df_mapping.copy(),
                         df_close_price.copy())
    df_f['MIS_DATE'] = ex_date
    # filename = os.path.abspath("MIS_" + str(ex_date.date())+".csv")
    # filename = os.path.join("MIS_CSV",filename)
    # df_f.to_csv(filename)
    df_temp = df_f[MIS_COLUMNS]
    # df_temp['DeltaPnL'] = 0.0

    # df_temp = pd.concat([df_temp,df_expired])

    # df_pos_4_cash = map_calculation_type(df_temp.copy().set_index(UNI_INDEX),df_mapping.copy())

    df_cash, df_cash_ftd = generate_cash(ex_date, previous_date, df_f, db_connection)

    df_cash_entries = generate_cash_entry(df_cash_ftd, ex_date, previous_date, MIS_COLUMNS, db_connection)
    df_temp = pd.concat([df_temp, df_cash_entries])
    df_cash.to_sql(TableName.Cash_MIS_Table.value, db_connection, if_exists='append', index=False)
    df_temp.to_sql(TableName.Final_MIS_Table.value, db_connection, if_exists='append', index=False)
    # DeltaCalculations
    df_delta = generate_delta(ex_date, database)
    df_delta.to_sql(TableName.DeltaTable.value, db_connection, if_exists='append', index=False)


# endregion

def run_menu_selection(selected_menu, database):
    """

    :param selected_menu:
    :param ex_date:
    :param db_connection:
    :return:
    """
    if selected_menu == Menu.run_mis:
        ex_date = dt.datetime.strptime(input("Enter Date:"), "%d-%m-%Y")
        # ex_date = dt.datetime(2014, 5, 30)
        generate_mis(ex_date, database, database.db_connection)
    elif selected_menu == Menu.delete_mis_entry:
        ex_date = dt.datetime.strptime(input("\\\\\\\\\\Enter Date:"), "%d-%m-%Y")
        delete_mis_for_date(ex_date, DB_MIS)


def menu(database):
    """

    :param db_connection:
    :return:
    """
    while True:
        i = 1
        for menu in Menu:
            menu_str = 'Press {sn_no} to {menu_title}'.format(sn_no=str(i), menu_title=menu.value)
            print(menu_str)
            i += 1
        menu_selection = int(input("Select menu:"))
        if menu_selection == 1:
            selected_menu = Menu.run_mis
        run_menu_selection(selected_menu, database)


if __name__ == '__main__':
    t2 = time.time()
    base_path = os.path.join(os.path.abspath(r'\\192.168.10.2\aia-drive\MIS\Ariana_MIS'), 'Database')
    db_name = 'DELTA_MIS_DB'
    db = hp.Database(base_path, db_name)
    db.conncet_db()
    # dt_p = dt.datetime(2020,3,30)
    # df_k = generate_delta(dt_p,db)
    df_date_list = db.get_query_df(
        "select Date from DateList where Date>='2020-12-14 00:00:00' and Date<='2020-12-17 00:00:00' order by Date")
    df_date_list['Date'] = pd.to_datetime(df_date_list['Date'])
    df_date_list.set_index(['Date'], inplace=True)
    for p_date in df_date_list.index.tolist():
        print("Processing for {}".format(str(p_date)))
        t_start = time.time()
        generate_mis(p_date, db, db.db_connection)
        # df_delta = generate_delta(p_date)
        # df_delta.to_sql(TableName.DeltaTable.value, DB_MIS.conncet_db(), if_exists='append', index=False)
        print("FINAL RUN {}".format(str(time.time() - t_start)))
    print("Total RUN {}".format(str(time.time() - t2)))

    # menu(db)

    # t = generate_cash(ex_date,get_previous_date(ex_date,db_connection),pd.DataFrame(),db_connection)
    # t.to_clipboard()
    db.db_connection.close()
