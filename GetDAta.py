from helper import Database
import helper as hp
import datetime as dt
import os
import numpy as np

if __name__ == '__main__':
    base_path = os.path.join(os.path.abspath(r'\\192.168.10.2\aia-drive\MIS\Ariana_MIS'), 'Database')
    # db_name = 'DELTA_MIS_DB - 09-12-2019'
    db_name = 'DELTA_MIS_DB'
    db = hp.Database(base_path, db_name)
    query_c = "select Date , Broker, Currency, sum(TOTAL) from CASH_MIS WHERE Date between '2018-12-31' and '2020-05-31' group by Date,Broker, Currency"
    # query_c = "select * from CASH_MIS WHERE Date between '2018-12-31' and '2020-05-31' group by Date,Broker, Currency"

    # region Vamsi MIS
    start_date = dt.datetime(2020, 11, 1)
    end_date = dt.datetime(2020, 11, 30)
    query_vamsi_expense = "select * from Expense where Date >= '{s_date}' and Date<='{e_date}'".format(
        s_date=str(start_date), e_date=str(end_date))
    query_close = "select * from CloseFile where Date>='{s_date}'and Date<='{e_date}' and SecurityType='SPOT' and Broker='ALL'".format(
        s_date=str(start_date), e_date=str(end_date))
    query__vamsi_r = "select MIS_DATE,StrategyTag,Broker,SecurityType,sum(FTD_DOLLAR) from  FINAL_MIS where MIS_DATE>='{s_date}' and MIS_DATE<='{e_date}' group by MIS_DATE,StrategyTag,Broker,SecurityType ".format(
        s_date=str(start_date), e_date=str(end_date))
    query_subs_data = "SELECT * FROM Subscribe_Redeem  where NAV_Date >='2020-10-30 00:00:00'"

    # endregion
    query_dbs = "select * from Expense where Broker = 'DBS' and currency ='USD'"

    # query_t = "select Date , Broker, Currency, sum(TOTAL) from CASH_MIS and Date group by Date,Broker, Currency"

    query_old_cash = "select TradeDate , Broker, Ticker, sum(TLocalCashBal) from FTD where TradeDate between '2019-06-01' and '2019-08-30'  and SecurityType='SPOT'  group by TradeDate ,Broker, Ticker "
    query_old_pnl = "select TradeDate, sum(DollarFtd) from FTD where TradeDate between '2019-01-01' and '2019-07-26' group by TradeDate"
    query_exp = "select * from FINAL_MIS Where Expired=1 and SecurityType!='SPOT'"

    query_s123 = "Select * from FINAL_MIS where Broker = 'EDEL' or Broker like 'ICICI%' or Broker = 'YES' or Broker like 'INDUS'"

    numerix_tally = "select * from FINAL_MIS where MIS_DATE between '2020-01-31' and '2020-02-28'"

    mis_query = "select MIS_DATE,sum(FTD_DOLLAR) from FINAL_MIS WHERE MIS_Date between '2019-01-01' and '2019-12-12' group by MIS_Date"
    # mis_query = "select UniqueTicker,sum(FTD_DOLLAR) from FINAL_MIS WHERE MIS_Date between '2019-10-16' and '2019-10-17' group by UniqueTicker"
    # mis_query = "select distinct MIS_DATE from FINAL_MIS"
    str_query = "SELECT sum(Cash),Date FROM Expense WHERE Broker = 'DBS' GROUP BY Date"
    str_query = "SELECT sum(FTD_DOLLAR),MIS_DATE,StrategyTag FROM FINAL_MIS WHERE Broker = 'DBS' and StrategyTag Like 'Treasury%' GROUP BY MIS_DATE"
    str_query = "Select * from Auto where TradeDate>'2019-11-28 00:00:00' and StrategyTag Like 'ARBITRAGE|EQUITY|CALENDAR|%' "
    mapping_ticker = "select DISTINCT UniqueTicker from Auto where TradeDate < '2020-01-01' and UniqueTicker in ( select distinct UniqueTicker from Mapping)"
    multiplier_ticker = "SELECT Distinct b.UniqueTicker, b.Multiplier FROM Auto a, Mapping b WHERE a.TradeDate < '2020-01-01' and b.Multiplier <> 1"
    tally_query = "select UniqueTicker,StrategyTag, Cumm_PnL_Local MIS_DATE from FINAL_MIS where MIS_DATE<='2019-12-31 00:00:00' and EXPIRED=1 and SecurityType<>'SPOT' order by UniqueTicker"
    str_query = "SELECT sum(Cash),Date FROM Expense WHERE Broker = 'DBS' GROUP BY Date"
    G1_query = "select UniqueTicker,StrategyTag, Cumm_PnL_Local MIS_DATE,LastTradeableDate,Exchange,SecurityType from FINAL_MIS where MIS_DATE<='2019-12-31 00:00:00' and EXPIRED=1 and SecurityType<>'SPOT' order by UniqueTicker"
    G2_query = "SELECT Distinct Date, UniqueTicker,Close FROM CloseFile where SecurityType ='SPOT';"
    xau_query = "select UniqueTicker, sum(FilledQty) , TransactionType, StrategyTag from Auto where UniqueTicker like 'XAU%' group by UniqueTicker, TransactionType, StrategyTag, TradeDate"
    datelist_query = "select * from DateList order by Date"
    instrument_query = "select a.UniqueTicker, b.Numerix_IntrumentType from Auto a , Mapping b where a.UniqueTicker=b.UniqueTicker  and a.TradeDate>'2019-12-31'"
    G3_query = "select UniqueTicker,StrategyTag, Cumm_PnL_Local MIS_DATE,LastTradeableDate,Exchange,SecurityType from FINAL_MIS where MIS_DATE<='2020-02-01 00:00:00' and EXPIRED=1 and SecurityType<>'SPOT' order by UniqueTicker;"
    G3_query = "select distinct UniqueTicker,StrategyTag, Cumm_PnL_Local,MIS_DATE,NET_QTY,NET_VALUE,LastTradeableDate,Exchange,SecurityType from FINAL_MIS where MIS_DATE<='2020-04-07 00:00:00' and EXPIRED=1 and SecurityType<>'SPOT' order by UniqueTicker;"
    G4_query = "select UniqueTicker,StrategyTag,Cumm_PnL_Local,  MIS_DATE,NET_QTY,NET_VALUE,LastTradeableDate,Exchange,SecurityType from FINAL_MIS where MIS_DATE<='2020-03-20 00:00:00' and SecurityType<>'SPOT' order by UniqueTicker;"
    GNAV_query = "select MIS_DATE,UniqueTicker,FTD_DOLLAR,FTD_LOCAL,LastTradeableDate,Exchange,SecurityType from FINAL_MIS where MIS_DATE between '2020-03-01 00:00:00' and '2020-04-01 00:00:00' and LastTradeableDate>'2020-02-28 00:00:00' order by UniqueTicker;"
    Exp_Query = "Select * from Expense"
    G_Sumit_Query = "select distinct UniqueTicker, sum(FTD_BROKER),Cumm_PnL_Local,MIS_DATE,NET_QTY,NET_VALUE,LastTradeableDate,Exchange,SecurityType from FINAL_MIS where MIS_DATE<='2020-04-07' group by UniqueTicker"
    numerix_price_query = "select cf.UniqueTicker, cf.Close, cf.Date from CloseFile cf, Mapping m where cf.UniqueTicker = m.UniqueTicker and m.Numerix_IntrumentType = 'Externally Priced Instrument'"
    # numerix_price_query

    cash_tally_query = "select fm.MIS_DATE, fm.UniqueTicker,fm.SecurityType,mm.Numerix_IntrumentType, fm.NET_QTY, fm.NET_VALUE, fm.Cumm_PnL_Local from FINAL_MIS fm, Mapping mm where fm.UniqueTicker=mm.UniqueTicker and (mm.PnLCurrency1='INR' or mm.PnLCurrency2='INR') and fm.Expired=1 and fm.SecurityType<>'SPOT'"
    cash_mis_query = "select * from CASH_MIS where Currency='MYR'"
    cash_tally_query = "select fm.MIS_DATE, fm.UniqueTicker,fm.SecurityType,mm.Numerix_IntrumentType, fm.NET_QTY, fm.NET_VALUE, fm.Cumm_PnL_Local from FINAL_MIS fm, Mapping mm where fm.Exchange in ('SELT','OTC','RTS') and fm.Broker in ('BCS','CS','OTK') and (mm.PnLCurrency1='INR' or mm.PnLCurrency2='INR') and fm.Expired=1 and fm.SecurityType<>'SPOT'"
    cash_tally_query_2 = "select distinct fm.UniqueTicker,fm.SecurityType,mm.Numerix_IntrumentType, sum(FTD_BROKER) from FINAL_MIS fm, Mapping mm where fm.UniqueTicker=mm.UniqueTicker and (mm.PnLCurrency1='RUB' or mm.PnLCurrency2='RUB') and fm.SecurityType<>'SPOT' group by  fm.UniqueTicker,fm.SecurityType,mm.Numerix_IntrumentType"
    mapping_details = "select * from Mapping where Numerix_IntrumentType ='Currency Future Option' and (PnLCurrency1='USD' or PnLCurrency2='USD')"
    query_auto_m = "select * from Auto where TradeDate>='2019-04-01 00:00:00'and TradeDate<='2020-03-31 00:00:00' and Broker='EDEL' or Broker like 'ICICI%' or Broker ='YES' or Broker='INDUS'"

    query_sec_list = "SELECT MIS_DATE,UniqueTicker,Exchange,Broker,SecurityType, NET_QTY, NET_VALUE, Expired FROM FINAL_MIS where SecurityType ='EQ' OR SecurityType = 'FIS'"
    tw_query = "select * from TradeFile where TradeDate >= '2020-05-01' and StrategyTag like '%TW-EQUITY%' "
    query_close_security = "select * from CloseFile where SecurityType = 'FIS'"
    test_query = "select * from TradeFile where TradeDate >= '2020-01-01' and Broker in ('SG','SCFX') and Exchange='OTC'"
    query_daily_ftd_data = "select * from FINAL_MIS where MIS_DATE>'2019-12-31 00:00:00' and MIS_DATE<='2020-01-31 00:00:00'"
    query_bond_close = "select *from CloseFile where UniqueTicker in (select distinct UniqueTicker from Mapping where Numerix_IntrumentType='Bond')"
    query_taiwan_close = "select * from CloseFile where Date >= '2020-05-05' and UniqueTicker in ( select distinct UniqueTicker from FINAL_MIS where StrategyTag like '%TW-EQUITY%' and Exchange <> 'OTC' and MIS_DATE >'2020-05-05')"
    query_daily_FTD = "select MIS_DATE, UniqueTicker, sum(FTD_DOLLAR) from FINAL_MIS where FTD_DOLLAR!=0 and MIS_DATE<='2020-08-31 00:00:00' group by MIS_DATE,UniqueTicker"
    query_daily_FTD_new = "select MIS_DATE , SUM(FTD_DOLLAR) from FINAL_MIS group by MIS_DATE order by MIS_DATE"
    query_cash_mis = "select * from Cash_MIS where Broker = 'DBS' and Currency='USD'"
    query_ExpenseE = "select * from Expense"
    query_CashA = "select * from Cash"
    query_OPTTrades = "Select * from Auto where SecurityType ='OPT' and TradeDate >= '2018-01-01 00:00:00'"
    query_data_trade = "select * from Auto where TradeDate ='2020-11-19 00:00:00'"
    ex_date = dt.datetime(2020, 11, 19)
    df = db.get_query_df(query__vamsi_r)
    # ticker_list = df['UniqueTicker'].tolist()
    # group_col = ['TradeDate', 'Broker', 'Exchange', 'UniqueTicker', 'LastTradeableDate', 'SecurityType', 'StrategyTag']
    # df = df.pivot_table(index=group_col, columns=['TransactionType'], values=['FilledQty', 'Value'], aggfunc=np.sum)
    # df.columns = [s1 + "_" + str(s2) for (s1, s2) in df.columns.tolist()]
    # column_list = df.columns.tolist()
    # for col in column_list:
    #     df[col].fillna(value=0, inplace=True)
    # df.reset_index(inplace=True)
    # df.set_index('UniqueTicker', inplace=True)
    # df_close = db.get_close_price(ticker_list, ex_date)
    # df_close.drop_duplicates(['UniqueTicker'], inplace=True)
    # df_close.set_index(['UniqueTicker'], inplace=True)
    # df['Spot'] = df_close['Conversion']
    # df['Dollar_Value_Buy'] = df['Value_Buy'] / df['Spot']
    # df['Dollar_Value_Sell'] = df['Value_Sell'] / df['Spot']
    # df.sort_values('StrategyTag', inplace=True)

    # t = df['UniqueTicker'].tolist()
    # df_map = db.get_mapping_table(t)
    # df_map = df_map.drop_duplicates(subset='UniqueTicker', keep='first')
    # df_map.set_index('UniqueTicker', inplace=True)
    # df.set_index('UniqueTicker', inplace=True)
    # df['Type'] = df_map['Numerix_IntrumentType']
    # df['ValueDate'] = df_map['ValueDate']
    # df['Type'].fillna(value='Cash', inplace=True)
    # df.reset_index(inplace=True)
    df.to_clipboard()
