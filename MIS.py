# region Import
import datetime as dt
from builtins import str
import pandas as pd
import numpy as np
import time
from helper import Database
# endregion


# region Global Variables
MIS_START_DATE = dt.date(2014, 5, 21)
DB_NAME = "DB_MIS"
QUANTO = "Quanto"
NON_QUANTO_ROUNDING = int(12)

# endregion







def calculate_mtm_breakup(df_data, day_today, previous_day):
    """
    Purpose :- 
    :param df_data: data frame  
    :param day_today: 
    :param previous_day: 
    :return: 
    """

    if type(day_today) == dt.datetime:
        type_column = str(day_today) + "_" + "Type"
        today_close = str(day_today) + "_" + "Close"
        todays_cf = str(day_today) + "_" + "ConversionFactor"
        todays_co = str(day_today) + "_" + "Conversion"
        todays_rf = str(day_today) + "_" + "RoundingFactor"
    else:
        today_close = day_today +  "_" + "Close"
        type_column = day_today + "_" + "Type"
        todays_cf = day_today + "_" + "ConversionFactor"
        todays_rf = day_today + "_" + "RoundingFactor"
        todays_co = day_today + "_" + "Conversion"

    if type(previous_day) == dt.datetime:
        type_column = str(previous_day) + "_" + "Type"
        previous_close = str(previous_day) + "_" + "Close"
        previous_cf = str(previous_day) + "_" + "ConversionFactor"
        previous_rf = str(previous_day) + "_" + "RoundingFactor"
        previous_co = str(previous_day)  + "_" + "Conversion"
    else:
        type_column = previous_day + "_" + "Type"
        previous_close = previous_day + "_" + "Close"
        previous_cf = previous_day + "_" + "ConversionFactor"
        previous_rf = previous_day + "_" + "RoundingFactor"
        previous_co = previous_day + "_" + "Conversion"

    # region Calculating Todays PNL
    df_data['TodayPNL_Local'] = (df_data['todays_value'] - df_data['prev_value']) + (
                (df_data['todays_qty'] - df_data['prev_qty']) * df_data['Multiplier'] * df_data[
            today_close])

    df_data['TodayPNL_Dollar'] = df_data['TodayPNL_Local'] * (df_data[todays_cf] / df_data[todays_co])

    df_data.TodayPNL_Dollar.fillna(value=0,inplace=True)
    df_data.TodayPNL_Local.fillna(value=0, inplace=True)
    # endregion

    df_data['PreviousPNL_Local'] = (np.round(df_data[today_close] * df_data[todays_cf], NON_QUANTO_ROUNDING) - np.round(
        df_data[previous_close] * df_data[todays_cf], NON_QUANTO_ROUNDING)) * df_data['prev_qty'] * df_data['Multiplier']
    df_data['PreviousPNL_Local'].fillna(value=0,inplace=True)
    df_p_dollar = (df_data['prev_value'] + (df_data[previous_close] * df_data['Multiplier'] * df_data['prev_qty'])) / \
                  df_data[previous_co]
    df_t_dollar = (df_data['prev_value'] + (df_data[today_close] * df_data['Multiplier'] * df_data['prev_qty'])) / \
                  df_data[todays_co]
    df_final_dollar = df_t_dollar - df_p_dollar
    df_data['PreviousPNL_Dollar'] = df_final_dollar
    ix1 = df_data[type_column] == QUANTO
    df_data.loc[ix1,'PreviousPNL_Dollar'] = df_data['PreviousPNL_Local'] / df_data[todays_co]

    # endregion

    return df_data



def get_trade_info(db, table_name, end_date, previous_date, derivatives=True):
    """

    :param table_name:
    :param end_date:
    :return:
    """
    # region Getting Raw Trades
    trades_connection = db
    if derivatives == True:
        query_str = "SELECT * from {t_name} where TradeDate between '{s_date}' and '{e_date}' and not(LastTradableDate is NULL)".format(
            s_date=str(MIS_START_DATE), e_date=str(end_date), t_name=table_name)
    else:
        query_str = "SELECT * from {t_name} where TradeDate between '{s_date}' and '{e_date}' and (LastTradableDate is NULL)".format(
            s_date=str(MIS_START_DATE), e_date=str(end_date), t_name=table_name)

    df_trades = pd.read_sql(query_str, trades_connection)
    ix1 = df_trades.TransactionType == 'Sell'
    df_trades.loc[ix1, 'FilledQty'] *= -1.0
    df_trades.loc[np.logical_not(ix1), 'Value'] *= -1.0
    # endregion

    # region Trade Grouping
    df_eq_trade = df_trades.groupby(
        by=['UniqueTicker', 'Broker', 'Exchange', 'Classification', 'Strategy', 'SecurityType', 'Multiplier']).agg(
        {'FilledQty': np.sum, 'Value': np.sum,'LastTradableDate':'first'})
    df_eq_trade["todays_value"] = df_eq_trade['Value']
    df_eq_trade["todays_qty"] = df_eq_trade['FilledQty']
    df_eq_trade.drop(['FilledQty', 'Value'], axis=1, inplace=True)

    df_temp = df_trades[df_trades.TradeDate <= previous_date]
    df_eq_trade_previous = df_temp.groupby(
        by=['UniqueTicker', 'Broker', 'Exchange', 'Classification', 'Strategy', 'SecurityType', 'Multiplier']).agg(
        {'FilledQty': np.sum, 'Value': np.sum})
    df_eq_trade_previous["prev_value"] = df_eq_trade_previous['Value']
    df_eq_trade_previous["prev_qty"] = df_eq_trade_previous['FilledQty']

    df_eq_trade_previous.drop(['FilledQty', 'Value'], axis=1, inplace=True)
    # endregion

    df_con = pd.concat([df_eq_trade, df_eq_trade_previous], axis=1, join='outer')
    df_con.fillna(value=0, inplace=True)
    df_con.to_clipboard()
    df_con['LastTradableDate'] = pd.to_datetime(df_con['LastTradableDate'])
    if not derivatives:
        ix1 = df_con["todays_value"] == df_con["prev_value"]
    else:
        ix1 = df_con['LastTradableDate'] < end_date
    df_con['expired'] = False
    df_con.loc[ix1, 'expired'] = True

    return df_con


def fix_usd_string(df_table):
    """

    :param df_table:
    :return:
    """
    df_temp = df_table.copy()
    for ix, ticker_row in df_table.iterrows():
        ticker_value = ticker_row[0]
        if not 'USD' in ticker_value:
            if ticker_value[0] in ['AUD', 'GBP', 'EUR', 'XAU', 'XAG']:
                p = ticker_value + '/USD'
            else:
                p = 'USD/' + ticker_value
            df_temp.loc[ix, 'Ticker'] = p
    df_temp = pd.DataFrame(data=list(set(df_temp.Ticker.tolist())))
    return df_temp


def unique_spot(db):
    """

    :return:
    """
    conn = db.conncet_db()
    query = "SELECT Ticker FROM CloseFile where SecurityType='SPOT' UNION SELECT DISTINCT Currency FROM Expense"
    df_test = pd.read_sql(query, conn)
    k = lambda x: x if 'USD' in x else 'USD/' + x
    return df_test


def map_close(df_data, df_close, date):
    """

    :param df_data:
    :param df_close:
    :return:
    """
    df_data.set_index(['UniqueTicker', 'Broker'], inplace=True)
    df_close.set_index(['UniqueTicker', 'Broker'], inplace=True)
    if type(date) == dt.datetime:
        df_close_temp = df_close[df_close.Date == str(date)]
    else:
        df_close_temp = df_close[df_close.Date == date]
    columms_to_get = ['Close', 'BrokerClose', 'Conversion', 'Type', 'RoundingFactor', 'ConversionFactor']
    func_t = lambda x: str(date) + "_" + x
    columns_to_set = list(map(func_t, columms_to_get))
    df_data[columns_to_set] = df_close_temp[columms_to_get]
    return df_data.reset_index(drop=False)


def map_calculation_type(df_data, df_mapping):
    """

    :param df_data:
    :param df_mapping:
    :return:
    """
    df_data.reset_index(inplace=True)
    df_data.set_index(['UniqueTicker', 'Broker', 'Exchange'], inplace=True)
    df_mapping.reset_index(inplace=True, drop=True)
    df_mapping.set_index(['UniqueTicker', 'Broker', 'Exchange'], inplace=True)
    df_mapping = df_mapping.loc[~df_mapping.index.duplicated(keep='first')]
    # df_data = pd.concat([df_data, df_mapping], axis=1, join='outer')

    df_data[['PNL_Type1', 'PNL_Type2']] = df_mapping[['PnLCurrency1', 'PnLCurrency2']]
    df_data.reset_index(inplace=True, drop=False)
    ix1 = np.logical_and(df_data['PNL_Type1'].isnull(), df_data['PNL_Type2'].isnull())
    if not ix1.empty:
        if ix1.any():
            stock_name = df_data.loc[ix1, 'UniqueTicker'].values[0]
            raise Exception("PNL Type Cannot be null for {}".format(stock_name))
    return df_data


def main():
    # region Get Input from User
    # str_start_date = input("Start Date:-")
    # str_end_date = input("End Date :-")
    str_start_date = '21-05-2014'
    str_end_date = '21-05-2014'
    dt_start_date = dt.datetime.strptime(str_start_date, "%d-%m-%Y")
    dt_end_date = dt.datetime.strptime(str_end_date, "%d-%m-%Y")
    # dt_range = pd.date_range(dt_start_date,dt_end_date)
    # endregion

    # region Getting Dates
    db_mis = Database(DB_NAME)
    db_connection = db_mis.conncet_db()
    query_str = "SELECT DISTINCT Date from CloseFile WHERE Date BETWEEN '{s_date}' and '{e_date}'".format(
        s_date=str(dt_start_date), e_date=str(dt_end_date))

    df_dates = pd.read_sql(query_str, db_connection)
    df_dates.set_index('Date', inplace=True)
    previous_date = (df_dates.index[-1])
    # endregion
    df_equity = get_trade_info(db_connection, "Auto", dt_end_date, previous_date, False)
    df_derivative = get_trade_info(db_connection, "Auto", dt_end_date, previous_date, True)

    query_str = "select UniqueTicker , Exchange, Broker, PNLCurrency1, PNLCurrency2 from Mapping"
    df_mapping = pd.read_sql(query_str, db_connection)

    # df_equity = map_calculation_type(df_equity.copy(), df_mapping.copy())
    df_derivative = map_calculation_type(df_derivative.copy(), df_mapping.copy())

    # list_of_securities = df_equity[df_equity.expired == False].UniqueTicker.tolist()
    list_of_securities = []
    list_of_securities.extend(df_derivative[df_derivative.expired == False].UniqueTicker.tolist())
    if len(list_of_securities) == 1:
        t_s = "('{t}')".format(t=list_of_securities[0])
    else:
        t_s = tuple(list_of_securities)
    # t_s = tuple(list_of_securities)
    query_str = f"""SELECT  * FROM CloseFile WHERE Date BETWEEN '{previous_date}' AND '{dt_end_date}' AND UniqueTicker IN {t_s}"""
    df_close = pd.read_sql(query_str, con=db_connection)
    df_close[df_close.ConversionFactor == 0] = 1.0
    df_close[df_close.RoundingFactor == 0] = NON_QUANTO_ROUNDING
    df_close.ConversionFactor.fillna(value=1.0, inplace=True)
    df_close.RoundingFactor.fillna(value=NON_QUANTO_ROUNDING, inplace=True)
    # df_equity = map_close(df_equity, df_close.copy(), previous_date)
    # df_equity = map_close(df_equity, df_close.copy(), dt_end_date)
    df_derivative = map_close(df_derivative, df_close.copy(), previous_date)
    df_derivative = map_close(df_derivative, df_close.copy(), dt_end_date)
    df_equity_test = calculate_mtm_breakup(df_derivative,dt_end_date,previous_date)
    # df_equity_test = calculate_mtm_breakup(df_equity, dt_end_date, previous_date)
    df_equity_test.to_clipboard()


if __name__ == '__main__':
    tick = time.time()
    main()
    # query_str = "SELECT  * FROM FINAL_MIS"
    # db_mis = Database(DB_NAME)
    # db_connection = db_mis.conncet_db()
    # df_mis = pd.read_sql(query_str,db_connection)
    # print (df_mis.columns)
    print(time.time() - tick)

    # db_trade = Database("DB_MIS")
    # cur = db_trade.conncet_db().cursor()
    # query = "SELECT Ticker FROM CloseFile where SecurityType='SPOT'"
    # p = unique_spot(db_trade)
    # df_t = fix_usd_string(p)
    # df_t.to_clipboard()
