import time, os, logging
import helper as hp
import datetime as dt
import pandas as pd
import UploadFiles
import ClosingScripGen
import FreshMIS
import ReportGenerator
import warnings


def execute_menu_item(int_menu, ex_date, database, logger):
    """

    :param int_menu:
    :param ex_date:
    :param database:
    :param logger:
    :return:
    """
    if int_menu == 1:
        try:
            folder_p = os.path.abspath(r'\\192.168.10.2\aia-drive\MIS\Ariana_MIS\UploadFiles_ALL\All_Mapping.csv')
            df_data = pd.read_csv(folder_p, index_col=None, parse_dates=[3, 4])
            df_data['Update_DateTime'] = dt.datetime.now()
            df_data.dropna(how='all', inplace=True)
            df_data.to_sql("Mapping", database.db_connection, if_exists='append', index=False)
        except:
            logger.log_message(Exception)
            logger.log_message("Issue while uploading Mapping please check")
    elif int_menu == 2:
        test_query = "select count(distinct MIS_DATE) from {t_name} where MIS_DATE >= '{d_date}'".format(
            t_name=hp.TableName.Final_MIS_Table.value, d_date=(ex_date))
        p = database.query_result(test_query)
        if p >= 1:
            logger.log_message("Data for {} days of MIS will be deleted".format(str(p)), logging.WARNING)
            time.sleep(2)
            str_val = input("To continue press Y:")
            logger.log_message("{} selected".format(str_val))
        else:
            str_val = 'Y'
        if str_val == 'Y':
            UploadFiles.upload_file_main(ex_date, database)
    elif int_menu == 3:
        ClosingScripGen.generate_close(ex_date, database)
    elif int_menu == 4:
        FreshMIS.generate_mis(ex_date, database, database.db_connection)
        ReportGenerator.generate_all_report(ex_date, database)
    elif int_menu == 5:
        exit()


def display_menu(logger, database, ex_date):
    """

    :param logger:
    :param database:
    :return:
    """
    while True:

        i = 1
        for menu in hp.MainMenu:
            menu_str = 'Press {sn_no} to {menu_title}'.format(sn_no=str(i), menu_title=menu.value)
            print(menu_str)
            i += 1

        menu_selection = int(input("Select menu:"))
        execute_menu_item(menu_selection, ex_date, database, logger)


def start_process(logger):
    """

    :return:
    """
    # region INIT basic Variables
    logger.log_message("Starting Process")
    base_path = os.path.abspath('Database')
    db_name = 'GAF_MIS'
    db = hp.Database(base_path, db_name)
    db.conncet_db()
    # endregion

    ex_date = dt.datetime.strptime(input("Please Enter the Date for Processing:"), "%d-%m-%Y")
    logger.log_message("date selected:- {}".format(ex_date))

    test_mis = db.get_mis(ex_date)
    if not test_mis.empty:
        str_val = input("MIS for this date exist press Y to continue:")
        logger.log_message("{} :- Selected ".format(str_val).format(ex_date))
    else:
        str_val = 'Y'

    if str_val.upper() == 'Y':
        display_menu(logger, db, ex_date)
    else:
        exit()


if __name__ == '__main__':
    warnings.filterwarnings('ignore')
    filer_logger = os.path.join("Logs", 'log_{}'.format(dt.datetime.now().date()))
    logger = hp.log_message('MIS_LOGS')
    logger.set_file_handler(filer_logger)
    while True:
        start_process(logger)
