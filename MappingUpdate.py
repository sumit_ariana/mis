import sqlite3
import os
import pandas as pd
from helper import Database
import UploadFiles
import helper as hp
import datetime as dt
import FreshMIS as fm

base_path = os.path.join(os.path.abspath(r'\\192.168.10.2\aia-drive\MIS\Ariana_MIS'), 'Database')
db_name = 'DELTA_MIS_DB'
DB_MIS = hp.Database(base_path, db_name)


def update_mapping(ticker_list, database, instrument_type):
    """

    :param ticker:
    :param database
    :param instrument_type:
    :return:
    """
    if len(ticker_list) == 1:
        t_list = "('{t}')".format(t=ticker_list[0])
    else:
        t_list = tuple(ticker_list)
    query_string = "update Mapping set Numerix_IntrumentType='{instrument}' ,Update_DateTime = '{t_now}' where UniqueTicker in {t}".format(
        instrument=instrument_type, t=t_list, t_now=dt.datetime.now())
    database.run_query(query_string)


if __name__ == '__main__':
    db_connection = DB_MIS.conncet_db()
    # folder_p = os.path.abspath(r'\\192.168.10.2\aia-drive\MIS\Ariana_MIS\UploadFiles_ALL\All_Mapping.csv')
    # df_data = pd.read_csv(folder_p, index_col=None, parse_dates=[3, 4])
    # df_data.dropna(how='all', inplace=True)
    # df_data.to_sql("Mapping", DB_MIS.conncet_db(), if_exists='append', index=False)

    # filer_path = r'D:\Workspace\Numerix_File_Gen\MappingInfo'
    # filep = os.path.join(os.path.abspath(filer_path),'scrip_mapping_latest.csv')
    # df_p = pd.read_csv(filep)
    # unique_instruments = list(set(df_p.Instrument_Type.tolist()))
    # for instrument_type in unique_instruments:
    #     print("Processing{}".format(instrument_type))
    #     temp_df = df_p[df_p.Instrument_Type==instrument_type]
    #     ticker_list = temp_df.UniqueTicker.tolist()
    #     update_mapping(ticker_list,DB_MIS,instrument_type)
