import pandas as pd
import helper as hp
import os
import datetime as dt
import numpy as np


def get_finaL_mis_data(db_con, ticker_list, start_date, end_date, column_list=None, list_date_column=[]):
    """

    :param list_scrip:
    :param start_date:
    :param end_date:
    :param column_list:
    :return:
    """
    if len(ticker_list) == 1:
        t_list = "('{t}')".format(t=ticker_list[0])
    else:
        t_list = tuple(ticker_list)

    if column_list is None:

        query_string = "select fm.MIS_DATE, fm.UniqueTicker,fm.SecurityType,fm.Broker,fm.LastTradeableDate, fm.NET_QTY,fm.StrategyTag,fm.NET_VALUE, fm.FTD_BROKER,fm.FTD_LOCAL,fm.Expired from {t_name} fm where fm.MIS_DATE between '{s_date}' and '{e_date}' and  fm.UniqueTicker in {ls_t}".format(
            t_name=hp.TableName.Final_MIS_Table.value,
            s_date=(start_date), e_date=(end_date), ls_t=t_list)
    else:
        if len(column_list) == 1:
            col_list = "('{t}')".format(t=column_list[0])
        else:
            col_list = tuple(column_list)
        query_string = "select {c_list} from {t_name} where MIS_DATE between '{s_date}' and '{e_date}' where ticker in {ls_t}".format(
            t_name=hp.TableName.Final_MIS_Table.value,
            s_date=(start_date), end_date=(end_date), ls_t=t_list, c_list=col_list)

    df_data = pd.read_sql(query_string, db_con)

    for date_col in list_date_column:
        df_data[date_col] = pd.to_datetime(df_data[date_col])

    return df_data


def get_trades(table_name, begin_date, end_date):
    """
    Generates the Query String
    :param table_name:
    :param begin_date:
    :param end_date:
    :param tuple_ticker:
    :return:
    """

    query_string = "select * from {t_name} where TradeDate between '{s_date}' and '{e_date}' and UniqueTicker in {s_list} and FilledQty>0 and IsNotional is not Null".format(
        s_date=begin_date, e_date=end_date, t_name=table_name)
    return query_string


if __name__ == '__main__':
    base_path = os.path.join(os.path.abspath(r'\\192.168.10.2\aia-drive\MIS\Ariana_MIS'), 'Database')
    # db_name = 'DELTA_MIS_DB - 09-12-2019'
    db_name = 'DELTA_MIS_DB'
    db = hp.Database(base_path, db_name)
    db.conncet_db()
    currency = 'USD'
    start_date = dt.date(2014, 5, 1)
    end_date = dt.date(2019, 12, 31)
    # end = dt.date(2015, 1, 1)
    scrip_query = "select * from Mapping where SecurityType<>'SPOT' and (PnLCurrency1='{cr}' or PnLCurrency2='{cr}')".format(
        cr=currency)
    # scrip_query = "select * from Mapping"
    df_mapping = db.get_query_df(scrip_query)
    df_mapping.to_clipboard()
    scrip_list = df_mapping.UniqueTicker.tolist()
    # scrip_list = ['2330 TT Equity','2303 TT Equity']
    print(len(scrip_list))
    scrip_list = list(set(scrip_list))
    print(len(scrip_list))
    column_list = ['MIS_DATE', 'UniqueTicker', 'SecurityType', 'NET_QTY', "NET_VALUE", 'FTD']
    df_mis = get_finaL_mis_data(db.db_connection, scrip_list, start_date, end_date)
    print(df_mis.MIS_DATE.max())
    df_mis.set_index(['UniqueTicker'], inplace=True)
    df_mapping.set_index(['UniqueTicker'], inplace=True)
    df_mapping = df_mapping.loc[~df_mapping.index.duplicated(keep='first')]
    df_mis["Numerix_IntsrumentType"] = df_mapping['Numerix_IntrumentType']
    df_mis['LastTradeableDate'].fillna(value='Eq', inplace=True)
    df_mis['PNL_Cur1'] = df_mapping['PnLCurrency1']
    df_mis['PNL_Cur2'] = df_mapping['PnLCurrency2']
    df_mis.sort_values('MIS_DATE', ascending=True, inplace=True)
    print(len(df_mis))
    # df_mis = df_mis[df_mis['Expired']==0]
    print(len(df_mis))
    df_t = df_mis.groupby(['UniqueTicker', 'Numerix_IntsrumentType', 'Broker', 'MIS_DATE']).agg(
        {"FTD_LOCAL": np.sum, "NET_QTY": np.sum, "NET_VALUE": np.sum})
    df_t.reset_index(drop=False, inplace=True)
    df_t = df_t[df_t['NET_QTY'] != 0]
    print(len(df_t))
    # df_pi = pd.pivot_table(df_t,index=['Broker','UniqueTicker'],values=['NET_QTY'],columns=['MIS_DATE'])
    # df_pi.reset_index(inplace=True,drop=False)
    # df_pi.set_index(['UniqueTicker'],inplace=True)
    # df_pi['PNL_Cur1'] = df_mapping['PnLCurrency1']
    # df_pi['PNL_Cur2'] = df_mapping['PnLCurrency2']
    # df_pi.to_csv('NET_QTY.csv')
    # df_pi = pd.pivot_table(df_t, index=['Broker', 'UniqueTicker'], values=['NET_VALUE'], columns=['MIS_DATE'])
    # df_pi.reset_index(inplace=True, drop=False)
    # df_pi.set_index(['UniqueTicker'], inplace=True)
    # df_pi['PNL_Cur1'] = df_mapping['PnLCurrency1']
    # df_pi['PNL_Cur2'] = df_mapping['PnLCurrency2']
    # df_pi.to_csv('NET_VALUE.csv')

    df_t = df_mis.groupby(['UniqueTicker', 'Numerix_IntsrumentType', 'Broker', 'LastTradeableDate']).agg(
        {"FTD_LOCAL": np.sum, "NET_QTY": 'last', "NET_VALUE": 'last', 'Expired': 'last'})
    # ix1 = np.logical_or(df_mis['Expired']==1,df_mis['Numerix_IntsrumentType']=='Equity')
    # df_k = df_mis[ix1]
    # df_k = df_k.groupby(['UniqueTicker', 'Numerix_IntsrumentType','Broker','LastTradeableDate']).agg({"NET_VALUE": 'sum'})
    # df_t["NET_VALUE"] = df_k['NET_VALUE']
    # df_t.to_clipboard()
    df_t.reset_index(drop=False, inplace=True)
    df_t.set_index(['UniqueTicker'], inplace=True)
    df_t['To_Include_PnL'] = df_mapping['To_Include_PnL']
    df_t['To_Include_Value'] = df_mapping['To_Include_Value']
    df_t['Is_Quanto'] = df_mapping['Is_Quanto']
    df_k = df_mis.groupby(['UniqueTicker', 'Broker', 'MIS_DATE']).agg({"NET_QTY": 'sum', "NET_VALUE": 'sum'})
    df_k.reset_index(drop=False, inplace=True)
    df_k.sort_values(by=['MIS_DATE'], ascending=True, inplace=True)
    df_k = df_k.groupby(['UniqueTicker', 'Broker']).agg({'NET_QTY': 'last', 'NET_VALUE': 'last'})
    df_k.reset_index(inplace=True)
    df_k.set_index(['UniqueTicker', 'Broker'], inplace=True)
    df_t.reset_index(drop=False, inplace=True)
    df_t.set_index(['UniqueTicker', 'Broker'], inplace=True)
    # df_k = df_k[df_k.MIS_DATE==min_date]
    # df_k.set_index('UniqueTicker',inplace=True)
    df_t['NET_VALUE'] = df_k['NET_VALUE']
    df_t['NET_QTY'] = df_k['NET_QTY']
    df_t['sign'] = np.sign(df_k['NET_QTY'])
    df_t.to_clipboard()
