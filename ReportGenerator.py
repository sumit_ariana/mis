import datetime as dt
import os
import warnings
from enum import Enum

import numpy as np
import pandas as pd

import helper as hp
from helper import VaR

BASE_FOLDER = os.path.abspath("Reports")

EXP_BROKER = ['DBS', 'DBS-A', 'OCBC','OCBC-A']


class Report(Enum):
    """

    """
    mtd_report = 1
    var_report = 2
    pnl_report = 3
    delta_report = 4
    maturity_delta = 6
    cash_report = 5
    strategy_report = 7
    trade_report = 8


def format_mis(writer, ex_date, dict_shape):
    """

    :param writer:
    :return:
    """
    workbook = writer.book
    date_format = workbook.add_format({'num_format': 'd mmm yyyy'})
    worksheets = writer.sheets
    number_format = workbook.add_format({'num_format': '_(* #,##0_);_(* (#,##0);_(* "-"??_);_(@_)'})
    number_format_2 = workbook.add_format({'num_format': '#,##0.00'})
    for sheet in worksheets:
        work_s = writer.sheets[sheet]
        work_s.hide_gridlines(option=2)
        not_sheet = ['Strategy MTM Summary','Trade Summary']
        if sheet not in not_sheet :
            work_s.write_datetime('A1', ex_date, date_format)
        sheet_shape = dict_shape[sheet]
        starting_column = sheet_shape[2]
        number_of_col = sheet_shape[1] + starting_column - 1
        if sheet == 'FTD':
            work_s.set_column(starting_column, number_of_col, 15, number_format_2)
        else:
            work_s.set_column(starting_column, number_of_col, 15, number_format)
        work_s.set_column(0, starting_column, 15)
        if sheet == 'PnL Summary':
            work_s.freeze_panes(1, starting_column)
        elif sheet == 'Near Maturity Position':
            work_s.freeze_panes(1, 3)
        elif "Maturity Delta" in sheet:
            work_s.freeze_panes(1, 1)
        elif "FTD" in sheet:
            work_s.freeze_panes(2, starting_column)
            # work_s.
        elif sheet ==  "Trade Summary":
            work_s.freeze_panes(1, 1)
        else:
            work_s.freeze_panes(3, starting_column)


def generate_trade_report(ex_date, database):
    """
    Prupose :- This Functions generates the Dataframe required for the Trade Report
    :param ex_data:
    :param database:
    :return:
    """
    query_data_trade = "select * from Auto where TradeDate ='{}'".format(ex_date)
    df = database.get_query_df(query_data_trade)

    ticker_list = list(set(df['UniqueTicker'].tolist()))
    group_col = ['TradeDate', 'Broker', 'Exchange', 'UniqueTicker', 'LastTradeableDate', 'SecurityType', 'StrategyTag']
    df = df.pivot_table(index=group_col, columns=['TransactionType'], values=['FilledQty', 'Value'], aggfunc=np.sum)
    df.columns = [s1 + "_" + str(s2) for (s1, s2) in df.columns.tolist()]
    column_list = df.columns.tolist()
    for col in column_list:
        df[col].fillna(value=0, inplace=True)
    if df.empty:
        return df
    df.reset_index(inplace=True)
    df.set_index('UniqueTicker', inplace=True)
    df_close = database.get_close_price(ticker_list, ex_date)
    df_close.drop_duplicates(['UniqueTicker'], inplace=True)
    df_close.set_index(['UniqueTicker'], inplace=True)
    df['Spot'] = df_close['Conversion']
    if 'Value_Buy' in df.columns.tolist():
        df['Dollar_Value_Buy'] = df['Value_Buy'] / df['Spot']
    else:
        df['Value_Buy'] = 0
        df['Dollar_Value_Buy'] = 0

    if 'Value_Sell' in df.columns.tolist():
        df['Dollar_Value_Sell'] = df['Value_Sell'] / df['Spot']
    else:
        df['Value_Sell'] = 0
        df['Dollar_Value_Sell'] = 0

    df.sort_values('StrategyTag', inplace=True)
    return df




def generate_cash_report(ex_date, database):
    """

    :param ex_date:
    :return:
    """
    index_column = ['UniqueTicker', 'Broker']
    df_mis = database.get_mis(ex_date)
    df_mis = df_mis[df_mis['Expired'] == 0]
    df_mapping = database.get_mapping_table(df_mis['UniqueTicker'].tolist())

    df_mis.set_index(index_column, inplace=True)
    df_mapping.set_index(index_column, inplace=True)
    df_mapping = df_mapping.loc[~df_mapping.index.duplicated(keep='first')]
    df_mis['PNL_INCLUDE'] = df_mapping['To_Include_PnL']
    df_mis['VALUE_INCLUDE'] = df_mapping['To_Include_Value']
    df_mis['Broker_Currency'] = df_mapping['Broker_Currency']
    df_mis.reset_index(drop=False, inplace=True)
    ix1 = np.logical_not(np.logical_or(df_mis['PNL_INCLUDE'] == 1, df_mis['VALUE_INCLUDE'] == 1))
    df_mis = df_mis[ix1]

    ticker_list = df_mis['UniqueTicker'].tolist()
    if len(ticker_list) == 1:
        t_list = "('{t}')".format(t=ticker_list[0])
    else:
        t_list = tuple(ticker_list)
    # query_string = f"""select UniqueTicker, Broker, sum(FTD_LOCAL) as TOTAL_LOCAL,
    # Cumm_PnL_Broker as TOTAL_BROKER from FINAL_MIS where MIS_DATE <= '{ex_date}' and
    # UniqueTicker in {t_list} group by UniqueTicker ,Broker"""
    query_string = f"""select UniqueTicker, Broker, sum(Cumm_PnL_LOCAL) as TOTAL_LOCAL, 
    sum(Cumm_PnL_Broker) as TOTAL_BROKER from FINAL_MIS where MIS_DATE = '{ex_date}' and 
    UniqueTicker in {t_list} group by UniqueTicker ,Broker"""
    df_mis_sim = database.get_query_df(query_string)
    df_mis_sim.set_index(index_column, inplace=True)
    df_mis_sim['PNL_INCLUDE'] = df_mapping['To_Include_PnL']
    df_mis_sim['Currency'] = df_mapping['Broker_Currency']
    # df_mis_sim.rename({'Broker_Currency':'Currency'},axis=1,inplace=True)
    df_mis = df_mis_sim.groupby(['Broker', 'Currency']).agg(
        {'TOTAL_LOCAL': np.sum, 'TOTAL_BROKER': np.sum})
    df_cash_mis = database.get_cash_mis(ex_date)
    df_cash_mis = df_cash_mis.groupby(['Broker', 'Currency']).agg(
        {'Total': np.sum, 'Total_Broker': np.sum})
    df_cash_mis = pd.merge(df_cash_mis, df_mis, left_index=True, right_index=True, how='outer')
    df_cash_mis.rename({'TOTAL_LOCAL': 'MTM_LOCAL', 'TOTAL_BROKER': 'MTM_BROKER'}, axis=1,
                       inplace=True)
    # df_cash_mis['MTM_LOCAL'] = df_mis['TOTAL_LOCAL']
    # df_cash_mis['MTM_BROKER'] = df_mis['TOTAL_BROKER']
    df_cash_mis['MTM_LOCAL'].fillna(value=0, inplace=True)
    df_cash_mis['MTM_BROKER'].fillna(value=0, inplace=True)
    df_cash_mis.rename({'Total': 'Cash', 'Total_Broker': 'Broker_Cash'}, axis=1, inplace=True)
    df_cash_mis['Cash'].fillna(value=0, inplace=True)
    df_cash_mis['Broker_Cash'].fillna(value=0, inplace=True)
    df_cash_mis['Broker_NAV'] = df_cash_mis['Broker_Cash'] + df_cash_mis['MTM_BROKER']
    df_cash_mis['NAV'] = df_cash_mis['Cash'] + df_cash_mis['MTM_LOCAL']
    columns_to_sum = df_cash_mis.columns.tolist()
    df_cash_mis.reset_index(drop=False, inplace=True)
    df_p = pd.pivot_table(df_cash_mis, index=['Broker', 'Currency'], values=columns_to_sum,
                          aggfunc=[np.sum],
                          margins=False)

    df_p = df_p.rename(dict(sum='CASH MIS'), axis=1)
    column_order = ['Broker_Cash', 'Cash', 'MTM_BROKER', 'MTM_LOCAL', 'Broker_NAV', 'NAV']
    df_p = df_p.reindex(columns=column_order, level=1)
    return df_p


def generate_pnl_report(ex_date, database, maturity_threshold):
    """

    :param ex_date:
    :return:
    """

    df_mis = database.get_mis(ex_date)
    ticker_list = list(set(df_mis['UniqueTicker'].tolist()))
    df_mapping = database.get_mapping_table(ticker_list)
    df_mapping.drop_duplicates(['UniqueTicker'], inplace=True, keep='first')
    df_mapping.set_index(['UniqueTicker'],inplace=True)
    expense_broker = EXP_BROKER
    ix1 = np.logical_and(df_mis.Broker.isin(expense_broker), df_mis.StrategyTag.str.contains('TREASURY|FX|CASH|'))
    df_mis = df_mis[np.logical_not(ix1)]
    df_mis['Strat'] = df_mis['StrategyTag'].apply(lambda x: '|'.join(x.split('|')[:4]))
    new = df_mis.Strat.str.split("|", n=3, expand=True)
    category_index = []
    columns_to_sum = ['FTD_DOLLAR']
    for i in range(len(new.columns.tolist())):
        column_string = 'Category' + str(i + 1)
        category_index.append(column_string)
        df_mis[column_string] = new[i].str.strip()

    df_mis.Category1 = df_mis.Category1.str.strip()
    rename_dict = {'RISK-ARB': 'RISK ARB', 'TREASURY': 'ARBITRAGE'}
    for keys in rename_dict:
        value = rename_dict[keys]
        df_mis.Category1 = df_mis.Category1.str.replace(keys, value)

    category_index.append('UniqueTicker')
    df_temp_mis = df_mis[df_mis.FTD_DOLLAR != 0]
    df_temp_mis.set_index('UniqueTicker', inplace=True)
    df_dates = database.get_dates(ex_date, 5)
    p_date = df_dates.iloc[0]['Date']
    df_todays_close = database.get_close_price(ticker_list, ex_date)
    df_todays_close.drop_duplicates(['UniqueTicker'],keep='first',inplace=True)
    df_todays_close.set_index('UniqueTicker', inplace=True)
    df_close = database.get_close_price_between_dates(ticker_list, p_date, ex_date)
    df_close = df_close.pivot_table(index='UniqueTicker', columns='Date', values='Close')
    agg_func = {'FTD_DOLLAR': np.sum}
    date_col = []
    df_dates.sort_values(by='Date', ascending=False, inplace=True)
    for ix, each_row in df_dates.iterrows():
        each_date = each_row['Date']
        str_date = each_date.strftime('%Y-%m-%d')
        name_date = "Close_{}".format(str_date)
        df_temp_mis[name_date] = df_close[each_date].fillna(value=0)
        columns_to_sum.append(name_date)
        date_col.append(name_date)
        agg_func[name_date] = np.mean
    df_temp_mis.reset_index(inplace=True, drop=False)
    df_pnl = pd.pivot_table(df_temp_mis, index=category_index, values=columns_to_sum,
                            aggfunc=agg_func,
                            margins=True, margins_name='Grand Total')

    df_pnl.loc['Grand Total', date_col] = np.nan

    df_pnl = df_pnl[columns_to_sum]

    # df_pnl = df_pnl.rename(index=dict(All='Grand Total'))
    df_pnl = df_pnl.rename(dict(sum='FTD INFO'), axis=1)
    df_pnl = put_total_on_top(df_pnl, category_index)

    df_net_qty = pd.pivot_table(df_mis, index=['Broker', 'UniqueTicker'], values=['NET_QTY'],
                                aggfunc=[np.sum],
                                margins=False)
    df_net_qty = df_net_qty.rename(dict(sum='Quantity Details'), axis=1)
    ix1_test = np.logical_and(df_mis['Expired'] == 1, df_mis['SecurityType'] != 'SPOT')
    df_test = df_mis[np.logical_not(ix1_test)]
    df_test.set_index('UniqueTicker',inplace=True)
    df_test[['close','conversion']] = df_todays_close[['Close','Conversion']]
    df_test['multiplier'] = df_mapping['Multiplier']
    df_test['Dollar_Value'] = (df_test['NET_QTY'] * df_test['close'] * df_test['multiplier']) / df_test['conversion']

    df_strategy_pos = pd.pivot_table(df_test,
                                     index=['Category1', 'Category2', 'Category3','Category4', 'UniqueTicker'],
                                     values=['NET_QTY', 'NET_VALUE','Dollar_Value'], aggfunc=np.sum,
                                     margins=False)
    df_strategy_pos = df_strategy_pos[['NET_QTY','NET_VALUE',"Dollar_Value"]]
    df_temp_mis = df_mis[np.logical_not(df_mis.LastTradeableDate.isnull())]
    df_temp_mis['Days'] = df_temp_mis['LastTradeableDate'] - ex_date
    ix1 = df_temp_mis['Days'].dt.days < maturity_threshold
    df_temp_mis = df_temp_mis[ix1]
    df_temp_mis = df_temp_mis.groupby(['Category1', 'UniqueTicker', 'LastTradeableDate', 'Days'])[
        'NET_QTY', 'NET_VALUE'].sum()
    df_temp_mis = df_temp_mis[df_temp_mis['NET_QTY'] != 0]
    df_temp_mis.reset_index(drop=False, inplace=True)
    df_temp_mis.sort_values('Days', inplace=True)
    df_temp_mis.set_index(['Category1', 'UniqueTicker', 'LastTradeableDate', 'Days'], inplace=True)
    # df_temp_mis.reset_index(drop=False,inplace=True)
    return df_pnl, df_net_qty, df_strategy_pos, df_temp_mis


def generate_mtm_report(ex_date, database):
    """

    :param ex_date:
    :param database:
    :return:
    """
    end_date = ex_date
    expense_broker = EXP_BROKER
    year_start_date = dt.date(end_date.year, 1, 1)
    month_start_date = dt.date(end_date.year, end_date.month, 1)
    df_mis = database.get_mis_range(year_start_date, end_date)
    ix1 = np.logical_and(df_mis.Broker.isin(expense_broker), df_mis.StrategyTag.str.contains('TREASURY|FX|CASH'))
    df_expense = df_mis[ix1]
    df_mis = df_mis[np.logical_not(ix1)]

    # df_nav = database.get_nav_range(year_start_date,end_date)
    df_mis['Strat'] = df_mis['StrategyTag'].apply(lambda x: '|'.join(x.split('|')[:4]))
    new = df_mis.Strat.str.split("|", n=3, expand=True)
    category_index = []
    columns_to_sum = ['FTD_DOLLAR']
    for i in range(len(new.columns.tolist())):
        column_string = 'Category' + str(i + 1)
        category_index.append(column_string)
        df_mis[column_string] = new[i].str.strip()
    df_temp = pd.DataFrame()
    j = 0
    df_mis.Category1 = df_mis.Category1.str.strip()
    df_temp_expense = pd.DataFrame(columns=['FTD', 'MTD', 'YTD'], index=['Expense'])
    rename_dict = {'RISK-ARB': 'RISK ARB', 'TREASURY': 'ARBITRAGE', 'DELTA': 'ARBITRAGE'}
    for keys in rename_dict:
        value = rename_dict[keys]
        df_mis.Category1 = df_mis.Category1.str.replace(keys, value)
    for i in [end_date, month_start_date, year_start_date]:
        ix1 = np.logical_and(df_mis['MIS_DATE'] <= end_date, df_mis['MIS_DATE'] >= i)
        df_mis_temp = df_mis.loc[ix1, ['Category1', 'FTD_DOLLAR']]
        df_mis_temp = df_mis_temp.groupby(['Category1']).sum()
        if j == 0:
            column_name = 'FTD'
        elif j == 1:
            column_name = 'MTD'
        elif j == 2:
            column_name = 'YTD'
        ix1 = np.logical_and(df_expense['MIS_DATE'] <= end_date, df_expense['MIS_DATE'] >= i)
        df_temp_expense.loc['Expense', column_name] = df_expense.loc[ix1, 'FTD_DOLLAR'].sum()

        if df_temp.empty:
            df_temp = pd.DataFrame(data=df_mis_temp.iloc[:, 0].values, index=df_mis_temp.index,
                                   columns=[column_name])
        else:
            df_mis_temp.rename(columns={'FTD_DOLLAR': column_name}, inplace=True)
            df_temp = df_temp.merge(df_mis_temp.drop_duplicates(), left_index=True, right_index=True,
                                    how='outer')

        j += 1
    df_temp.index.name = 'Strategy'
    df_total = df_temp.sum(numeric_only=True, axis=0).to_frame(name='Total')
    df_total = df_total.transpose()
    df_temp = pd.concat([df_total, df_temp])
    line = pd.DataFrame({"FTD": np.nan, "MTD": np.nan, 'YTD': np.nan}, index=[np.nan])
    df_temp = pd.concat([df_temp.iloc[:1], line, df_temp.iloc[1:]])
    df_temp = pd.concat([df_temp.iloc[:len(df_temp)], line, df_temp.iloc[len(df_temp):]])
    df_temp = pd.concat([df_temp, df_temp_expense])
    df_temp = pd.concat([df_temp.iloc[:len(df_temp)], line, df_temp.iloc[len(df_temp):]])
    df_temp.loc['Net Total'] = (df_temp.loc['Total', :] + df_temp.loc['Expense', :]).values
    return df_temp


def generate_stratgey_mtm_report(ex_date, database):
    """

    :param ex_date:
    :param database:
    :return:
    """
    end_date = ex_date
    expense_broker = EXP_BROKER
    year_start_date = dt.date(end_date.year, 1, 1)
    month_start_date = dt.date(end_date.year, end_date.month, 1)
    df_mis = database.get_mis_range(year_start_date, end_date)
    ix1 = np.logical_and(df_mis.Broker.isin(expense_broker), df_mis.StrategyTag.str.contains('TREASURY|FX|CASH|'))
    df_mis = df_mis[np.logical_not(ix1)]
    # df_nav = database.get_nav_range(year_start_date,end_date)
    df_mis['Strat'] = df_mis['StrategyTag'].apply(lambda x: '|'.join(x.split('|')[:4]))
    new = df_mis.Strat.str.split("|", n=3, expand=True)
    category_index = []
    columns_to_sum = ['FTD_DOLLAR']
    for i in range(len(new.columns.tolist())):
        column_string = 'Category' + str(i + 1)
        category_index.append(column_string)
        df_mis[column_string] = new[i].str.strip()
    df_temp = pd.DataFrame()
    j = 0
    df_mis.Category1 = df_mis.Category1.str.strip()
    rename_dict = {'RISK-ARB': 'RISK ARB', 'TREASURY': 'ARBITRAGE', 'DELTA': 'ARBITRAGE'}
    for keys in rename_dict:
        value = rename_dict[keys]
        df_mis.Category1 = df_mis.Category1.str.replace(keys, value)
    for i in [end_date, month_start_date, year_start_date]:
        ix1 = np.logical_and(df_mis['MIS_DATE'] <= end_date, df_mis['MIS_DATE'] >= i)
        df_mis_temp = df_mis[ix1]
        # df_mis_temp = df_mis.loc[ix1, ['Category1','Category2','Category3','Category4', 'FTD_DOLLAR']]
        # df_mis_temp = df_mis_temp.groupby(['Category1','Category2','Category3','Category4']).sum()
        df_mis_temp = pd.pivot_table(df_mis_temp, index=['Category1', 'Category2', 'Category3', 'Category4'],
                                     values=['FTD_DOLLAR'],
                                     aggfunc=[np.sum],
                                     margins=False)
        # df.droplevel(0, axis=1)
        df_mis_temp = df_mis_temp.droplevel(0, axis=1)
        # df_mis_temp = df_mis_temp.unstack()
        if j == 0:
            column_name = 'FTD'
        elif j == 1:
            column_name = 'MTD'
        elif j == 2:
            column_name = 'YTD'

        if df_temp.empty:
            df_temp = pd.DataFrame(data=df_mis_temp.iloc[:, 0].values, index=df_mis_temp.index,
                                   columns=[column_name])
        else:
            df_mis_temp.rename(columns={'FTD_DOLLAR': column_name}, inplace=True)
            df_temp = df_temp.merge(df_mis_temp.drop_duplicates(), left_index=True, right_index=True,
                                    how='outer')

        j += 1
    # df_temp.index.name = 'Strategy'
    df_temp.reset_index(drop=False, inplace=True)
    df_total = df_temp.sum(numeric_only=True, axis=0).to_frame(name='Total')
    df_total = df_total.transpose()
    df_temp = pd.concat([df_total, df_temp])
    line = pd.DataFrame({"FTD": np.nan, "MTD": np.nan, 'YTD': np.nan}, index=[np.nan])
    df_temp = pd.concat([df_temp.iloc[:1], line, df_temp.iloc[1:]])
    df_temp.loc['Total', 'Category1'] = 'Total'
    df_temp.fillna(value=0, inplace=True)
    df_temp.iloc[1] = np.nan
    # df_temp.usntack(inplace=True,drop=False)
    return df_temp


def get_delta(ex_date, database):
    """

    :param ex_date:
    :param database:
    :return:
    """
    df_delta = database.get_delta_report(ex_date)
    df_mapping = database.get_mapping_table(df_delta.UniqueTicker.tolist())
    df_delta.set_index(['UniqueTicker'], inplace=True)
    df_mapping.set_index(['UniqueTicker'], inplace=True)
    df_mapping = df_mapping.loc[~df_mapping.index.duplicated(keep='first')]
    df_delta[['Strike', 'CallPut']] = df_mapping[['Strike', 'CallPut']]
    df_delta[["Leg1_Currency", "Leg2_Currency", "Leg3_Currency"]] = df_mapping[
        ["Leg1_Cur", "Leg2_Cur", "Leg3_Cur"]]

    df_close = database.get_close_price([], ex_date)
    ix_close = np.logical_and(df_close['SecurityType'] == 'SPOT', df_close['Broker'] == 'ALL')
    df_close = df_close[ix_close]
    df_close.set_index('UniqueTicker', inplace=True)
    df_delta.reset_index(drop=False, inplace=True)
    ix_option_call = np.logical_and(df_delta.SecurityType == 'OPT', df_delta.CallPut == 'CE')
    ix_option_put = np.logical_and(df_delta.SecurityType == 'OPT', df_delta.CallPut == 'PE')
    df_delta.loc[ix_option_call, ['L1_Delta', 'L2_Delta']] = df_delta.loc[
                                                                 ix_option_call, ['L1_Delta',
                                                                                  'L2_Delta']] * 1.0
    df_delta.loc[ix_option_put, ['L1_Delta', 'L2_Delta']] = df_delta.loc[
                                                                ix_option_put, ['L1_Delta',
                                                                                'L2_Delta']] * -1.0
    ix_option_call = np.logical_and(ix_option_call,
                                    df_delta["Leg1_Currency"].str.contains('PE', regex=False))
    df_delta.loc[ix_option_call, ['L1_Delta', 'L2_Delta']] = df_delta.loc[
                                                                 ix_option_call, ['L1_Delta',
                                                                                  'L2_Delta']] * \
                                                             -1.0
    ix_option_put = np.logical_and(ix_option_put,
                                   df_delta["Leg1_Currency"].str.contains('CE', regex=False))
    df_delta.loc[ix_option_put, ['L1_Delta', 'L2_Delta']] = df_delta.loc[
                                                                ix_option_put, ['L1_Delta',
                                                                                'L2_Delta']] * -1.0
    func_check = lambda x: True if '/USD' in x else False
    df_delta['Test'] = df_delta['L1_Delta'] + df_delta['L2_Delta'] + df_delta['L3_Delta']
    df_delta = df_delta[df_delta['Test'] != 0]
    df_delta.drop('Test', axis=1, inplace=True)
    all_currency = []
    for i in [1, 2, 3]:
        currency_string = 'Leg{}_Currency'.format(str(i))
        new_column = 'Close_Currency_{}'.format(str(i))
        close_column = 'Close_{}'.format(str(i))
        dollar_delta_column = 'L{}_Dollar_Delta'.format(str(i))
        local_delta_column = 'L{}_Delta'.format(str(i))
        df_delta[new_column] = df_delta[currency_string].apply(hp.get_currency)
        all_currency.extend(
            list(set(all_currency).union(df_delta[currency_string].values.tolist())))
        df_delta.set_index(new_column, inplace=True)
        df_close = df_close.loc[~df_close.index.duplicated(keep='first')]
        df_delta[close_column] = df_close['Close']
        df_delta[close_column].fillna(value=1, inplace=True)
        df_delta.reset_index(drop=False, inplace=True)
        ix1 = df_delta[new_column].apply(func_check)
        ix2 = np.logical_not(ix1)
        df_delta.loc[ix1, dollar_delta_column] = df_delta.loc[ix1, local_delta_column] * \
                                                 df_delta.loc[ix1, close_column]
        df_delta.loc[ix2, dollar_delta_column] = df_delta.loc[ix2, local_delta_column] / \
                                                 df_delta.loc[ix2, close_column]
        df_delta.drop([close_column], axis=1, inplace=True)

    all_currency = list(set(all_currency))
    df_delta['Strat'] = df_delta['StrategyTag'].apply(lambda x: '|'.join(x.split('|')[:4]))
    category_index = []
    new = df_delta.Strat.str.split("|", n=3, expand=True)
    for j in range(len(new.columns.tolist())):
        column_string = 'Category' + str(j + 1)
        category_index.append(column_string)
        df_delta[column_string] = new[j]

    return df_delta, all_currency


def put_total_on_top(df_data, category_index):
    """
    This function is to put total delta on top
    :param df_data:
    :return:
    """
    df_data.reset_index(inplace=True)
    df_temp_total = df_data[df_data.Category1 == 'Grand Total']
    df_final = df_data[df_data.Category1 != 'Grand Total']
    df_final = pd.concat([df_temp_total, df_final])
    df_final.set_index(category_index, inplace=True)
    return df_final


def get_seperate_delta(df_raw, strategy_level, header_name):
    """

    :param df_raw:
    :param strategy_level:
    :return:
    """
    df_raw['Strat'] = df_raw['StrategyTag'].apply(lambda x: '|'.join(x.split('|')[:strategy_level]))
    columns_to_sum = list(set((df_raw.columns.tolist())).difference(['Strat', 'StrategyTag']))
    new = df_raw.Strat.str.split("|", n=strategy_level - 1, expand=True)
    category_index = []
    for k in range(len(new.columns.tolist())):
        column_string = 'Category' + str(k + 1)
        category_index.append(column_string)
        df_raw[column_string] = new[k].str.strip()
    ix_band = df_raw['Category1'] == 'RV'
    df_final_rv = df_raw[ix_band]
    df_final = df_raw[np.logical_not(ix_band)]
    df_final = pd.pivot_table(df_final, index=category_index, values=columns_to_sum,
                              aggfunc=[np.sum],
                              margins=True)
    df_final = df_final.rename(index=dict(All='Grand Total'))
    df_final = df_final.rename(dict(sum=header_name), axis=1)
    df_final = put_total_on_top(df_final, category_index)
    df_final_rv = pd.pivot_table(df_final_rv, index=category_index, values=columns_to_sum,
                                 aggfunc=[np.sum],
                                 margins=True)
    df_final_rv = df_final_rv.rename(index=dict(All='Grand Total'))
    df_final_rv = df_final_rv.rename(dict(sum=header_name), axis=1)
    df_final_rv = put_total_on_top(df_final_rv, category_index)
    return df_final, df_final_rv


def generate_strategy_delta(df_delta, all_currencies):
    """

    :param df_delta:
    :return:
    """

    df_t = df_delta.copy()
    df_final_local = pd.DataFrame()
    df_final_dollar = pd.DataFrame()
    df_final_local_rv = pd.DataFrame()
    df_final_dollar_rv = pd.DataFrame()
    for j in [1, 2]:
        # J == 1 is for Local Delta and J == 2 is for Dollar delta
        df_final = pd.DataFrame()
        for i in [1, 2, 3]:
            # i represents the delta of different legs
            currency_string = 'Leg{}_Currency'.format(str(i))
            if j == 1:
                value_string = 'L{}_Delta'.format(str(i))
            elif j == 2:
                value_string = 'L{}_Dollar_Delta'.format(str(i))

            df_temp = df_delta.pivot_table(index=['StrategyTag', currency_string],
                                           values=value_string,
                                           aggfunc=sum).unstack()
            df_temp = df_temp.xs(value_string, axis=1, drop_level=True)
            missing_columns = list(set(all_currencies).difference(df_temp.columns.tolist()))
            df_missing = pd.DataFrame(data=0, columns=missing_columns, index=df_temp.index)
            df_temp = pd.concat([df_temp, df_missing], sort=True)
            df_temp.fillna(value=0, inplace=True)
            if df_final.empty:
                df_final = df_temp.copy()
            else:
                df_final += df_temp
        df_final.reset_index(drop=False, inplace=True)
        # df_final_raw = df_final.copy()
        if j == 1:
            header_name = "Local Delta"
        elif j == 2:
            header_name = "Dollar Delta"

        df_final, df_final_rv = get_seperate_delta(df_final, 5, header_name)

        if j == 1:
            df_final_local = df_final.copy()
            df_final_local_rv = df_final_rv.copy()
        elif j == 2:
            df_final_dollar = df_final.copy()
            df_final_dollar_rv = df_final_rv.copy()

    return df_final_local, df_final_dollar, df_final_local_rv, df_final_dollar_rv


def generate_maturity_delta(df_delta, all_currencies):
    """

    :param df_delta:
    :return:
    """

    # df_t = df_delta.copy()
    df_final_local = pd.DataFrame()
    df_final_dollar = pd.DataFrame()
    df_final_local_rv = pd.DataFrame()
    df_final_dollar_rv = pd.DataFrame()
    for k in [1, 2]:
        # K = 1 is for pure and 2 is for RV
        for j in [1, 2]:
            # J == 1 is for Local Delta and J == 2 is for Dollar delta
            df_final = pd.DataFrame()
            for i in [1, 2, 3]:
                # i represents the delta of different legs
                currency_string = 'Leg{}_Currency'.format(str(i))
                if j == 1:
                    value_string = 'L{}_Delta'.format(str(i))
                elif j == 2:
                    value_string = 'L{}_Dollar_Delta'.format(str(i))
                ix_band = df_delta['Category1'] == 'RV'
                if k == 1:
                    ix_band = np.logical_not(ix_band)
                df_t = df_delta[ix_band]
                if not df_t.empty:
                    df_temp = df_t.pivot_table(index=['LastTradeableDate', currency_string],
                                               values=value_string,
                                               aggfunc=sum).unstack()
                    df_temp = df_temp.xs(value_string, axis=1, drop_level=True)
                else:
                    df_temp = pd.DataFrame()

                missing_columns = list(set(all_currencies).difference(df_temp.columns.tolist()))
                df_missing = pd.DataFrame(data=0, columns=missing_columns, index=df_temp.index)
                df_temp = pd.concat([df_temp, df_missing], sort=True)
                df_temp.fillna(value=0, inplace=True)
                if df_final.empty:
                    df_final = df_temp.copy()
                else:
                    df_final += df_temp
            df_final['Total'] = df_final.sum(axis=1)
            ix_non_zero = df_final['Total'] != 0
            df_final = df_final.loc[ix_non_zero]
            df_final.drop('Total', axis=1, inplace=True)
            df_total = df_final.sum(numeric_only=True, axis=0).to_frame(name='Total')
            df_total = df_total.transpose()
            df_final = pd.concat([df_total, df_final])
            if j == 1:
                if k == 1:
                    df_final_local = df_final.copy()
                elif k == 2:
                    df_final_local_rv = df_final.copy()
            elif j == 2:
                if k == 1:
                    df_final_dollar = df_final.copy()
                elif k == 2:
                    df_final_dollar_rv = df_final.copy()

    return df_final_local, df_final_dollar, df_final_local_rv, df_final_dollar_rv


def generate_var_report(var, ex_date, database):
    """

    :param var:
    :param ex_date:
    :param database:
    :return:
    """
    percentile_level_list = [97.5, 99.0]
    column_name = 'VaR :-'
    column_list = []
    for i in percentile_level_list:
        c_name = "{} Percentile Level".format(str(i))
        column_list.append(c_name)
    df_final = pd.DataFrame(index=[column_name], columns=column_list)
    for percentile_level in percentile_level_list:
        c_name = "{} Percentile Level".format(str(percentile_level))
        var_value = var.calculate_var_number(ex_date, database, 1.0 - (percentile_level / 100.0))
        df_final.loc[column_name, c_name] = var_value
    return df_final


def generate_all_report(ex_date, database):
    """
    :param ex_date:
    :return:
    """
    report_file = os.path.join(BASE_FOLDER, "Report_{}.xlsx".format(str(ex_date.date())))
    var_close_file_path = os.path.join("VaR_Data", "cur_closing.csv")
    var_multi_file_path = os.path.join("VaR_Data", "currency_multiplier.csv")
    print(report_file)
    writer = pd.ExcelWriter(report_file, engine='xlsxwriter')
    dict_shape = {}
    df_delta, all_currency = get_delta(ex_date, database)
    var = VaR(var_close_file_path, var_multi_file_path)
    for report in Report:
        if report == Report.delta_report:
            i = 0
            list_delta = generate_strategy_delta(df_delta, all_currency)
            for df_report in list_delta:
                if i == 0:
                    report_sheet_name = 'Strategy Level Delta Local'
                elif i == 1:
                    report_sheet_name = 'Strategy Level Delta Dollar'
                elif i == 2:
                    report_sheet_name = 'Strategy Level Delta RV Local'
                elif i == 3:
                    report_sheet_name = 'Strategy Level Delta RV Dollar'
                rows, col = df_report.shape
                dict_shape[report_sheet_name] = [rows, col, len(df_report.index.names)]
                df_report.to_excel(writer, report_sheet_name)
                i += 1
        elif report == Report.cash_report:
            df_report = generate_cash_report(ex_date, database)
            report_sheet_name = 'Broker_Cash'
            rows, col = df_report.shape
            dict_shape[report_sheet_name] = [rows, col, len(df_report.index.names)]
            df_report.to_excel(writer, report_sheet_name)
        elif report == Report.pnl_report:
            list_report = generate_pnl_report(ex_date, database, 15)
            i = 0
            for df_report in list_report:
                if i == 0:
                    report_sheet_name = 'FTD'
                elif i == 1:
                    report_sheet_name = 'Global Position'
                elif i == 2:
                    report_sheet_name = 'StrategyWise Position'
                elif i == 3:
                    report_sheet_name = 'Near Maturity Position'

                i += 1
                rows, col = df_report.shape
                dict_shape[report_sheet_name] = [rows, col, len(df_report.index.names)]
                if not df_report.empty:
                    df_report.to_excel(writer, report_sheet_name, float_format='%.2f')
        elif report == Report.mtd_report:
            df_report = generate_mtm_report(ex_date, database)
            report_sheet_name = 'PnL Summary'
            rows, col = df_report.shape
            dict_shape[report_sheet_name] = [rows, col, len(df_report.index.names)]
            df_report.to_excel(writer, report_sheet_name)
        elif report == Report.maturity_delta:
            i = 0
            list_delta = generate_maturity_delta(df_delta, all_currency)
            for df_report in list_delta:
                if i == 0:
                    report_sheet_name = 'Maturity Delta Local'
                elif i == 1:
                    report_sheet_name = 'Maturity Delta Dollar'
                elif i == 2:
                    report_sheet_name = 'Maturity Delta RV Local'
                elif i == 3:
                    report_sheet_name = 'Maturity Delta RV Dollar'
                rows, col = df_report.shape
                dict_shape[report_sheet_name] = [rows, col, len(df_report.index.names)]
                df_report.to_excel(writer, report_sheet_name)
                i += 1
        elif report == Report.var_report:
            df_report = generate_var_report(var, ex_date, database)
            report_sheet_name = 'Delta VaR'
            rows, col = df_report.shape
            dict_shape[report_sheet_name] = [rows, col, len(df_report.index.names)]
            df_report.to_excel(writer, report_sheet_name)
        elif report == Report.strategy_report:
            df_report = generate_stratgey_mtm_report(ex_date, database)
            report_sheet_name = 'Strategy MTM Summary'
            rows, col = df_report.shape
            dict_shape[report_sheet_name] = [rows, col, len(df_report.index.names)]
            df_report.to_excel(writer, report_sheet_name, index=False)
        elif report == Report.trade_report:
            # pass
            df_report = generate_trade_report(ex_date, database)
            report_sheet_name = 'Trade Summary'
            rows, col = df_report.shape
            dict_shape[report_sheet_name] = [rows, col, len(df_report.index.names)]
            df_report.to_excel(writer, report_sheet_name, index=True)

    format_mis(writer, ex_date, dict_shape)
    writer.save()


if __name__ == '__main__':

    warnings.filterwarnings('ignore')
    base_path = os.path.abspath('Database')
    db_name = 'GAF_MIS'
    db = hp.Database(base_path, db_name)
    db.conncet_db()
    # ex_date = dt.datetime.strptime(input("Enter Date:"), "%d-%m-%Y")
    # generate_all_report(ex_date, db)
    df_date_list = db.get_query_df(
        "select Date from DateList where Date>='2014-12-31 00:00:00' and Date<='2015-01-01 00:00:00' order by Date")
    df_date_list['Date'] = pd.to_datetime(df_date_list['Date'])
    df_date_list.set_index(['Date'], inplace=True)
    # ex_date = dt.datetime.strptime(input("Enter Date:"), "%d-%m-%Y")
    # generate_all_report(ex_date, db)

    for ex_date in df_date_list.index.tolist():
        print("Processing for {}".format(str(ex_date)))
        generate_all_report(ex_date, db)

    # # df_t = generate_mtm_report(ex_date, db)
    # # df_t.to_clipboard()
    # print("Processing Done for {}".format(str(ex_date)))
    # list_delta = generate_dollar_delta_report(ex_date, db)
    # report_file = os.path.abspath("Report_{}.xlsx".format(str(ex_date.date())))
    # print(report_file)
    # writer = pd.ExcelWriter(report_file, engine='xlsxwriter')
    # i = 0
    # dict_shape = {}
    # for df_report in list_delta:
    #     if i == 0:
    #         report_sheet_name = 'Delta Local'
    #     elif i == 1:
    #         report_sheet_name = 'Delta Dollar'
    #     elif i == 2:
    #         report_sheet_name = 'Delta RV Local'
    #     elif i == 3:
    #         report_sheet_name = 'Delta RV Dollar'
    #     rows, col = df_report.shape
    #     dict_shape[report_sheet_name] = [rows, col, len(df_report.index.names)]
    #     df_report.to_excel(writer, report_sheet_name)
    #     i += 1
    # writer.save()
