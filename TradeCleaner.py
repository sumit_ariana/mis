import sqlite3
import os
import pandas as pd
from helper import Database

DB_NAME = "Test_DB_MIS"
DB_MIS = Database(DB_NAME)

if __name__ == '__main__':

    db_connection = DB_MIS.conncet_db()
    filer = os.path.abspath("strate_tag_new.csv")
    df_strategy_name = pd.read_csv(filer, index_col=None)

    for index, rowval in df_strategy_name.iterrows():

        broker = rowval['Broker']
        Exchange = rowval['Exchange']
        uni_ticker = rowval['Unique_Ticker']
        classification = rowval['Classification']
        Strategy = rowval['Strategy']
        tag = []
        for i in range(6):
            if i > 0:
                field_name = "Strategy-" + str(i)
                field_value = rowval[field_name]
                tag.append(field_value)
        final_tag = "|".join(tag)

        print (uni_ticker)
        query_string = "UPDATE Auto SET StrategyTag = '{tag}' WHERE Broker = '{b}' and Exchange = '{e}'  and UniqueTicker = '{u}' and Strategy = '{s}' and Classification = '{c}'".format(tag=final_tag, b=broker, e=Exchange, u=uni_ticker,
                                                           s=Strategy, c=classification)
        DB_MIS.run_query(query_string)
