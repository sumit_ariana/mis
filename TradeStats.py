import pandas as pd
import numpy as np
import helper as hp
import os
import datetime as dt
from helper import TableName

def calculate_trades(start_date, end_date, database):
    """

    :param start_date:
    :param end_date:
    :param database:
    :return:
    """

    trade_query = "SELECT * from {t_name} WHERE TradeDate BETWEEN '{s_date}' and '{e_date}' and IsNotional is Null".format(
        s_date=str(start_date),
        e_date=str(end_date),
        t_name=TableName.Trade_Table.value)
    df_data = pd.read_sql(trade_query, database.db_connection, parse_dates=['TradeDate'])
    ticker_list = list(set(df_data['UniqueTicker'].tolist()))
    df_mapping = database.get_mapping_table(ticker_list)
    df_mapping = df_mapping.drop_duplicates(subset='UniqueTicker', keep='first')
    df_mapping.set_index('UniqueTicker', inplace=True)
    df_data.set_index('UniqueTicker', inplace=True)
    df_data['Type'] = df_mapping['Numerix_IntrumentType']
    df_data['CurrenyInfo'] = df_mapping['Broker_Currency']
    ix_instrum = df_data['LastTradeableDate'].isnull()
    df_data['LastTradeableDate'] = pd.to_datetime(df_data['LastTradeableDate'])
    df_data['Expiry'] = df_data['LastTradeableDate'].dt.strftime('%m-%y')
    df_data.loc[ix_instrum, 'Expiry'] = df_data.loc[ix_instrum, 'Type']
    funct_cur = lambda x: 'USD/' + x if x not in hp.CUR_LIST else x + '/USD'
    df_data['Spot_Currency'] = df_data['CurrenyInfo'].apply(funct_cur)
    spot_ticker = list(set(df_data['Spot_Currency'].tolist()))
    if len(spot_ticker) == 1:
        t_list = "('{t}')".format(t=spot_ticker[0])
    else:
        t_list = tuple(spot_ticker)
    spot_close_str = f"""SELECT * FROM '{TableName.Close_Table.value}' WHERE Date BETWEEN '{start_date}' AND '{end_date}' AND UniqueTicker IN {t_list}"""
    df_spot = pd.read_sql(spot_close_str, db.db_connection, parse_dates=['Date'])
    df_spot.rename(columns={'Date': 'TradeDate'}, inplace=True)

    df_spot = df_spot.drop_duplicates(['UniqueTicker', 'TradeDate'], keep='first')
    df_spot.set_index(['UniqueTicker', 'TradeDate'], inplace=True)
    df_data.reset_index(drop=False, inplace=True)
    df_data.set_index(['Spot_Currency', 'TradeDate'], inplace=True)
    df_data['Spot_Price'] = df_spot['Close']
    df_data.reset_index(drop=False, inplace=True)
    ix1 = np.logical_and(df_data['Spot_Price'].isnull(), df_data['CurrenyInfo'] == 'USD')
    df_data.loc[ix1, 'Spot_Price'] = 1.0
    df_data['Dollar_Value'] = df_data['Value'] / df_data['Spot_Price']
    df_data.set_index('UniqueTicker', inplace=True)
    grouping_columns = ['UniqueTicker', 'Broker', 'Type']
    agg_dict = {'FilledQty': np.sum, 'Dollar_Value': np.sum}
    # df_data = df_data.pivot_table(index=grouping_columns, aggfunc=np.sum, values=['FilledQty', 'Dollar_Value'],columns=['Expiry'])
    df_data = pd.pivot_table(df_data, index=grouping_columns, values=['FilledQty', 'Dollar_Value'], columns=['Expiry'],
                             aggfunc=np.sum, margins=False)
    # df_data = df_data.xs(['FilledQty'], axis=1, drop_level=True,level=0)
    df_data.fillna(value=0,inplace=True)
    df_data.to_clipboard()


if __name__ == '__main__':
    base_path = os.path.join(os.path.abspath(r'\\192.168.10.2\aia-drive\MIS\Ariana_MIS'), 'Database')
    db_name = 'DELTA_MIS_DB'
    db = hp.Database(base_path, db_name)
    db.conncet_db()
    start_date = dt.datetime(2017, 1, 1)
    end_date = dt.datetime(2017, 12, 31)
    # df_data = calculate_trades(start_date, end_date, db)
    query_string_1 = "select * from Expense where Date >= '{s_date}' and Date <= '{e_date}'".format(s_date=start_date,
                                                                                                     e_date=end_date)

    query_string_2 = "select * from Subscribe_Redeem where Broker_Date >= '{s_date}' and Broker_Date <= '{e_date}'".format(s_date=start_date,
                                                                                                    e_date=end_date)

    # query_string_2 = "select * from Subscribe_Redeem"

    query_string_3 = "select * from Cash where DATE >= '{s_date}' and DATE <= '{e_date}'".format(
        s_date=start_date,
        e_date=end_date)
    df1 = db.get_query_df(query_string_1)
    df1.to_clipboard()