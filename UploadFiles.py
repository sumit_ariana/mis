import os

import numpy as np
import pandas as pd

from helper import Database

BASE_UPLOAD_FOLDER = os.path.join(os.path.abspath(r'\\192.168.10.2\aia-drive\MIS\Ariana_MIS'), "UploadFiles")
import datetime as dt
from helper import TableName
import helper as hp


# region Global Variables
# base_path = os.path.join(os.path.abspath(r'\\192.168.10.2\aia-drive\MIS\Ariana_MIS'), 'Database')
# db_name = 'DELTA_MIS_DB'
# DB_MIS = hp.Database(base_path, db_name)


# endregion


def upload_file(ex_date, db_connection, table, DB_MIS):
    """

    :param ex_date:
    :param db_connection:
    :return:
    """
    if type(table) != TableName:
        raise ValueError("Table has to be of TableName Type")
    table_name = table.value
    hp.log_info("Processing upload for {}".format(table_name))
    file_path = os.path.join(BASE_UPLOAD_FOLDER, str(ex_date.date()))
    if not os.path.isdir(file_path):
        raise Exception("Folder Does Not Exists")

    dict_rename = {}

    if table == TableName.Close_Table:
        file_path = os.path.join(file_path, ex_date.strftime('%Y%m%d') + '_Close.csv')
        date_col = [0]
        dict_rename = {"Ticker": 'UniqueTicker', 'Security Type': 'SecurityType', 'Broker Close': 'BrokerClose',
                       'Rounding Factor': 'RoundingFactor', 'Conversion Factor': 'ConversionFactor'}
        date_column_name = 'Date'
    elif table == TableName.Raw_Trade_Table:
        file_path = os.path.join(file_path, ex_date.strftime('%Y%m%d') + '_Trade.csv')
        date_col = [0, 4, 5]
        date_column_name = 'TradeDate'
        groupby_column = ['TradeDate', 'Broker', 'Exchange', 'UniqueTicker', 'TransactionType', 'StrategyTag']
        dict_aggregate = {'Symbol': 'first', 'LastTradeableDate': 'first', 'ValueDate': 'first',
                          'SecurityType': 'first', 'CallPut': 'first', 'Multiplier': 'first', 'OTCType': 'first',
                          'FilledQty': np.sum, 'Value': np.sum}
    elif table == TableName.Expense:
        file_path = os.path.join(file_path, ex_date.strftime('%Y%m%d') + '_Expense.csv')
        date_col = [0]
        date_column_name = 'Date'
    elif table == TableName.Daily_Cash_Table:
        file_path = os.path.join(file_path, ex_date.strftime('%Y%m%d') + '_CashTransfer.csv')
        date_col = [0]
        date_column_name = 'Date'
    elif table == TableName.Subscription_Redemption_Table:
        file_path = os.path.join(file_path, ex_date.strftime('%Y%m%d') + '_SUB.csv')
        date_col = [0]
        date_column_name = 'NAV_Date'

    if not os.path.isfile(file_path):
        raise Exception("{} File Does Not Exists".format(file_path))

    # check_query = "select count({columns_name}) from {t_name} where {columns_name} ='{d_date}'".format(t_name=table_name,
    #                                                                                       d_date=str(ex_date),
    #                                                                                       columns_name=date_column_name)
    check_query = True

    if check_query:
        delete_query = "delete from {t_name} where {column_name} = '{d_date}'".format(t_name=table_name,
                                                                                      d_date=str(ex_date),
                                                                                      column_name=date_column_name)

        row_deleted = DB_MIS.run_delete_query(delete_query)
        msg = "Number of Rows Deleted in {} :- {}".format(table_name, str(row_deleted))
        hp.log_info(msg)
        if table == TableName.Raw_Trade_Table:
            delete_query = "delete from {t_name} where {column_name} = '{d_date}'".format(
                t_name=TableName.Trade_Table.value,
                d_date=str(ex_date),
                column_name=date_column_name)
            row_deleted = DB_MIS.run_delete_query(delete_query)
            msg = "Number of Rows Deleted in {} :- {}".format(TableName.Trade_Table.value, str(row_deleted))
            hp.log_info(msg)

        # delete the table information

    df_table_data = pd.read_csv(file_path, parse_dates=date_col, dayfirst=True, index_col=None)
    df_table_data.dropna(how='all', inplace=True)
    df_table_data[date_column_name] = ex_date
    ix_check = df_table_data[date_column_name] == ex_date

    if (ix_check == False).any():
        raise ValueError("File is of the Wrong Date")
    if table == TableName.Raw_Trade_Table:
        df_table_data['Value'] = df_table_data['Value'].astype(np.float)
        df_auto_data = df_table_data.groupby(groupby_column).agg(dict_aggregate)
        df_auto_data.reset_index(inplace=True, drop=False)
        df_auto_data['TradePrice'] = df_auto_data['Value'] / df_auto_data['FilledQty']
        df_auto_data.dropna(how='all', inplace=True)
        df_auto_data.to_sql(TableName.Trade_Table.value, db_connection, if_exists='append', index=False)
        msg = "Number of Rows Updated in {}:- {}".format('AutoTable', str(len(df_auto_data)))
        hp.log_info(msg)
    elif table == TableName.Subscription_Redemption_Table:
        df_table_data['Broker_Date'] = pd.to_datetime(df_table_data['Broker_Date'])
    elif table == TableName.Close_Table:
        if 'MIS_Ticker' in df_table_data.columns:
            df_table_data.drop(columns=['MIS_Ticker'], inplace=True)

    # else:
    #     df_table_data.dropna(axis=1, how='all', inplace=True)
    if dict_rename:
        df_table_data.rename(dict_rename, inplace=True, axis=1)

    df_table_data.dropna(how='all', inplace=True)
    df_table_data.columns = df_table_data.columns.str.strip()
    if not df_table_data.empty:
        df_table_data.to_sql(table_name, db_connection, if_exists='append', index=False)
        msg = "Number of Rows Updated in {}:- {}".format(table_name, str(len(df_table_data)))
    else:
        msg = '{} File is Empty'.format(table_name)
    hp.log_info(msg)


def upload_file_main(ex_date, database):
    """

    :param ex_date:
    :param db_conncection:
    :return:
    """
    list_of_Upload = [TableName.Close_Table, TableName.Raw_Trade_Table, TableName.Daily_Cash_Table,
                      TableName.Subscription_Redemption_Table, TableName.Expense]
    # region UploadSection
    try:
        for table in list_of_Upload:
            upload_file(ex_date, database.db_connection, table, database)
    except Exception as err:
        print("Something went wrong : " + str(err))
    # endregion

    # region Delete MIS Section
    try:
        # region Deleting MIS Tables First
        mis_table_to_delete = [TableName.Cash_MIS_Table, TableName.Final_MIS_Table, TableName.DeltaTable]
        for t_name in mis_table_to_delete:
            if t_name == TableName.Final_MIS_Table:
                d_column_name = 'MIS_DATE'
            elif t_name == TableName.Subscription_Redemption_Table:
                d_column_name = 'NAV_Date'
            else:
                d_column_name = 'Date'
            delete_query = "delete from {t_name} where {column_name} >= '{d_date}'".format(t_name=t_name.value,
                                                                                           d_date=str(ex_date),
                                                                                           column_name=d_column_name)
            row_deleted = database.run_delete_query(delete_query)
            msg = "Number of Rows Deleted in {} :- {}".format(t_name.value, str(row_deleted))
            hp.log_info(msg)
        # endregion
    except Exception as err:
        print("Something went wrong : " + str(err))
    # endregion


# if __name__ == '__main__':
#     db_conn = DB_MIS.conncet_db()
#     list_of_Upload = [TableName.Close_Table, TableName.Raw_Trade_Table, TableName.Daily_Cash_Table,
#                       TableName.Subscription_Redemption_Table, TableName.Expense]
#
#     date_list = os.path.abspath('Settings\Date_List.csv')
#     df_list = pd.read_csv(date_list, index_col=0, parse_dates=True)
#     try:
#         ex_date = dt.datetime.strptime(input("Enter Date:"), "%d-%m-%Y")
#
#         for table in list_of_Upload:
#             upload_file(ex_date, db_conn, table)
#     except Exception as err:
#         print("Something went wrong : " + str(err))
#     try:
#         # region Deleting MIS Tables First
#         mis_table_to_delete = [TableName.Cash_MIS_Table, TableName.Final_MIS_Table, TableName.DeltaTable]
#         for t_name in mis_table_to_delete:
#             if t_name == TableName.Final_MIS_Table:
#                 d_column_name = 'MIS_DATE'
#             else:
#                 d_column_name = 'Date'
#             delete_query = "delete from {t_name} where {column_name} >= '{d_date}'".format(t_name=t_name.value,
#                                                                                            d_date=str(ex_date),
#                                                                                            column_name=d_column_name)
#             row_deleted = DB_MIS.run_delete_query(delete_query)
#             msg = "Number of Rows Deleted in {} :- {}".format(t_name.value, str(row_deleted))
#             hp.log_info(msg)
#         # endregion
#     except Exception as err:
#         print("Something went wrong : " + str(err))
#     ans = input("Press enter to close program")
