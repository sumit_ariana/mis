import os

import numpy as np
import pandas as pd

from helper import Database

BASE_UPLOAD_FOLDER = os.path.join(os.path.abspath(r'\\192.168.10.2\aia-drive\MIS\Ariana_MIS'),"UploadFiles_ALL")
import datetime as dt
from helper import TableName
import helper as hp

# region Global Variables
base_path = os.path.join(os.path.abspath(r'\\192.168.10.2\aia-drive\MIS\Ariana_MIS'), 'Database')
db_name = 'DELTA_MIS_DB'
DB_MIS = hp.Database(base_path, db_name)


# endregion


def upload_file(ex_date, db_connection, table):
    """

    :param ex_date:
    :param db_connection:
    :return:
    """
    if type(table) != TableName:
        raise ValueError("Table has to be of TableName Type")
    table_name = table.value
    hp.log_info("Processing upload for {}".format(table_name))
    # file_path = os.path.join(BASE_UPLOAD_FOLDER, str(ex_date.date()))
    # if not os.path.isdir(file_path):
    #     raise Exception("Folder Does Not Exists")

    dict_rename = {}

    if table == TableName.Close_Table:

        file_path = r'X:\Ariana_MIS\UploadFiles_ALL\All_Close.csv'
        date_col = [0]
        dict_rename = {"Ticker": 'UniqueTicker', 'Security Type': 'SecurityType', 'Broker Close': 'BrokerClose',
                       'Rounding Factor': 'RoundingFactor', 'Conversion Factor': 'ConversionFactor'}
        date_column_name = 'Date'
    elif table == TableName.Raw_Trade_Table:

        file_path = r'X:\Ariana_MIS\UploadFiles_ALL\All_Trades.csv'
        date_col = [0, 4, 5]
        date_column_name = 'TradeDate'
        groupby_column = ['TradeDate', 'Broker', 'Exchange', 'UniqueTicker', 'TransactionType', 'StrategyTag']
        dict_aggregate = {'Symbol': 'first', 'LastTradeableDate': 'first', 'ValueDate': 'first',
                          'SecurityType': 'first', 'CallPut': 'first', 'Multiplier': 'first', 'OTCType': 'first',
                          'FilledQty': np.sum, 'Value': np.sum}
    elif table == TableName.Expense:

        file_path = r'X:\Ariana_MIS\UploadFiles_ALL\All_Expenses.csv'
        date_col = [0]
        date_column_name = 'Date'
    elif table == TableName.Daily_Cash_Table:

        file_path = r'X:\Ariana_MIS\UploadFiles_ALL\All_CashTransfer.csv'
        date_col = [0]
        date_column_name = 'Date'
    elif table == TableName.Subscription_Redemption_Table:
        file_path = os.path.join(os.path.abspath(r'\\192.168.10.2\aia-drive\MIS\Ariana_MIS\UploadFiles_ALL'),"All_SUB_RED.csv")
        date_col = [0,1]
        date_column_name = 'Broker_Date'

    if not os.path.isfile(file_path):
        raise Exception("{} File Does Not Exists".format(file_path))

    # check_query = "select count({columns_name}) from {t_name} where {columns_name} ='{d_date}'".format(t_name=table_name,
    #                                                                                       d_date=str(ex_date),
    #                                                                                       columns_name=date_column_name)
    check_query = True


    if check_query:
        delete_query = "delete from {t_name} where {column_name} = '{d_date}'".format(t_name=table_name,
                                                                                      d_date=str(ex_date),
                                                                                      column_name=date_column_name)

        row_deleted = DB_MIS.run_delete_query(delete_query)
        msg = "Number of Rows Deleted in {} :- {}".format(table_name, str(row_deleted))
        hp.log_info(msg)
        if table == TableName.Raw_Trade_Table:
            delete_query = "delete from {t_name} where {column_name} = '{d_date}'".format(t_name=TableName.Trade_Table.value,
                                                                                          d_date=str(ex_date),
                                                                                          column_name=date_column_name)
            row_deleted = DB_MIS.run_delete_query(delete_query)
            msg = "Number of Rows Deleted in {} :- {}".format(TableName.Trade_Table.value, str(row_deleted))
            hp.log_info(msg)



        # delete the table information

    df_table_data = pd.read_csv(file_path, parse_dates=date_col, dayfirst=True, index_col=None)

    if table == TableName.Raw_Trade_Table:
        df_auto_data = df_table_data.groupby(groupby_column).agg(dict_aggregate)
        df_auto_data.reset_index(inplace=True,drop=False)
        df_auto_data['TradePrice'] = df_auto_data['Value'] / df_auto_data['FilledQty']
        df_auto_data.dropna(how='all', inplace=True)
        df_auto_data.to_sql(TableName.Trade_Table.value, db_connection, if_exists='append', index=False)
        msg = "Number of Rows Updated in {}:- {}".format('AutoTable', str(len(df_auto_data)))
        hp.log_info(msg)

    if dict_rename:
        df_table_data.rename(dict_rename, inplace=True, axis=1)

    df_table_data.dropna(how='all', inplace=True)

    if not df_table_data.empty:
        df_table_data.to_sql(table_name, db_connection, if_exists='append', index=False)
        msg = "Number of Rows Updated in {}:- {}".format(table_name, str(len(df_table_data)))
    else:
        msg = '{} File is Empty'.format(table_name)
    hp.log_info(msg)


if __name__ == '__main__':
    db_conn = DB_MIS.conncet_db()


    list_of_Upload = [TableName.Subscription_Redemption_Table]

    try:
        ex_date = dt.datetime.strptime(input("Enter Date:"), "%d-%m-%Y")

        for table in list_of_Upload:
            upload_file(ex_date, db_conn, table)
    except Exception as err:
        print("Something went wrong : " + str(err))
    try:
        # region Deleting MIS Tables First
        mis_table_to_delete = [TableName.Cash_MIS_Table, TableName.Final_MIS_Table, TableName.DeltaTable]
        for t_name in mis_table_to_delete:
            if t_name == TableName.Final_MIS_Table:
                d_column_name = 'MIS_DATE'
            else:
                d_column_name = 'Date'
            delete_query = "delete from {t_name} where {column_name} >= '{d_date}'".format(t_name=t_name.value,
                                                                                           d_date=str(ex_date),
                                                                                           column_name=d_column_name)
            row_deleted = DB_MIS.run_delete_query(delete_query)
            msg = "Number of Rows Deleted in {} :- {}".format(t_name.value, str(row_deleted))
            hp.log_info(msg)
        # endregion
    except Exception as err:
        print("Something went wrong : " + str(err))
    ans = input("Press enter to close program")

