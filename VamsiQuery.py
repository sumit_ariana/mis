import pandas as pd
import helper as hp
import os
import datetime as dt
import numpy as np


def get_currency_close(start_date, end_date, scrip_name, instrument_type,df_expiry_date):
    """

    :param start_date:
    :param end_date:
    :param scrip_name:
    :param instrument_type:
    :return:
    """
    scrip_data = []
    list_of_months = ['F', 'G', 'H', 'J', 'K', 'M', 'N', 'Q', 'U', 'V', 'X', 'Z']
    dict_test = {}
    name_of_scrip = scrip_name

    month_counter = 1
    for i in list_of_months:
        scrip_query = "select * from Mapping where SecurityType='{}' and UniqueTicker like '{}%' and UniqueTicker not like '%=%'".format(
            instrument_type,
            name_of_scrip + i)
        temp_df = db.get_query_df(scrip_query)
        scrip_data.extend(temp_df.UniqueTicker.tolist())
        dict_test[i] = month_counter
        month_counter += 1
    scrip_data = list(set(scrip_data))
    df_mapping = db.get_mapping_table(scrip_data)
    df_mapping = df_mapping.drop_duplicates(subset=['UniqueTicker'], keep='first')
    df_mapping['LastTradeableDate'] = pd.to_datetime(df_mapping['LastTradeableDate'])
    df_mapping.set_index(['UniqueTicker'], inplace=True)
    df_close = db.get_close_price_between_dates(scrip_data, start_date, end_date)
    df_close['Date'] = pd.to_datetime(df_close['Date'])
    df_close.set_index(['UniqueTicker'], inplace=True)
    df_close['LastTradeableDate'] = df_mapping['LastTradeableDate']
    df_close['LastTradeableDate'] = pd.to_datetime(df_close['LastTradeableDate'])
    df_close.reset_index(drop=False,inplace=True)
    df_close.set_index(['Date'], inplace=True)
    df_close['NDF_Expiry'] = df_expiry_date['NDF_Expiry']
    df_close['NDF_Expiry'] = pd.to_datetime(df_close['NDF_Expiry'])
    df_close['Minic'] = df_close['LastTradeableDate'] - df_close['NDF_Expiry']
    ix1 = df_close['Minic'].dt.days == 0.0
    df_close = df_close[ix1]
    df_close.reset_index(inplace=True, drop=False)
    df_close = df_close[['Date', 'UniqueTicker', 'Broker','LastTradeableDate', 'Close']]
    func_t = lambda x: dict_test[x[len(name_of_scrip):][0]]
    df_close['Month_Number'] = df_close['UniqueTicker'].apply(func_t)
    df_close.sort_values(by=['Date', 'Month_Number'], inplace=True)
    df_close = df_close.drop_duplicates(subset=['Date'], keep='first')
    df_close.set_index('Date', inplace=True)
    return df_close


def get_ndf_close(start_date, end_date, currency_pair):
    """

    :param start_date:
    :param end_date:
    :param currency_pair:
    :return:
    """
    query_string = "select * from FINAL_MIS where MIS_DATE between '{s_date}' and  '{e_date}' and UniqueTicker like '{cur} %' and Exchange = 'OTC' and SecurityType='FWD' and NET_QTY<>0 and Expired = 0 and StrategyTag like '%LINEAR|INR%' ".format(
        s_date=start_date, e_date=end_date, cur=currency_pair)
    temp_df = db.get_query_df(query_string)
    temp_df['MIS_DATE'] = pd.to_datetime(temp_df['MIS_DATE'])
    temp_df = temp_df.groupby(['MIS_DATE','UniqueTicker','LastTradeableDate']).agg({'NET_QTY':np.sum})
    temp_df['ABS_NET_QTY'] = np.abs(temp_df['NET_QTY'])
    temp_df.reset_index(inplace=True,drop=False)
    temp_df.sort_values(by=['MIS_DATE','LastTradeableDate','ABS_NET_QTY'], inplace=True, ascending=[True,True,False])
    # temp_df.sort_values(by=['MIS_DATE', 'LastTradeableDate'], inplace=True,
    #                     ascending=[True, True])

    df_final = temp_df.drop_duplicates(subset=['MIS_DATE'], keep='first')

    df_final.rename(columns={'MIS_DATE': 'Date'}, inplace=True)
    list_scrrip = df_final['UniqueTicker'].tolist()
    df_final.set_index(['Date', 'UniqueTicker'], inplace=True)

    df_close = db.get_close_price_between_dates(list_scrrip, start_date, end_date)
    df_close = df_close.drop_duplicates(subset=['Date', 'UniqueTicker'], keep='first')
    df_close.set_index(['Date', 'UniqueTicker'], inplace=True)
    # df_final_close = pd.DataFrame(index=[df_final['Date'], df_final['UniqueTicker']])
    df_final['Close'] = df_close['Close']
    df_final.reset_index(inplace=True)
    df_mapping = db.get_mapping_table(list_scrrip)
    df_mapping = df_mapping.drop_duplicates(subset=['UniqueTicker'], keep='first')
    df_mapping['ValueDate'] = pd.to_datetime(df_mapping['ValueDate'])
    df_mapping.set_index(['UniqueTicker'], inplace=True)
    df_final.set_index(['UniqueTicker'], inplace=True)
    df_final['ValueDate'] = df_mapping['ValueDate']
    df_final.reset_index(drop=False, inplace=True)
    func_char = lambda x: '{cur} {va_date}x{mis_date} BGN Curncy'.format(cur=currency_pair,
                                                                         va_date=dt.datetime.strftime(x['ValueDate'],
                                                                                                      '%m%d%y'),
                                                                         mis_date=dt.datetime.strftime(x['Date'],
                                                                                                       '%m%d%y'))
    df_final['Spot_Calculation'] = df_final.apply(func_char, axis=1)
    df_final.set_index(['Date'], inplace=True)
    return df_final


if __name__ == '__main__':
    base_path = os.path.join(os.path.abspath(r'\\192.168.10.2\aia-drive\MIS\Ariana_MIS'), 'Database')
    db_name = 'DELTA_MIS_DB'
    db = hp.Database(base_path, db_name)
    db.conncet_db()
    currency = 'USD'
    start_date = dt.date(2018, 1, 1)
    end_date = dt.date(2020, 3, 31)
    df_final_close = pd.DataFrame(index=pd.date_range(start_date, end_date, freq='B'))
    temp_df = get_ndf_close(start_date, end_date, 'USD/INR')
    df_final_close['NDF'] = temp_df['Close']
    df_final_close['NDF_Expiry'] = temp_df['LastTradeableDate']
    df_final_close['BBG_S'] = temp_df['Spot_Calculation']

    symbols = ['INT', 'IRD']
    # symbols = [ 'INB']
    for i in symbols:
        temp_df = get_currency_close(start_date, end_date, i, 'FUT', df_final_close['NDF_Expiry'].to_frame())
        df_final_close[i] = temp_df['Close']
        df_final_close[i+'_Expiry'] = temp_df['LastTradeableDate']
        if i == "IRD":
            df_final_close[i] = 10000.0 / df_final_close[i]
        df_final_close[i + '_spread'] = df_final_close['NDF'] - df_final_close[i]
        df_final_close[i + '_spread'].fillna(method='FFILL', inplace=True)
        df_final_close[i].fillna(df_final_close['NDF'] - df_final_close[i + '_spread'], inplace=True)
    for i in symbols:
        ix1 = np.logical_and(df_final_close[i + '_spread'] == 0, df_final_close[i+'_Expiry'].isnull())
        if i =='INT':
            j = 'IRD'
        elif i == 'IRD':
            j = 'INT'
        df_final_close.loc[ix1,i] = df_final_close.loc[ix1,j]
    df_final_close.to_clipboard()
    # end = dt.date(2015, 1, 1)
