# region Import

import sqlite3
import os
from enum import Enum
import pandas as pd
import numpy as np
from dateutil import relativedelta as rd
import datetime as dt
# endregion

DB_NAME = "Test_DB_MIS"

CUR_LIST = ['EUR', 'GBP', 'AUD']
NEUTRAL_CUR = ['USD', 'XAU', 'XAG', 'XCU', 'XAL', 'XZC']


class Database:
    """

    """

    base_path = os.path.join(os.path.abspath(r'\\192.168.10.20\mis'), 'Database')

    def __init__(self, db_path, db_name):
        self.db_path = os.path.join(db_path, db_name + ".db")
        self.TableName = TableName
        self.trade_table = "Auto"

        self.conncet_db()

    def conncet_db(self):
        """

        :return:
        """
        self.db_connection = sqlite3.connect(self.db_path)
        return self.db_connection

    def close_connection(self):
        """

        :return:
        """
        self.db_connection.close()

    def run_query(self, query_string):
        """

        :param query_string:
        :return:
        """
        cur = self.db_connection.cursor()
        cur.execute(query_string)
        self.db_connection.commit()
        return cur.rowcount

    def get_mis(self, ex_date, column_list=None):
        """

        :param db_connection:
        :return:
        """

        if column_list is None:
            query_string = "select * from {t_name} where MIS_DATE = '{d_date}'".format(
                t_name=self.TableName.Final_MIS_Table.value,
                d_date=(ex_date))
        else:
            if len(column_list) == 1:
                t_list = "('{t}')".format(t=column_list[0])
            else:
                t_list = tuple(column_list)
            query_string = "select {c_list} from {t_name} where MIS_DATE = '{d_date}'".format(
                t_name=self.TableName.Final_MIS_Table.value,
                d_date=(ex_date), c_list=",".join(t_list))

        df_data = pd.read_sql(query_string, self.db_connection)
        # region Converting String to Datetime
        ret_columns = df_data.columns.tolist()
        if 'MIS_DATE' in ret_columns:
            df_data['MIS_DATE'] = pd.to_datetime(df_data['MIS_DATE'])
        if 'LastTradeableDate' in ret_columns:
            df_data['LastTradeableDate'] = pd.to_datetime(df_data['LastTradeableDate'])
        if 'EXPIRED_ON' in ret_columns:
            df_data['EXPIRED_ON'] = pd.to_datetime(df_data['EXPIRED_ON'])
        if 'Expired' in ret_columns:
            df_data['Expired'] = df_data['Expired'].astype(bool)
        # endregion
        return df_data

    def get_mis_range(self, start_date, end_date, column_list=None):
        """

        :param db_connection:
        :return:
        """

        if column_list is None:
            query_string = "select * from {t_name} where MIS_DATE between '{s_date}' and '{e_date}' ".format(
                t_name=self.TableName.Final_MIS_Table.value,
                s_date=(start_date), e_date=(end_date))
        else:
            if len(column_list) == 1:
                t_list = "('{t}')".format(t=column_list[0])
            else:
                t_list = tuple(column_list)
            query_string = "select {c_list} from {t_name} where between '{s_date}' and '{e_date}'".format(
                t_name=self.TableName.Final_MIS_Table.value,
                s_date=(start_date), e_date=(end_date), c_list=",".join(t_list))

        df_data = pd.read_sql(query_string, self.db_connection)
        # region Converting String to Datetime
        ret_columns = df_data.columns.tolist()
        if 'MIS_DATE' in ret_columns:
            df_data['MIS_DATE'] = pd.to_datetime(df_data['MIS_DATE'])
        if 'LastTradeableDate' in ret_columns:
            df_data['LastTradeableDate'] = pd.to_datetime(df_data['LastTradeableDate'])
        if 'EXPIRED_ON' in ret_columns:
            df_data['EXPIRED_ON'] = pd.to_datetime(df_data['EXPIRED_ON'])
        if 'Expired' in ret_columns:
            df_data['Expired'] = df_data['Expired'].astype(bool)
        # endregion
        return df_data

    def get_cash_mis(self, ex_date, column_list=None):
        """

        :param db_connection:
        :return:
        """

        if column_list is None:
            query_string = "select * from {t_name} where DATE = '{d_date}'".format(
                t_name=self.TableName.Cash_MIS_Table.value,
                d_date=(ex_date))
        else:
            if len(column_list) == 1:
                t_list = "('{t}')".format(t=column_list[0])
            else:
                t_list = tuple(column_list)
            query_string = "select {c_list} from {t_name} where DATE = '{d_date}'".format(
                t_name=self.TableName.Final_MIS_Table.value,
                d_date=(ex_date), c_list=",".join(t_list))

        df_data = pd.read_sql(query_string, self.db_connection)
        # region Converting String to Datetime
        ret_columns = df_data.columns.tolist()
        if 'Date' in ret_columns:
            df_data['Date'] = pd.to_datetime(df_data['Date'])
        # endregion
        return df_data

    def get_delta_report(self, ex_date, column_list=None):
        """

        :param db_connection:
        :return:
        """

        if column_list is None:
            query_string = "select * from {t_name} where DATE = '{d_date}'".format(
                t_name=self.TableName.DeltaTable.value,
                d_date=(ex_date))
        else:
            if len(column_list) == 1:
                t_list = "('{t}')".format(t=column_list[0])
            else:
                t_list = tuple(column_list)
            query_string = "select {c_list} from {t_name} where DATE = '{d_date}'".format(
                t_name=self.TableName.DeltaTable.value,
                d_date=(ex_date), c_list=",".join(t_list))

        df_data = pd.read_sql(query_string, self.db_connection)
        # region Converting String to Datetime
        ret_columns = df_data.columns.tolist()
        if 'DATE' in ret_columns:
            df_data['MIS_DATE'] = pd.to_datetime(df_data['MIS_DATE'])
        if 'LastTradeableDate' in ret_columns:
            df_data['LastTradeableDate'] = pd.to_datetime(df_data['LastTradeableDate'])

        # endregion
        return df_data

    def get_nav_range(self, start_date, end_date):
        """

        :param db_connection:
        :return:
        """
        if start_date > end_date.date():
            raise ValueError("Starting Date cannot be greater than End Date")

        query_string = "select * from {t_name} where between '{s_date}' and '{e_date}'".format(
            t_name=self.TableName.NAV_TABLE.value,
            s_date=(start_date), e_date=(end_date))

        df_data = pd.read_sql(query_string, self.db_connection)
        # region Converting String to Datetime
        ret_columns = df_data.columns.tolist()
        if 'REPORT_DATE' in ret_columns:
            df_data['REPORT_DATE'] = pd.to_datetime(df_data['REPORT_DATE'])
        # endregion
        return df_data

    def run_delete_query(self, query_string):
        """

        :param query_string:
        :return:
        """
        cur = self.db_connection.cursor()
        cur.execute(query_string)
        self.db_connection.commit()
        return cur.rowcount

    def check_entry(self, query_string):
        """

        :param query_string:
        :return:
        """
        cur = self.db_connection.cursor()
        cur.execute(query_string)
        data = cur.fetchone()[0]
        if data != 0:
            return True
        else:
            return False

    def query_result(self, query_string):
        """

        :param query_string:
        :return:
        """
        cur = self.db_connection.cursor()
        cur.execute(query_string)
        data = cur.fetchone()[0]
        return data

    def get_query_df(self, query_string):
        """

        :param query_string:
        :return:
        """
        df_data = pd.read_sql(query_string, self.db_connection)
        return df_data

    def get_trade_report(self, ex_date):
        """

        :param ex_date:
        :return:
        """
        UNI_INDEX = ['UniqueTicker', 'Broker', 'Exchange', 'StrategyTag', 'SecurityType','ValueDate']
        query_string = "select * from {t_name} where TradeDate = '{d_date}'".format(t_name=self.trade_table,
                                                                                    d_date=ex_date)
        df_data = pd.read_sql(query_string, self.db_connection)
        # region Converting String to Datetime
        df_data['TradeDate'] = pd.to_datetime(df_data['TradeDate'])
        df_data['LastTradeableDate'] = pd.to_datetime(df_data['LastTradeableDate'])
        df_data['ValueDate'] = pd.to_datetime(df_data['ValueDate'])
        ix1 = df_data.TransactionType == 'Sell'
        df_data.loc[ix1, 'FilledQty'] *= -1.0
        df_data.loc[np.logical_not(ix1), 'Value'] *= -1.0
        if df_data.empty:
            l_columns = UNI_INDEX.copy()
            l_columns.append('FilledQty')
            l_columns.append('Value')
            l_columns.append('LastTradeableDate')
            df_data = df_data[l_columns]
        else:
            df_data = df_data.groupby(by=UNI_INDEX).agg(
                {'FilledQty': np.sum, 'Value': np.sum, 'LastTradeableDate': 'first','ValueDate': 'first'})
            df_data.reset_index(drop=False, inplace=True)
        # endregion
        return df_data

    def get_mapping_table(self, ticker_list):
        """
        :param db_connection
        :return: Mapping Table
        """
        if len(ticker_list) == 1:
            t_list = "('{t}')".format(t=ticker_list[0])
        else:
            t_list = tuple(ticker_list)
        query_str = f"""select *  from '{self.TableName.Mapping_Table.value}' where UniqueTicker in {t_list}"""
        df_data = pd.read_sql(query_str, self.db_connection)
        df_data['LastTradeableDate'] = pd.to_datetime(df_data['LastTradeableDate'])
        return df_data

    def get_close_price(self, ticker_list, ex_date):
        """

        :param ex_date:
        :return:
        """
        if len(ticker_list) == 1:
            t_list = "('{t}')".format(t=ticker_list[0])
        else:
            t_list = tuple(ticker_list)
        if len(ticker_list) == 0:
            query_str = f"""SELECT * FROM '{TableName.Close_Table.value}' WHERE Date BETWEEN '{ex_date}' AND '{ex_date}'"""
        else:
            query_str = f"""SELECT * FROM '{TableName.Close_Table.value}' WHERE Date BETWEEN '{ex_date}' AND '{ex_date}' AND UniqueTicker IN {t_list}"""
        df_close = pd.read_sql(query_str, con=self.db_connection)
        df_close['Date'] = pd.to_datetime(df_close['Date'])
        return df_close

    def delete_entry_for_date(self, table, ex_date, date_column):
        """

        :param ex_date:
        :return:
        """
        delete_query = "delete from {t_name} where {column_name} >= '{d_date}'".format(t_name=table.value,
                                                                                       d_date=str(ex_date),
                                                                                       column_name=date_column)
        self.run_query(delete_query)

    def delete_mis_entries_for_date(self, ex_date):
        """

        :param ex_date:
        :return:
        """
        mis_tables = [TableName.Final_MIS_Table, TableName.Cash_MIS_Table, TableName.DeltaTable]
        for table in mis_tables:
            if table == TableName.Final_MIS_Table:
                d_column_name = 'MIS_DATE'
            else:
                d_column_name = 'Date'
            self.delete_entry_for_date(table, ex_date, d_column_name)

    def get_stratgey_trade(self, stratgey_tag, start_date, end_date):
        """

        :param ex_date:
        :return:
        """
        UNI_INDEX = ['UniqueTicker', 'Broker', 'Exchange', 'StrategyTag', 'SecurityType']
        query_string = "select * from {t_name} where StrategyTag like '%{s_tag}%' and  TradeDate between '{s_date}' and '{e_date}'".format(
            t_name='TradeFile', s_date=start_date, e_date=end_date, s_tag=stratgey_tag)
        df_data = pd.read_sql(query_string, self.db_connection)
        # region Converting String to Datetime
        df_data['TradeDate'] = pd.to_datetime(df_data['TradeDate'])
        df_data['LastTradeableDate'] = pd.to_datetime(df_data['LastTradeableDate'])
        df_data['ValueDate'] = pd.to_datetime(df_data['ValueDate'])
        ix1 = df_data.TransactionType == 'Sell'
        df_data.loc[ix1, ['FilledQty', 'Value']] *= -1.0
        # df_data.loc[ix1, 'Value'] *= -1.0
        if df_data.empty:
            l_columns = UNI_INDEX.copy()
            l_columns.append('FilledQty')
            l_columns.append('Value')
            l_columns.append('LastTradeableDate')
            df_data = df_data[l_columns]
        else:
            df_data = df_data.groupby(by=UNI_INDEX).agg(
                {'FilledQty': np.sum, 'Value': np.sum, 'LastTradeableDate': 'first',
                 'Strike': 'first', 'CallPut': 'first'})
            df_data.reset_index(drop=False, inplace=True)
        # endregion
        return df_data

    def get_close_price_between_dates(self,ticker_list, start_date, end_date):
        """

        :param ex_date:
        :return:
        """
        if len(ticker_list) == 1:
            t_list = "('{t}')".format(t=ticker_list[0])
        else:
            t_list = tuple(ticker_list)
        if len(ticker_list) == 0:
            query_str = f"""SELECT * FROM '{TableName.Close_Table.value}' WHERE Date BETWEEN '{start_date}' AND '{end_date}'"""
        else:
            query_str = f"""SELECT * FROM '{TableName.Close_Table.value}' WHERE Date BETWEEN '{start_date}' AND '{end_date}' AND UniqueTicker IN {t_list}"""
        df_close = pd.read_sql(query_str, con=self.db_connection)
        df_close['Date'] = pd.to_datetime(df_close['Date'])
        return df_close

    def get_dates(self,ex_date,window):
        """

        :param ex_date:
        :param window:
        :return:
        """
        p_date = ex_date - dt.timedelta(days=window+5)
        query_str = f"""SELECT * FROM '{TableName.Date_List_Table.value}' WHERE Date BETWEEN '{p_date}' AND '{ex_date}'"""
        df = self.get_query_df(query_str)
        df['Date'] = pd.to_datetime(df['Date'])
        df.sort_values(by='Date', inplace=True)
        df = df.iloc[-window:]

        return df



class Menu(Enum):
    """
    Class contains the menu items
    """
    # delete_mis_entry = "Delete MIS for a Date"
    run_mis = "Run MIS For a Data"


class MainMenu(Enum):
    """
    Contains Enum for all Menu Items
    """
    update_mapping = "Update Mapping"
    upload_files = "Upload Files"
    generate_closing = "Generating Closing"
    run_day_mis = "Generate MIS"
    exit = "Exit"


class TableName(Enum):
    Close_Table = 'CloseFile'
    Trade_Table = 'Auto'
    Raw_Trade_Table = 'TradeFile'
    Cash_MIS_Table = "CASH_MIS"
    Daily_Cash_Table = 'Cash'
    Final_MIS_Table = 'FINAL_MIS'
    Mapping_Table = 'Mapping'
    Expense = 'Expense'
    DeltaTable = 'DeltaReport'
    Subscription_Redemption_Table = 'Subscribe_Redeem'
    Date_List_Table = 'DateList'
    NAV_TABLE = 'NAV'


def log_info(msg):
    """

    :param msg:
    :return:
    """
    print(msg)


def fix_usd_string(df_table):
    """

    :param df_table:
    :return:
    """
    df_temp = df_table.copy()
    for ix, ticker_row in df_table.iterrows():
        ticker_value = ticker_row[0]
        if not 'USD' in ticker_value:
            if ticker_value[0] in ['AUD', 'GBP', 'EUR', 'XAU', 'XAG']:
                p = ticker_value + '/USD'
            else:
                p = 'USD/' + ticker_value
            df_temp.loc[ix, 'Ticker'] = p
    df_temp = pd.DataFrame(data=list(set(df_temp.Ticker.tolist())))
    return df_temp


def map_calculation_type(df_data, df_mapping, uni_index, list_of_columns):
    """

    :param df_data:
    :param df_mapping:
    :return:
    """
    df_data.set_index(uni_index, inplace=True)
    df_mapping.reset_index(inplace=True, drop=True)
    df_mapping.set_index(uni_index, inplace=True)
    df_mapping = df_mapping.loc[~df_mapping.index.duplicated(keep='first')]
    df_data[list_of_columns] = df_mapping[list_of_columns]
    df_data.reset_index(inplace=True, drop=False)
    return df_data


def map_delta_close(df_data, df_close, list_of_columns):
    """

    :param df_data:
    :param df_mapping:
    :return:
    """
    mis_index = ['Delta_Cur']
    close_index = ['UniqueTicker']
    df_data.set_index(mis_index, inplace=True)
    df_close.reset_index(inplace=True, drop=True)
    df_close.set_index(close_index, inplace=True)
    df_close = df_close.loc[~df_close.index.duplicated(keep='first')]
    df_data[list_of_columns] = df_close[list_of_columns]
    df_data.reset_index(inplace=True, drop=False)
    return df_data


def get_currency(cur):
    """

    :param cur:
    :return:
    """
    if cur in CUR_LIST:
        ret_cur = cur + '/USD'
    elif cur in NEUTRAL_CUR:
        ret_cur = 'USD'
    else:
        try:
            ret_cur = 'USD/' + cur
        except:
            print(str(cur))
    return ret_cur


class VaR:
    """
    This class takes care of VaR Calculations
    """

    def __init__(self, close_file_path, multiplier_file_path):
        """

        :param close_file_path:
        :param multiplier_file_path:
        """
        self.df_close = pd.read_csv(close_file_path, parse_dates=True, index_col=0, dayfirst=True)
        self.df_close.fillna(method='FFILL', inplace=True)
        self.df_multiplier = pd.read_csv(multiplier_file_path, parse_dates=True, index_col=0,
                                         dayfirst=True)
        self.df_return = self.calculate_returns(1, True)
        self.INO_LIST = ['INO-CE', 'INO-PE', 'IDO-CE', 'IDO-PE']
        self.CNO_LIST = ['CNO-CE', 'CNO-PE']
        self.TWO_LIST = ['TWO-CE', 'TWO-PE']
        self.MYO_LIST = ['MYO-CE', 'MYO-PE']
        self.STO_LIST = ['STO-CE', 'STO-PE']
        self.COM_LIST = ['XAU', 'XAG', 'XCU', 'XZN', 'XAL']

    def calculate_returns(self, window, is_log):

        """
        This calculates the returns either log or normal and then multiplied to the multiplier
        :param df_price:
        :param df_multiplier:
        :param is_log:
        :return:
        """

        if is_log:
            df_return = np.log(self.df_close / self.df_close.shift(window))
        else:
            df_return = self.df_close.pct_change(window)

        index_list = self.df_multiplier.index.tolist()
        index_list = list(set(index_list).intersection(df_return.columns.tolist()))

        for cur in index_list:
            df_return[cur] = df_return[cur] * self.df_multiplier.loc[cur].values[0]

        return df_return

    def create_delta_dataframe(self, ex_date, database):
        """

        :param ex_date:
        :param database:
        :return:
        """

        print("Generating for :- " + str(ex_date))
        df_t, df_d = VaR.generate_delta_report(ex_date, database)
        cur_list = df_d.columns.tolist()
        ino_list = list(set(self.INO_LIST).intersection(cur_list))
        cno_list = list(set(self.CNO_LIST).intersection(cur_list))
        two_list = list(set(self.TWO_LIST).intersection(cur_list))
        sto_list = list(set(self.STO_LIST).intersection(cur_list))
        myo_list = list(set(self.MYO_LIST).intersection(cur_list))
        comdty_list = list(set(self.COM_LIST).intersection(cur_list))
        if self.INO_LIST[0] in ino_list and self.INO_LIST[1] in ino_list:
            df_d['INO'] = (df_d['INO-PE'] + df_d['INO-CE'])
        elif self.INO_LIST[0] in ino_list and self.INO_LIST[1] not in ino_list:
            df_d['INO'] = df_d['INO-CE']
        elif self.INO_LIST[0] not in ino_list and self.INO_LIST[1] in ino_list:
            df_d['INO'] = df_d['INO-PE']

        if self.INO_LIST[2] in ino_list and self.INO_LIST[3] in ino_list:
            df_d['IDO'] = (df_d['IDO-PE'] + df_d['IDO-CE'])
        elif self.INO_LIST[2] in ino_list and self.INO_LIST[3] not in ino_list:
            df_d['IDO'] = df_d['IDO-CE']
        elif self.INO_LIST[2] not in ino_list and self.INO_LIST[3] in ino_list:
            df_d['IDO'] = df_d['IDO-PE']

        if self.CNO_LIST[0] in cno_list and self.CNO_LIST[1] in cno_list:
            df_d['CNO'] = (df_d['CNO-PE'] + df_d['CNO-CE'])
        elif self.CNO_LIST[0] in cno_list and self.CNO_LIST[1] not in cno_list:
            df_d['CNO'] = df_d['CNO-CE']
        elif self.CNO_LIST[0] not in cno_list and self.CNO_LIST[1] in cno_list:
            df_d['CNO'] = df_d['CNO-PE']

        if self.TWO_LIST[0] in two_list and self.TWO_LIST[1] in two_list:
            df_d['TWO'] = (df_d['TWO-PE'] + df_d['TWO-CE'])
        elif self.TWO_LIST[0] in two_list and self.TWO_LIST[1] not in two_list:
            df_d['TWO'] = df_d['TWO-CE']
        elif self.TWO_LIST[0] not in two_list and self.TWO_LIST[1] in two_list:
            df_d['TWO'] = df_d['TWO-PE']

        if self.STO_LIST[0] in sto_list and self.STO_LIST[1] in sto_list:
            df_d['STO'] = (df_d['STO-PE'] + df_d['STO-CE'])
        elif self.STO_LIST[0] in sto_list and self.STO_LIST[1] not in sto_list:
            df_d['STO'] = df_d['STO-CE']
        elif self.STO_LIST[0] not in sto_list and self.STO_LIST[1] in sto_list:
            df_d['STO'] = df_d['STO-PE']

        if self.MYO_LIST[0] in myo_list and self.MYO_LIST[1] in myo_list:
            df_d['MYO'] = (df_d['MYO-PE'] + df_d['MYO-CE'])
        elif self.MYO_LIST[0] in myo_list and self.MYO_LIST[1] not in myo_list:
            df_d['MYO'] = df_d['MYO-CE']
        elif self.MYO_LIST[0] not in myo_list and self.TWO_LIST[1] in myo_list:
            df_d['MYO'] = df_d['MYO-PE']

        if 'INO' in df_d.columns.tolist():
            df_d['INR'] -= df_d['INO']
            df_d.drop(['INO'], axis=1, inplace=True)
        if 'IDO' in df_d.columns.tolist():
            df_d['INR'] -= df_d['IDO']
            df_d.drop(['IDO'], axis=1, inplace=True)
        if 'CNO' in df_d.columns.tolist():
            df_d['CNH'] -= df_d['CNO']
            df_d.drop(['CNO'], axis=1, inplace=True)

        if 'TWO' in df_d.columns.tolist():
            df_d['TWD'] -= df_d['TWO']
            df_d.drop(['TWO'], axis=1, inplace=True)

        if 'STO' in df_d.columns.tolist():
            df_d['STO'] -= df_d['STO']
            df_d.drop(['STO'], axis=1, inplace=True)

        if 'IDF' in df_d.columns.tolist():
            df_d.drop(['IDF'], axis=1, inplace=True)

        df_d.drop(ino_list, axis=1, inplace=True)
        df_d.drop(cno_list, axis=1, inplace=True)
        df_d.drop(two_list, axis=1, inplace=True)
        df_sum = df_d.sum(axis=0)
        df_temp_close = self.df_close.loc[:ex_date].iloc[-1][comdty_list]
        df_sum[comdty_list] = df_sum[comdty_list] * df_temp_close[comdty_list]
        df_temp = df_sum.to_frame(name=ex_date)
        df_temp = df_temp.transpose()
        df_temp.drop(['USD'], axis=1, inplace=True)
        df_delta = df_temp.copy()
        return df_delta

    def generate_daily_portfolio_mtm(self, df_delta, lookback_window):
        """

        :param ex_date:
        :return:
        """
        ex_date = df_delta.index[0]
        p_date = ex_date - rd.relativedelta(days=lookback_window)
        ix1 = np.logical_and(self.df_return.index >= p_date, self.df_return.index <= ex_date)
        # df_temp_return = df_return.index.between_time(p_date, ex_date)
        df_temp_return = self.df_return.loc[ix1]
        df_temp_return.fillna(value=0, inplace=True)
        df_temp_return = df_temp_return[df_delta.columns.tolist()]
        df_temp_delta = df_delta.loc[ex_date].squeeze()
        df_daily_pnl = df_temp_return.multiply(df_temp_delta, axis=1)
        return df_daily_pnl.sum(axis=1)

    def calculate_var_number(self, ex_date, database, percentile):
        """

        :param ex_date:
        :param database:
        :return:
        """
        df_calculate_delta = self.create_delta_dataframe(ex_date, database)
        df_daily_pnl = self.generate_daily_portfolio_mtm(df_calculate_delta, 2620)
        final_var = df_daily_pnl.quantile(percentile)
        return final_var

    @staticmethod
    def generate_delta_report(ex_date, database):
        """

        :param ex_date:
        :return:
        """

        df_delta = database.get_delta_report(ex_date)
        df_mapping = database.get_mapping_table(df_delta.UniqueTicker.tolist())
        df_delta.set_index(['UniqueTicker'], inplace=True)
        df_mapping.set_index(['UniqueTicker'], inplace=True)
        df_mapping = df_mapping.loc[~df_mapping.index.duplicated(keep='first')]
        for i in [1, 2, 3]:
            column_name = 'Leg{}_Currency'.format(str(i))
            mapping_column = 'Leg{}_Cur'.format(str(i))
            df_delta[column_name] = df_mapping[mapping_column]
        df_delta[['Strike', 'CallPut']] = df_mapping[['Strike', 'CallPut']]
        df_delta.reset_index(drop=False, inplace=True)
        df_delta['Close_Currency_1'] = df_delta['Leg1_Currency'].apply(get_currency)
        df_delta['Close_Currency_2'] = df_delta['Leg2_Currency'].apply(get_currency)
        df_delta['Close_Currency_3'] = df_delta['Leg3_Currency'].apply(get_currency)
        unique_currency_list = list(
            set(df_delta['Close_Currency_1'].tolist()).union(df_delta['Close_Currency_2'].tolist()))
        unique_currency_list = list(
            set(unique_currency_list).union(df_delta['Close_Currency_3'].tolist()))
        df_close = database.get_close_price(unique_currency_list, ex_date)

        ix_option_call = np.logical_and(df_delta.SecurityType == 'OPT', df_delta.CallPut == 'CE')
        ix_option_put = np.logical_and(df_delta.SecurityType == 'OPT', df_delta.CallPut == 'PE')
        df_delta.loc[ix_option_call, ['L1_Delta', 'L2_Delta']] = df_delta.loc[
                                                                     ix_option_call, ['L1_Delta',
                                                                                      'L2_Delta']] * 0.5
        df_delta.loc[ix_option_put, ['L1_Delta', 'L2_Delta']] = df_delta.loc[
                                                                    ix_option_put, ['L1_Delta',
                                                                                    'L2_Delta']] \
                                                                * -0.5
        ix_option_call = np.logical_and(ix_option_call,
                                        df_delta["Leg1_Currency"].str.contains('PE', regex=False))
        df_delta.loc[ix_option_call, ['L1_Delta', 'L2_Delta']] = df_delta.loc[
                                                                     ix_option_call, ['L1_Delta',
                                                                                      'L2_Delta']] * -1.0
        ix_option_put = np.logical_and(ix_option_put,
                                       df_delta["Leg1_Currency"].str.contains('CE', regex=False))
        df_delta.loc[ix_option_put, ['L1_Delta', 'L2_Delta']] = df_delta.loc[
                                                                    ix_option_put, ['L1_Delta',
                                                                                    'L2_Delta']] \
                                                                * -1.0

        df_l1_delta = df_delta.pivot_table(index=['StrategyTag', 'Leg1_Currency'],
                                           values='L1_Delta', aggfunc=sum).unstack()
        df_l1_delta = df_l1_delta.xs('L1_Delta', axis=1, drop_level=True)
        df_l1_delta_dollar = VaR.get_dollar_delta(df_l1_delta, "Close_Currency", 'Leg1_Currency',
                                                  df_close.copy())

        df_l2_delta = df_delta.pivot_table(index=['StrategyTag', 'Leg2_Currency'],
                                           values='L2_Delta', aggfunc=sum).unstack()
        df_l2_delta = df_l2_delta.xs('L2_Delta', axis=1, drop_level=True)
        df_l2_delta_dollar = VaR.get_dollar_delta(df_l2_delta, "Close_Currency", 'Leg2_Currency',
                                                  df_close.copy())

        df_l3_delta = df_delta.pivot_table(index=['StrategyTag', 'Leg3_Currency'],
                                           values='L3_Delta', aggfunc=sum).unstack()
        df_l3_delta = df_l3_delta.xs('L3_Delta', axis=1, drop_level=True)
        df_l3_delta_dollar = VaR.get_dollar_delta(df_l3_delta, "Close_Currency", 'Leg3_Currency',
                                                  df_close.copy())

        total_currency = list(set(df_l2_delta.columns.tolist()).union(df_l1_delta.columns.tolist()))
        total_currency = list(set(total_currency).union(df_l3_delta.columns.tolist()))

        missing_columns = list(set(total_currency).difference(df_l2_delta.columns.tolist()))
        df_temp = pd.DataFrame(data=0, columns=missing_columns, index=df_l2_delta.index)
        df_l2_delta = pd.concat([df_l2_delta, df_temp], sort=True)
        df_l2_delta_dollar = pd.concat([df_l2_delta_dollar, df_temp], sort=True)

        missing_columns = list(set(total_currency).difference(df_l1_delta.columns.tolist()))
        df_temp = pd.DataFrame(data=0, columns=missing_columns, index=df_l1_delta.index)
        df_l1_delta = pd.concat([df_l1_delta, df_temp], sort=True)
        df_l1_delta_dollar = pd.concat([df_l1_delta_dollar, df_temp], sort=True)

        missing_columns = list(set(total_currency).difference(df_l3_delta.columns.tolist()))
        df_temp = pd.DataFrame(data=0, columns=missing_columns, index=df_l3_delta.index)
        df_l3_delta = pd.concat([df_l3_delta, df_temp], sort=True)
        df_l3_delta_dollar = pd.concat([df_l3_delta_dollar, df_temp], sort=True)

        df_local = df_l1_delta.fillna(value=0.0) + df_l2_delta.fillna(
            value=0.0) + df_l3_delta.fillna(value=0.0)
        df_local.reset_index(drop=False, inplace=True)
        df_dollar = df_l1_delta_dollar.fillna(value=0.0) + df_l2_delta_dollar.fillna(
            value=0.0) + df_l3_delta_dollar.fillna(
            value=0.0)
        df_dollar.reset_index(drop=False, inplace=True)
        # df.rename(columns={df.columns[0]: "StrategyTag"},inplace=True)
        df_local['Strat'] = df_local['StrategyTag'].apply(lambda x: '|'.join(x.split('|')[:4]))
        df_dollar['Strat'] = df_dollar['StrategyTag'].apply(lambda x: '|'.join(x.split('|')[:4]))
        columns_to_sum = list(set((df_local.columns.tolist())).difference(['Strat', 'StrategyTag']))
        df_local = df_local.groupby(['Strat'])[columns_to_sum].sum()
        df_local.sort_values(by=df_local.index.tolist(), axis=1, inplace=True)
        df_local = df_local.reindex(sorted(df_local.columns), axis=1)
        df_dollar = df_dollar.groupby(['Strat'])[columns_to_sum].sum()
        df_dollar.sort_values(by=df_local.index.tolist(), axis=1, inplace=True)
        df_dollar = df_dollar.reindex(sorted(df_dollar.columns), axis=1)
        return df_local, df_dollar

    @staticmethod
    def get_dollar_delta(df_delta, index_currency_col, pivot_currency_col, df_close):
        """

        :param df_delta:
        :param currency_column:
        :return:
        """
        func_check = lambda x: True if '/USD' in x else False
        df_temp = df_delta.unstack().reset_index()
        df_temp.rename({0: 'Delta'}, axis=1, inplace=True)
        df_temp['Delta'].fillna(value=0.0, inplace=True)
        df_temp['Close_Currency'] = df_temp[pivot_currency_col].apply(get_currency)
        df_close.set_index('UniqueTicker', inplace=True)
        df_temp.set_index(index_currency_col, inplace=True)
        df_close = df_close.loc[~df_close.index.duplicated(keep='first')]
        df_temp['Close'] = df_close['Close']
        df_temp['Close'].fillna(value=1, inplace=True)
        df_temp.reset_index(drop=False, inplace=True)
        df_temp['Multiplier'] = df_temp[index_currency_col].apply(func_check)
        ix1 = df_temp['Multiplier']
        ix2 = np.logical_not(ix1)
        df_temp.loc[ix1, 'DollarDelta'] = df_temp.loc[ix1, 'Delta'] * df_temp.loc[ix1, 'Close']
        df_temp.loc[ix2, 'DollarDelta'] = df_temp.loc[ix2, 'Delta'] / df_temp.loc[ix2, 'Close']
        df_temp.drop(['Close', 'Multiplier', 'Delta'], axis=1, inplace=True)
        df_temp = df_temp.pivot_table(index=['StrategyTag', pivot_currency_col],
                                      values='DollarDelta',
                                      aggfunc=sum).unstack()
        df_temp = df_temp.xs('DollarDelta', axis=1, drop_level=True)
        return df_temp


import logging


class log_message:
    """
    This Class is to hold all function for logging
    """

    def __init__(self, logger_name):
        """

        :param logger_name:
        """
        self.logger = logging.getLogger(logger_name)
        self.logger.setLevel(logging.DEBUG)
        self.console_handling = logging.StreamHandler()
        self.formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        self.console_handling.setFormatter(self.formatter)
        self.logger.addHandler(self.console_handling)

    def set_file_handler(self, file_name):
        """

        :param file_name:
        :return:
        """
        self.file_handler = logging.FileHandler(file_name, mode='a')
        self.file_handler.setFormatter(self.formatter)
        self.logger.addHandler(self.file_handler)

    def log_message(self, message, level=logging.INFO):
        """
        @purpose: This function ensure that
        :param message:
        :param level:
        :return:
        """
        if level not in [logging.INFO, logging.DEBUG, logging.ERROR, logging.WARNING]:
            raise Exception("level not in logging type")
        if level == logging.INFO:
            self.logger.info(message)
        elif level == logging.WARNING:
            self.logger.warning(message)
        elif level == logging.ERROR:
            self.logger.error(message)
        elif level == logging.DEBUG:
            self.logger.debug(message)
