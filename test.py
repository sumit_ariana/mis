import helper as hp
import os
import pandas as pd
import datetime as dt
if __name__ == '__main__':
    base_path = os.path.abspath('Database')
    db_name = 'GAF_MIS'
    destination_db = hp.Database(base_path, db_name)
    destination_db.conncet_db()
    base_path = os.path.abspath('Database')
    db_name = 'ARIANA_MIS'
    source_db = hp.Database(base_path, db_name)
    source_db.conncet_db()
    query_str = 'select * from Mapping where UniqueTicker = "USD/TWD 01/04/2021 Curncy"'
    df = source_db.get_query_df(query_str)
    df.dropna(how='all',inplace=True)
    df['Update_DateTime'] = pd.to_datetime(df['Update_DateTime'])
    df['LastTradeableDate'] = dt.datetime(2020,12,30)
    df['ValueDate'] = '04-Jan-2021'
    # df.to_clipboard()
    df.to_sql("Mapping", destination_db.db_connection, if_exists='append', index=False)
